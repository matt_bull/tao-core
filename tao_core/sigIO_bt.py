from sigIO import *
import bluetooth

class BtRfcommTransport(Transport):
	def __tinit__(self,sock=None,peeraddr=''):
		if sock:
			self.dev=sock
			self.peer=peeraddr
			self.registerFd(sock.fileno())
			self.setFlags(sock.fileno(),select.POLLIN | select.POLLERR)
			self.state=ST_CONNECTED
		else:
			#self.dev=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
			self.dev=bluetooth.BluetoothSocket(bluetooth.RFCOMM)
			self.registerFd(self.dev.fileno())
			self.state=ST_CLOSED
	def connect(self,address):
		#print "[sigIO_bt] connecting to %s , %s" % address
		self.dev.connect(address)
		Transport.connect(self,address)
		# this is bidirectional so set flags ready for response....
		self.setFlags(self.dev.fileno(),select.POLLIN | select.POLLHUP | select.POLLERR | select.POLLNVAL)
		#print "[sigIO_bt] connected"
	def listen(self,address):
		self.dev.bind(address)
		self.dev.listen(3)
		Transport.listen(self,address)
	def doInput(self,fd):
		#print "[sigIO_bt] doing bluetooth input on %s" % self
		if self.state==ST_LISTENING:
			so,peer=self.dev.accept()
			new_ch=self.spawnChannel(trans_args={'sock':so,'peeraddr':peer})
		else:
			try:
				chunk=self.dev.recv(4096)
			except bluetooth.BluetoothError,err:
				#self.service.log(LOG_ERR,"bluetooth error %s" % err)
				self.close()
				return
			#print "recv %s bytes" % len(chunk)
			return self.in_buffer.feed(chunk)
	def doOutput(self,fd):
		sent=self.dev.send(self.out_buffer)
		self.out_buffer=self.out_buffer[sent:]
		return len(self.out_buffer)
	def doClose(self):
		if self.dev:
			self.dev.close()
