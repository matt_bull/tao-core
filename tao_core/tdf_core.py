
#   Copyright 2011 Matthew Bull

#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# event dispatch core...

# "make the simple things easy, and the complex things possible" - Matt Bull

# a dispatcher is a callable (or a subclass of Dispatcher which overrides dispatch) which takes an event object, and optionally
# a callback (cb) as arguments, and returns False if dispatch should continue, True if dispatch should be held for some reason
# (only valid if callback has been passed in) or raises an exception, in particular if StopIteration is raised any further dispatch will
# stop and event will be immediately rendered back to its source.
# note: if a dispatcher returns True and cb is passed in the dispatcher takes on responsibility for calling the callback when the dispatcher
# has finished with the event... the callback __must__ be called.

# The callback is also a callable which takes the event object and any exceptions raised during dispatch as arguments. again the exception
# with particular meaning is StopIteration(), with the same meaning as when raised on dispatch, in fact all exceptions passed into a callback
# should be treated as they would be if they were raised on dispatch.

# There is only one major Gotcha.... you must not call self.dispatch or self.put (in a source) in a callback.... (the only exceptions to this
# are MultiDispatcher, BusDispatcher and LoopSource, so use these if you really need to do it) you may get away with it in some situations but sooner
# or later everything will come to a crashing halt... you have been warned!

# assembling the dispatch graph.... most dispatchers will be created and connected during initialisation of a channel / dispatch path, indeed most
# dispatchers enforce this, with the exception of the mapping / multi dispatchers (implementing the registerDispatcher / removeDispatcher API) which
# can and should be used to dynamically alter dispatch paths post initialisation.
from __future__ import absolute_import
import Queue,thread,threading,time,sys,traceback,types,copy,weakref
from . import utils
#from operator import isCallable

# meta class for events.... allows defaults to be inherited
class _EventMeta_(type):
	def __init__(cls,name,bases,dct):
		cls.__updschema__(cls,bases)
		return type.__init__(cls,name,bases,dct)
	@classmethod
	def __updschema__(meta,cls,bases):
		for base in	bases:
			try:
				for key in base.defaults.keys():
					if cls.defaults.has_key(key):
						if type(base.defaults[key])!=type(cls.defaults[key]):
							raise TypeError('Inconsistent type for %s in %s, should match base type %s' % (key,repr(cls),type(base.defaults[key])))
					else:
						cls.defaults[key]=copy.copy(base.defaults[key])
			except AttributeError:
				pass

# a simple event class derived from dict....
# defaults is sort of a combination of default values and schema...
# only use standard python types except generators (use a list instead)
# for values with an unknown type use None (types of values will not be checked)

class SimpleEvent(dict):
	__metaclass__=_EventMeta_
	defaults={}
	def __init__(self,evt_name,**kwargs):
		self.name=evt_name
		#print "updating simple event with data %s" % repr(data)
		dict.__init__(self)
		# don't avoid __setitem__ with kwargs
		for key in kwargs.keys():
			self[key]=kwargs[key]
	def has_key(self,key):
		if dict.has_key(self,key) or self.defaults.has_key(key):
			return True
		else:
			return False
	def __getitem__(self,key):
		try:
			return dict.__getitem__(self,key)
		except KeyError:
			# key not currently set... deal with mutable types here...
			# we must deal with mutable types as we will be passing back a reference to
			# an object which may be mutated outside our control.
			if type(self.defaults[key])==types.DictionaryType:
				n_d=copy.copy(self.defaults[key])
				self.__setitem__(key,n_d)
				return n_d
			elif type(self.defaults[key])==types.ListType:
				n_l=self.defaults[key][:]
				self.__setitem__(key,n_l)
				return n_l
			else:
				return self.defaults[key]
	def __setitem__(self,key,value):
		if self.defaults.has_key(key):
			if self.defaults[key]==None or type(value)==type(self.defaults[key]):
				dict.__setitem__(self,key,value)
			elif type(value)==types.IntType and type(self.defaults[key])==types.LongType:
				dict.__setitem__(self,key,value)
			else:
				try:
					dict.__setitem__(self,key,type(self.defaults[key])(value))
				except:
					raise ValueError('incompatible types for key %s - %s should be %s' % (key,type(value),type(self.defaults[key])))
		else:
			dict.__setitem__(self,key,value)
	def getDict(self):
		# return a copy of ourselves as a simple dict...
		d={}
		for key in self.defaults.keys():
			d[key]=self.defaults[key]
		for key in self.keys():
			d[key]=self[key]
		return d
	def __repr__(self):
		return "<Event Object : "+self.__class__.__module__+"."+self.__class__.__name__+" named "+self.name+">"
	def __hash__(self):
		return id(self)

# an authenticated event class which derives from event.Event....
# this is dervied from a C python object with readonly attributes (user,domain and validated) and can be trusted
# however not all events are derived from this so you should check with isinstance(AuthenticatedEvent)....
try:
	import event
	class AuthenticatedEvent(event.Event):
		defaults={}
		def __init__(self,evt_name,user,domain="unknown",cred=None):
			self._evt_name=evt_name
		def __hash__(self):
			return id(self)
except:
	pass

# while you don't have to use CompositeException / DispatchException for exception handling (the callback will also take a straight Exception object)
# it helps with debugging as it retains the traceback data and the dispatcher where the exception occured...
class DispatchException(utils.CompositeException):
	def __init__(self,dispatcher,msg=None):
		if msg:
			utils.CompositeException.__init__(self,"%s in dispatcher - %s" % (msg,repr(dispatcher)))
		else:
			utils.CompositeException.__init__(self,"Exception in Dispatcher - %s" % repr(dispatcher))

# utility functions
# quick and easy dispatch wrapper.... in effect do what source does for put(<event>,sync=True) without having to subclass
def dispatchWithCallback(dispatcher,event,cb):
	try:
		#print ">>dispatching>>",id(event),dispatcher
		dis_res=dispatcher(event,cb=cb)
		#print ">>dispatched>>",id(event),dispatcher,dis_res
	except StopIteration:
		#print ">>Callback>>",id(event),dispatcher,'StopIteration'
		cb(event)
	except Exception,exc:
		#print ">>Callback>>",id(event),dispatcher,exc
		cb(event,exc=DispatchException(dispatcher))
	else:
		if not dis_res:
			#print ">>Callback>>",id(event),dispatcher,'on false dis_res'
			cb(event)
		return dis_res

# modify graph event, this is never ignored by dispatchers
class ModifyGraph(dict):
	def __init__(self,dispatcher,register=True,key=None):
		self.name='org.digitao.tao_core.ModifyGraph'
		dict.__init__(self,{'dispatcher':dispatcher,'register':register,'key':key})

# a dispatch proxy for sigIO channels to use
class DispatchProxy(object):
	def __init__(self,dsp,meth_name='dispatch'):
		self._dsp=weakref.ref(dsp)
		self._meth=meth_name
	def __call__(self,evt,cb=None):
		d=self._dsp()
		if d:
			try:
				getattr(d,self._meth)(evt,cb=cb)
			except AttributeError:
				raise ReferenceError
		else:
			raise ReferenceError

# Main dispatcher base classes

class _DispatchMeta_(type):
	def __init__(cls,name,bases,dct):
		for base in bases:
			if hasattr(base,'accept_events'):
				for acc_evt in base.accept_events:
					if acc_evt not in cls.accept_events:
						cls.accept_events.append(acc_evt)
		return type.__init__(cls,name,bases,dct)

class Dispatcher(object):
	# dispatcher base class if you use a callable it should behave the same as dispatch()
	accept_events=[]
	#__metaclass__=_DispatchMeta_
	def __init__(self,dispatcher=None):
		self._dispatcher=dispatcher
	def dispatch(self,event,cb=None):
		# override this to implement other dispatch schemes
		# we ignore cb here as we never return True to hold dispatch
		if cb and self._dispatcher:
			return dispatchWithCallback(self._dispatcher,event,cb=cb)
		elif self._dispatcher:
			return self._dispatcher(event)
	def mgDispatch(self,event):
		# this is called for ModifyGraph events override in mapping base classes (with registerDispatcher et al) to change the way these
		# register / remove dispatchers
		# pass on to our dispatcher
		self._dispatcher.mgDispatch(event)
	def __call__(self,event,cb=None):
		# if self.accept_events has items... filter based on those event names
		if (not self.accept_events) or event.name in self.accept_events:
			#print "!!! DISPATCH"
			return self.dispatch(event,cb=cb)
		elif event.name=='org.digitao.tao_core.ModifyGraph':
			return self.mgDispatch(event)
		else:
			sys.stderr.write("rejecting event -"+event)

class Source:
	"""event source base class (dispatches all events to self._dispatcher)
	this implements the requirements for a source of sync events... (eg. a server IO channel)
	note: for non sync events, just call dispatch.
	this is _very_ simplistic (the absolute minimum), and should really track events / log exceptions etc"""
	def __init__(self,dispatcher):
		self._dispatcher=dispatcher
	def put(self,event,sync=False):
		if sync:
			dispatchWithCallback(self._dispatcher,event,self.render)
		else:
			try:
				self._dispatcher(event)
			except Exception:
				sys.stderr.write("Exception on dispatch -",repr(self),repr(DispatchException()))
	def render(self,event,exc=None):
		# override to render events back to wherever we are generating events from...
		# caveat: NEVER call self.put from inside this method... it can lock the event loop...
		pass

class SyncSource(Source):
	""" An event source that cannot deal with async callbacks from another thread...
	ie. Many GUI toolkits don't deal with python threads well... this synchronises returned events
	note. you need to regularly call iterCb to deal with all waiting (handled) events"""
	queue_len=20
	def __init__(self,dispatcher):
		Source.__init__(self,dispatcher)
		self.r_queue=Queue.Queue(self.queue_len)
	def put(self,event,sync=False):
		if sync:
			dispatchWithCallback(self._dispatcher,event,self._cb)
		else:
			try:
				self._dispatcher(event)
			except Exception:
				print repr(DispatchException(self))
	def _cb(self,event,exc=None):
		self.r_queue.put_nowait((event,exc))
	def iterCb(self):
		while 1:
			try:
				evt,exc=self.r_queue.get_nowait()
			except Queue.Empty:
				break
			self.render(evt,exc=exc)

class LoopSource(SyncSource):
	""" this one is a little funky, has its own event loop to generate events at
	intervals and call iterCb.... use where there is no event loop to drive dispatch
	(ie where we aren't a channel, but something else) note: poll _must_ not block"""
	def __init__(self,dispatcher):
		SyncSource.__init__(self,dispatcher)
		self._run=False
		self.intv=20
	def start(self):
		self._run=True
		thread.start_new_thread(self.mainloop,(),{})
	def stop(self):
		self._run=False
		self.r_queue.put((None,None))
	def mainloop(self):
		while self._run:
			self.poll()
			self.iterCb(self.intv)
	def poll(self):
		""" override to do scheduled tasks and possibly manipulate self.intv
		 to effect the next scheduled call.... THIS MUST NOT BLOCK"""
		pass
	def iterCb(self,intv):
		exit=time.time()+intv
		# clear the queue
		while 1:
			try:
				evt,exc=self.r_queue.get(True,intv)
			except Queue.Empty:
				break
			if evt:
				try:
					self.render(evt,exc=exc)
				except:
					traceback.print_exc()
			else:
				break
			# this ensures we clear the queue even if it takes us beyond intv
			intv=max(.1,exit-time.time())

class PollingSource(LoopSource):
	def __init__(self,dispatcher):
		""" different from loop source this one calls poll when the event
		is returned or the timeout is reached (whichever happens first)
		this is useful for a bunch of stuff but mostly for polling something
		else for data (eg http polling for data from a server)"""
		self.intv=60
		LoopSource.__init__(self,dispatcher)
	def mainloop(self):
		while self._run:
			self.iterCb(self.intv)
	def poll(self,resp=None):
		"""override to generate events possibly from resp, if resp is None we hit the timeout..."""
		pass
	def iterCb(self,intv):
		try:
			evt,exc=self.r_queue.get(True,intv)
		except Queue.Empty:
			return
		if evt:
			self.render(evt,exc=exc)
		else:
			self.poll()
	def render(self,event,exc=None):
		"""override to call poll with something meaningful, depending on response"""
		self.poll(resp=True)

class TrackDispatcher(Dispatcher):
	"""base class for dispatchers that keep track of callbacks,
	outbound (client) IO channels would be an example, but there
	are many more uses where you want to catch an event on the way through
	and the way back, this is also the base class to use for 'pipe' style dispatchers"""
	def __init__(self,dispatcher=None):
		self.cb_map={}
		Dispatcher.__init__(self,dispatcher=dispatcher)
	def dispatch(self,event,cb=None):
		#print "dispatch in track",self._dispatcher
		if cb and self._dispatcher:
			self.cb_map[event]=cb
			dispatchWithCallback(self._dispatcher,event,self.finish)
			return True
		elif self._dispatcher:
			self._dispatcher(event)
		# override and do the actual work here...
		# and call finish when done...
		# or call another dispatcher and pass in finish...
		# don't forget to call finish if you are using another
		# dispatcher and it doesn't return True (or better still as
		# above use dispatchWithCallback
	def mgDispatch(self,event):
		# we aren't interested in these but should pass it on...
		if self._dispatcher:
			return self._dispatcher(event)
	def finish(self,event,exc=None):
		try:
			#print self,event,'-cb->',self.cb_map[event]
			cb=self.cb_map.pop(event)
		except KeyError:
			#print self.cb_map
			print "[ERROR] Callback for %s not found in TrackDispatcher - %s" % (event,repr(self))
			return
		#print self,event,cb
		try:
			cb(event,exc=exc)
		except Exception:
			sys.stderr.write("!!! Exception on callback !!!")
			sys.stderr.write(repr(utils.CompositeException()))

class EventAdapter(TrackDispatcher):
	"""adapt events of one class coming from source to another going to _dispatcher"""
	def __init__(self,dispatcher):
		self._dispatcher=dispatcher
		TrackDispatcher.__init__(self)
		self.evt_map={}
	def dispatch(self,evt,cb=None):
		if self._dispatcher:
			n_evt=self.adaptEvent(evt)
			if cb:
				self.evt_map[n_evt]=evt
				return TrackDispatcher.dispatch(self,evt,cb=cb)
			else:
				self._dispatcher(n_evt)
	def finish(self,evt,exc=None):
		o_evt=self.evt_map.pop(evt)
		self.adaptResponse(evt,o_evt,exc)
		TrackDispatcher.finish(self,o_evt,exc=exc)
	def adaptEvent(self,o_evt):
		# override to get adapted event for o_evt
		raise NotImplementedError("no adaptEvent method in adapter")
	def adaptResponse(self,n_evt,o_evt,exc):
		# overide to adapt response in n_evt into o_evt....
		# we pass here as raising an exception in the return path is bad
		pass

class SyncDispatcher(TrackDispatcher):
	"""Useful little dispatcher base, allows you to dispatch req / response events
	in a synchronous manner via syncDispatch... which returns the event after complete dispatch
	this is a bit of a cheat (albeit a really useful one) and should be avoided if you can do 
	things in an async way (with callbacks), but a useful get out of jail free card when dealing
	with external libs. """
	def __init__(self):
		TrackDispatcher.__init__(self)
		self.sync_map={}
	def syncDispatch(self,event):
		lck=thread.allocate_lock()
		lck.acquire()
		self.sync_map[event]=(lck,None)
		dis_res=self.dispatch(event,self._sync_complete_)
		if dis_res:
			lck.acquire()
		exc=self.sync_map[event][1]
		del(self.sync_map[event])
		if exc:
			raise exc
		return event
	def _sync_complete_(self,event,exc=None):
		if exc:
			self.sync_map[event]=(self.sync_map[event][0],exc)
		try:
			self.sync_map[event][0].release()
		except thread.error:
			pass

class ThreadedDispatcher(TrackDispatcher):
	""" a fairly simple threaded dispatcher which starts a new thread for each event."""
	def dispatch(self,event,cb=None):
		TrackDispatcher.dispatch(self,event,cb=cb)
		if cb:
			thread.start_new_thread(self._handle,(event,True),{})
			return True
		else:
			thread.start_new_thread(self._handle,(event,False),{})
	def _handle(self,evt,sync):
		if sync:
			try:
				# it may seem a little wierd we don't pass in cb here...
				# but we will take care of it ourselves so passing it in would result in
				# a double call to self.finish
				self.handle(evt)
			except StopIteration,exc:
				self.finish(evt,exc=exc)
			except Exception:
				self.finish(evt,exc=DispatchException(self))
			else:
				self.finish(evt)
		else:
			self.handle(evt)
	def handle(self,evt):
		#override to do somthing with evt
		pass

class ExternalThreadedDispatcher(ThreadedDispatcher):
	""" a subclass of threaded which wraps a dispatcher or dispatch function
	<target>, calling it in its own thread and keeping everything straight..."""
	def __init__(self,target):
		ThreadedDispatcher.__init__(self)
		self._target=target
	def _handle(self,evt,sync):
		if sync:
			try:
				if not self._target(evt,cb=self.finish):
					self.finish(evt)
			except StopIteration,exc:
				self.finish(evt,exc=exc)
			except Exception:
				self.finish(evt,exc=DispatchException(self))
		else:
			self._target(evt)

class ThreadPoolDispatcher(TrackDispatcher):
	""" an external threaded dispatcher which maintains a thread pool and 
	calls <target> repeatedly for events in one of the pool threads via a queue...
	this is a really simple example, could be extended to allow pool resizing etc"""
	def __init__(self,target,pool_size,queue_size=None):
		TrackDispatcher.__init__(self)
		if not queue_size:
			queue_size=pool_size*5
		self._queue=Queue.Queue(queue_size)
		if type(target)==types.FunctionType:
			while pool_size>0:
				thread.start_new_thread(self.consumer,(target,),{})
				pool_size=pool_size-1
		else:
			# not a function, assume its a dispatcher which takes no args
			while pool_size>0:
				thread.start_new_thread(self.consumer,(target,),{})
				pool_size=pool_size-1
	def dispatch(self,event,cb=None):
		TrackDispatcher.dispatch(self,event,cb=cb)
		self._queue.put_nowait(event)
		if cb:
			return True
	def consumer(self,target):
		while 1:
			evt=self._queue.get()
			if not evt:
				break
			try:
				if not target(evt,cb=self.finish):
					self.finish(evt)
			except StopIteration,exc:
				self.finish(evt,exc=exc)
			except Exception:
				#print "exception in thread pool"
				#print repr(DispatchException(target))
				self.finish(evt,exc=DispatchException(target))
	def finish(self,event,exc=None):
		if self.cb_map.has_key(event):
			try:
				cb=self.cb_map.pop(event)
				cb(event,exc=exc)
			except Exception:
				sys.stderr.write("!!! Exception on callback !!!")
				sys.stderr.write(repr(utils.CompositeException()))

class QueueDispatcher(TrackDispatcher):
	"""sort of like a proccess in erlang, basically an Actor"""
	queue_len=20
	def __init__(self,*args,**kwargs):
		TrackDispatcher.__init__(self)
		self.in_queue=Queue.Queue(self.queue_len)
		self._active=True
		thread.start_new_thread(self.main,args,kwargs)
	def main(self,*args,**kwargs):
		"""override this to pull events from self.in_queue do something with each one then call self.finish in a loop.
		note that args and kwargs from __init__ are passed directly to this method...."""
		pass
	def dispatch(self,event,cb=None):
		#TrackDispatcher.dispatch(self,event,cb=cb)
		if cb:
			self.cb_map[event]=cb
			try:
				self.in_queue.put_nowait(event)
				return True
			except Exception:
				self.finish(event,exc=DispatchException(self))
		else:
			self.in_queue.put_nowait(event)
	def __del__(self):
		self._active=False
		self.in_queue.put(None)

class MappingDispatcher(Dispatcher):
	"""basic mapping dispatcher that dispatches based on event.name override registerDispatcher,
	removeDispatcher and dispatch, to dispatch based on another parameter or implement checking validity of parameter,
	note. this only allows one dispatcher per key - one to one mapping."""
	def __init__(self):
		Dispatcher.__init__(self)
		self.dispatch_map={}
	def registerDispatcher(self,evt_name,dispatcher,**kwargs):
		""" register dispatcher for event name <evt_name>, we use a kwargs glob here as
		other mapping dispatchers may have key / value pairs for matching (as kwargs)"""
		if self.dispatch_map.has_key(evt_name):
			raise NameError("Mapping dispatcher can only have one dispatcher for each event name")
		else:
			self.dispatch_map[evt_name]=dispatcher
	def removeDispatcher(self,evt_name,dispatcher):
		"""obviously this doesn't really need dispatcher (this only supports one dispatcher per event name)
		 but it helps keep the API consistent"""
		if not self.dispatch_map[evt_name]==dispatcher:
			raise ValueError('incorrect dispatcher')
		try:
			del(self.dispatch_map[evt_name])
		except:
			pass
	def removeAllDispatcher(self,dispatcher):
		for name,disp in self.dispatch_map:
			if disp==dispatcher:
				self.removeDispatcher(name,dispatcher)
	def dispatch(self,event,cb=None):
		if self.dispatch_map.has_key(event.name):
			try:
				return self.dispatch_map[event.name](event,cb=cb)
			except ReferenceError:
				del(self.dispatch_map[event.name])
	def mgDispatch(self,event):
		if event['register']:
			self.registerDispatcher(event['key'],event['dispatcher'])
		elif event['key']:
			self.removeDispatcher(event['key'],event['dispatcher'])
		else:
			self.removeAllDispatcher(event['dispatcher'])
	def __call__(self,event,cb=None):
		# filter as before but also accept events for any event name we have a dispatcher registered for...
		#print "MappingDispatcher call with ->",event.name,self.accept_events,self.dispatch_map
		if self.dispatch_map.has_key(event.name) or event.name in self.accept_events or (not self.accept_events):
			return self.dispatch(event,cb=cb)
		elif event.name=='org.digitao.tao_core.ModifyGraph':
			return self.mgDispatch(event)

class MultiDispatcher(TrackDispatcher):
	""" Base class for dispatchers which fire (multiple) other dispatchers...
	Services and Mainloops etc... this usually implies a loop of some description
	this is a base class so by itself doesn't do much you need to repeatedly call
	iterDispatch() from your subclass (either from some form of loop or an external source)"""
	def __init__(self):
		TrackDispatcher.__init__(self)
		self.event_map={}
		self.active=[]
		self.dispatch_map=utils.ListDict()
		self._lck=thread.allocate_lock()
		self.exc_map={}
	def registerDispatcher(self,evt_name,dispatcher,**kwargs):
		#print "[MultiDuspatcher.registerDispatcher]",evt_name,dispatcher
		self.dispatch_map.add(evt_name,dispatcher)
		#if self.dispatch_map.has_key(evt_name):
		#	self.dispatch_map[evt_name].append(dispatcher)
		#else:
		#	self.dispatch_map[evt_name]=[dispatcher]
		#print "[MultiDuspatcher.registerDispatcher]",self.dispatch_map[evt_name]
	def removeDispatcher(self,evt_name,dispatcher):
		#print "removeDispatcher ==>>",evt_name,self.dispatch_map
		try:
			self.dispatch_map[evt_name].remove(dispatcher)
		except KeyError:
			sys.stderr.write("Unkown event name %s during removal of dispatcher\n" % evt_name)
		except ValueError:
			pass
		if len(self.dispatch_map[evt_name])<1:
			del(self.dispatch_map[evt_name])
	def removeAllDispatcher(self,dispatcher):
		for name in self.dispatch_map.keys():
			#print dispatcher,self.dispatch_map[name]
			#print "==>> remove all for",name
			try:
				self.removeDispatcher(name,dispatcher)
			except ValueError:
				pass
	def getDispatchers(self,evt):
		try:
			return iter(self.dispatch_map[evt.name])
		except KeyError:
			return None
	def dispatch(self,event,cb=None):
		disp=self.getDispatchers(event)
		if not disp:
			sys.stderr.write("No dispatchers registered for event name %s in %s\n" % (event.name,repr(self)))
			return False
		if cb:
			self.cb_map[event]=cb
		self._lck.acquire()
		self.event_map[event]=disp
		self.active.append(event)
		self._lck.release()
		return True
	def mgDispatch(self,event):
		if event['register']:
			self.registerDispatcher(event['key'],event['dispatcher'])
		elif event['key']:
			self.removeDispatcher(event['key'],event['dispatcher'])
		else:
			self.removeAllDispatcher(event['dispatcher'])
	def __call__(self,event,cb=None):
		# filter as before but also accept events for any event name we have a dispatcher registered for...
		#print "call in multi",event.name,self.dispatch_map
		if event.name=='org.digitao.tao_core.ModifyGraph':
			return self.mgDispatch(event)
		elif self.dispatch_map.has_key(event.name) or event.name in self.accept_events or (not self.accept_events):
			return self.dispatch(event,cb=cb)
	def iterDispatch(self):
		#print "iter on %s" % self.active,self.event_map
		for event in self.active[:]:
			try:
				disp=self.event_map[event].next()
			except StopIteration:
				self.finish(event)
				continue
			except KeyError:
				# no longer in event map.....
				# remove from active
				self.active.remove(event)
				continue
			if self.cb_map.has_key(event):
				try:
					disp_res=disp(event,cb=self.handleNext)
				except ReferenceError:
					#print "-->> remove %s from %s" % (disp,self.dispatch_map)
					self.removeAllDispatcher(disp)
					continue
				except StopIteration:
					self.finish(event)
					continue
				except Exception,err:
					self.exc_map[event]=DispatchException(disp)
					#print "Exception on dispatch!!",disp,traceback.format_exc()
					continue
				if disp_res:
					self._lck.acquire()
					self.active.remove(event)
					self._lck.release()
			else:
				try:
					disp(event)
				except StopIteration:
					self.finish(event)
					continue
				except ReferenceError:
					self.removeAllDispatcher(disp)
				except Exception,err:
					self.exc_map[event]=DispatchException(disp)
					print "Exception on dispatch!!",disp,traceback.format_exc()
					continue					
	def handleNext(self,event,exc=None):
		if isinstance(exc,StopIteration):
			self.finish(event,exc=exc)
			return
		elif exc:
			self.exc_map[event]=exc
		self._lck.acquire()
		self.active.append(event)
		self._lck.release()
	def finish(self,event,exc=None):
		#print "BusDispatcher.finish() ->",event,exc
		#print "%s exc_map %s, cb_map %s, event_map %s." % (event,self.exc_map.has_key(event),self.cb_map.has_key(event),self.event_map.has_key(event))
		try:
			exc=self.exc_map.pop(event)
		except KeyError:
			pass
		self._lck.acquire()
		del(self.event_map[event])
		try:
			self.active.remove(event)
		except IndexError:
			pass
		self._lck.release()
		# we check here as non sync events don't get tracked....
		if self.cb_map.has_key(event):
			TrackDispatcher.finish(self,event,exc=exc)
		#print "%s exc_map %s, cb_map %s, event_map %s." % (event,self.exc_map.has_key(event),self.cb_map.has_key(event),self.event_map.has_key(event))

class BusDispatcher(MultiDispatcher):
	""" Bus dispatcher class, implements a generic loop...
	calls multiple dispatchers per key, this isn't neccesarily the only loop within a process
	if you wanted to implement a complex service for instance this would be ideal"""
	def __init__(self):
		MultiDispatcher.__init__(self)
		self._active=threading.Event()
		self._run=False
		self._dispatching=True
		self.timeout=None
	def dispatch(self,evt,cb=None):
		#print "Bus dispatch",evt,cb
		if self._dispatching:
			res=MultiDispatcher.dispatch(self,evt,cb=cb)
			if res:
				self._active.set()
			return res
	def mainloop(self):
		# run mainloop until self._run set false then return
		while self._run:
			try:
				self._active.wait(self.timeout)
			except KeyboardInterrupt:
				self._run=False
			self.iterDispatch()
			if not self.active:
				self._active.clear()
		# you should actually do any clean up here
	def handleNext(self,event,exc=None):
		MultiDispatcher.handleNext(self,event,exc=exc)
		self._active.set()
	def start(self):
		if not self._run:
			self._run=True
			thread.start_new_thread(self.mainloop,(),{})
		else:
			raise RuntimeError('Duplicate call to start, Mainloop already running')
	def stop(self,dispatch_remainder=True):
		self._dispatching=False
		if dispatch_remainder:
			# keep the loop spinning till all events are dealt with...
			while self.event_map:
				self._active.set()
				time.sleep(.2)
		self._run=False
		# set _active for the final time to unlock event loop
		self._active.set()
		#print "BusDispatcher -> exit"

class DispatchLogger(TrackDispatcher):
	""" A neat example of a dispatcher that actually does something....
	and a useful little tool to boot!!
	prints all events dispatched and returned, and exceptions generated downstream...
	pass in a path to have it log to a file..."""
	def __init__(self,dispatcher,ident='',trace=True,path=None):
		TrackDispatcher.__init__(self)
		self._dispatcher=dispatcher
		self.prestr=ident
		self.trace=trace
		self.l_f=None
		if path:
			try:
				self.l_f=open(path,'a')
			except OSError:
				sys.stderr.write("Error in DispatchLogger opening log file %s" % path)
	def logEvent(self,evt_name,msg,exc=None):
		if exc:
			log_str="ERROR - %s (%s) %s \n\t%s" % (evt_name,time.asctime(time.localtime()),msg,exc.__class__.__name__+' - '+str(exc))
		else:
			log_str="Info - %s (%s) %s" % (evt_name,time.asctime(time.localtime()),msg)
		if self.l_f:
			self.l_f.write(log_str+"\n")
		sys.stderr.write('>>>'+self.prestr+log_str)
	def dispatch(self,evt,cb=None):
		self.logEvent(evt.name,"Dispatch")
		try:
			dis_res=TrackDispatcher.dispatch(self,evt,cb=cb)
		except Exception,exc:
			self.logEvent(evt.name,"!!!Dispatch error\n"+traceback.format_exc())
			raise exc
		return dis_res
	def finish(self,evt,exc=None):
		if self.cb_map.has_key(evt):
			TrackDispatcher.finish(self,evt,exc=exc)
			self.logEvent(evt.name,"Callback",exc=exc)
		elif exc:
			self.logEvent(evt.name,"Error",exc=exc)

if __name__=="__main__":
	# simple test of events being dispatched by a BusDispatcher dispatcher
	# note how simple dispatchers _can_ be (all the functions below)
	class TestEvent(SimpleEvent):
		defaults={'text':'','type':'TestEvent'}
		# the default key 'text' means we can rely on this key existing
		# int the handlers (dispatcher functions) below
	class TestSubEvent(TestEvent):
		defaults={'foo':'bar','type':'TestSubEvent'}
	def handler1(event,cb=None):
		print "handler 1 %s" % event['text']
	def handler2(event,cb=None):
		print "handler 2 %s" % event['text']
	def handler3(event,cb=None):
		print "handler 3 %s" % event['text']
		# raise an exception to test the exception handling
		raise NameError('name error in handler 3 for event %s' % event['text'])
	def handler4_thread(event,cb=None):
		# if you are doing anything even vaguely complex here
		# it should be wrapped in try - except....
		# remember we are responsible for calling the cb here
		time.sleep(2)
		print "handler 4 %s" % event['text']
		if cb:
			cb(event)
	handler4=ExternalThreadedDispatcher(handler4_thread)
	def render(event,exc=None):
		print "rendering %s" % event
		for key in event.getDict().keys():
			print key,event[key]
		if exc:
			print "exception -> %s" % str(exc)
	ml=BusDispatcher()
	#ml.registerDispatcher("test_evt",handler1)
	#ml.registerDispatcher("test_evt",handler2)
	dl1=DispatchLogger(handler3)
	ml.registerDispatcher("test_evt",dl1)
	#ml.registerDispatcher("test_evt",handler4)
	ml.registerDispatcher("test_evt2",handler4)
	# lets route one through dispatch logger....
	# we create the logger and hand it one of the handlers as a dispatcher...
	dl2=DispatchLogger(handler1)
	# then register it for events with ml...
	ml.registerDispatcher("test_evt2",dl2)
	def pollMain():
		for ind in range(20):
			#print ">>> Dispatching %s" % ind
			ml.dispatch(TestEvent("test_evt",text='event 1-%s' % ind),cb=render)
			#ml.dispatch(TestEvent("test_evt",data={'text':'event 1a-%s' % ind}),cb=render)
			ml.dispatch(TestSubEvent("test_evt2",text='event 2-%s' % ind),cb=render)
			time.sleep(2)
		ml.stop()
	thread.start_new_thread(pollMain,(),{})
	print 'entering mainloop'
	ml._run=True
	ml.mainloop()
	print 'exit'

