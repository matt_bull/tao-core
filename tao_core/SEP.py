# Simple Event Protocol implementation

#   Copyright 2011 Matthew Bull

#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# SEP protocol outline....
# <opcode / flags><arg count><sencoded arg 1>
# see below for meaning of body args depending on opcode
from __future__ import absolute_import
from . import sigIO,tdf_core,sencode
import thread,weakref,gc,sys

OP_REG=0x01 # -> register for events of name <arg1>
OP_UNREG=0x02 # -> unregister for events of name <arg1>
OP_EVT=0x03 # -> event for peer dispatcher <arg1> with name <arg2> and event dict <arg3> and optionally id of <arg4> (if sync dispatch)
OP_CB=0x04 # -> event callback with id <arg1>, event dict of <arg2> and exception tb (if any) of <arg3>
OP_CONT=0x05 # -> continue previously incomplete event (eg one without FL_FINAL set)
OP_REL=0x06 # -> release a dispatcher in peer with id <arg1>

FL_EXC=0x40 # -> dispatch generated remote exception (only valid for OP_CB)
FL_CB=0x40 # -> op requires callback (only valid for OP_EVT)
FL_FINAL=0x80 # -> set if this is the final (or only packet) in an event or callback

OP_MASK=0x1f

class SepBuffer(sigIO.SimpleBuffer):
	def __init__(self,proxy_src=None):
		sigIO.SimpleBuffer.__init__(self,encoding=None)
		self.arg_cnt=0
		self.header=None
		self.args=[]
		self.part=''
		self.proxy_src=proxy_src
	def feed(self,data):
		self.part=self.part+data
		#print "feed ->",len(self.part),self.proxy_src
		while len(self.part)>=3:
			if not self.header:
				self.header=self.part[0]
				self.arg_cnt=ord(self.part[1])
				self.part=self.part[2:]
				self.args=[]
				#print ord(self.header),self.arg_cnt
			while len(self.args)<self.arg_cnt:
				consumed,adata=sencode.decode(self.part,ref_proxy=self.proxy_src)
				#print consumed,adata
				if consumed==0:
					return
				self.part=self.part[consumed:]
				self.args.append(adata)
			self.append((ord(self.header),self.args))
			self.header=None
		#print len(self)
		return len(self)

class DispatchProxy(object):
	def __init__(self,peer_id,channel):
		self._dsp=weakref.ref(channel)
		self.peer_id=peer_id
		#self.accept_events=list(peer['accept_events'])
	def __call__(self,evt,cb=None):
		chan=self._dsp()
		if chan:
			chan(evt,cb=cb,peer_id=self.peer_id)
		else:
			raise ReferenceError
	def __del__(self):
		#print "---deleting proxy"
		chan=self._dsp()
		try:
			chan.releaseRef(self.peer_id)
		except:
			pass

class SepProtocol(sigIO.Protocol,tdf_core.TrackDispatcher):
	def __pinit__(self,**kwargs):
		tdf_core.TrackDispatcher.__init__(self,dispatcher=self._dispatcher)
		sigIO.Protocol.__pinit__(self)
		self.in_buffer=SepBuffer(proxy_src=self.getProxy)
		self.producer=sigIO.SimpleProducer(encoding=None)	# make sure encoding is None... we are dealing with binary data
		self.op_map={} # map of events we have generated to id's
		self.ev_map={} # map of id's to events we have dispatched
		self.id_mutex=thread.allocate_lock() # lock to control access to the above 2
		self.proxied_map={0:self._dispatcher}
	def onData(self):
		#print "generate ->",len(self.in_buffer)
		for opcode,args in self.in_buffer:
			#print hb
			#print opcode,args
			op=opcode & OP_MASK
			if op==OP_REG:
				self._dispatcher.registerDispatcher(args[0],self)
			elif op==OP_UNREG:
				self._dispatcher.unregisterDispatcher(args[0],self)
			elif op==OP_EVT:
				evt=tdf_core.SimpleEvent(args[1],**args[2])
				if opcode & FL_CB:
					self.op_map[evt]=args[3]
					tdf_core.dispatchWithCallback(self.proxied_map[args[0]],evt,self.onCallback)
				else:
					self.proxied_map[args[0]](evt)
			elif op==OP_CB:
				self.id_mutex.acquire()
				#print "callback with args %s" % args
				if self.ev_map.has_key(int(args[0])):
					evt=self.ev_map.pop(int(args[0]))
					self.id_mutex.release()
					evt.update(args[1])
					if len(args)>2:
						self.finish(evt,exc=args[2])
					else:
						self.finish(evt)
				else:
					# oooppppsss....
					self.id_mutex.release()
					raise KeyError('no callback / event registered for id %s' % args[0])
			elif op==OP_REL:
				try:
					del(self.proxied_map[args[0]])
				except:
					pass
	def registerDispatcher(self,evt_name,dispatcher,**kwargs):
		# note this doesn't actually map the dispatcher... you need SEPMappingProxy below for that
		# this just deals with the network side of things to make sure the events are sent... they are
		# all sent to self._dispatcher
		if self._dispatcher and dispatcher!=self._dispatcher:
			raise AttributeError("attempt to change dispatcher in Sep channel...")
		elif not self._dispatcher:
			self._dispatcher=dispatcher
		self.send(chr(OP_REG | FL_FINAL)+'\x01\x04'+evt_name+'\x00')
	def removeDispatcher(self,evt_name,dispatcher):
		self.send(chr(OP_UNREG | FL_FINAL)+'\x01\x04'+evt_name+'\x00')
	def __call__(self,evt,cb=None,peer_id=0):
		#print "dispatch in ->",self
		#tdf_core.TrackDispatcher.dispatch(self,evt,cb=cb)
		if cb:
			self.cb_map[evt]=cb
			op_fl=OP_EVT | FL_CB
			e_id=self.getId(evt)
			#print "==>send",len(chr(op_fl)+'\x03'+sencode2.encode(evt.name,sencode2.TYPE_STR)+sencode2.encode(evt.getDict(),sencode2.TYPE_DICT)+sencode2.encode(e_id))
			self.send(chr(op_fl)+'\x04'+sencode.encode(peer_id,sencode.TYPE_LONG)+sencode.encode(evt.name,sencode.TYPE_STR)+sencode.encode(evt.getDict(),sencode.TYPE_DICT,ref_map=self.proxied_map)+sencode.encode(e_id))
			return True
		else:
			op_fl=OP_EVT
			self.send(chr(op_fl)+'\x03'+sencode.encode(peer_id,sencode.TYPE_LONG)+sencode.encode(evt.name,sencode.TYPE_STR)+sencode.encode(evt.getDict(),sencode.TYPE_DICT,ref_map=self.proxied_map))
	def onCallback(self,event,exc=None):
		if self.op_map.has_key(event):
			if exc:
				self.send(chr(OP_CB | FL_EXC)+'\x03'+sencode.encode(self.op_map.pop(event))+sencode.encode(event.getDict(),sencode.TYPE_DICT,ref_map=self.proxied_map)+sencode.encode(repr(exc),sencode.TYPE_STR))
			else:
				#print "SEP rendering ->",self.op_map[event],event.getDict()
				self.send(chr(OP_CB)+'\x02'+sencode.encode(self.op_map.pop(event))+sencode.encode(event.getDict(),sencode.TYPE_DICT,ref_map=self.proxied_map))
		else:
			raise KeyError('no Id for event %s' % repr(event))
	def getId(self,event):
		self.id_mutex.acquire()
		self.ev_map[id(event)]=event
		self.id_mutex.release()
		return id(event)
	def getProxy(self,peer_id):
		return DispatchProxy(peer_id,self)
	def releaseRef(self,peer_id):
		self.send(chr(OP_REL)+'\x01'+sencode.encode(peer_id,sencode.TYPE_LONG))
	def onClose(self):
		#print "onClose...",self
		del(self.in_buffer)
		self.id_mutex.acquire()
		for evt in self.ev_map.values():
			self.finish(evt,exc=IOError('connection to remote dispatcher closed'))
		self.id_mutex.release()
		self._dispatcher.removeAllDispatcher(self.dispatch)

cb_cnt=0
remote_dispatcher=None
remote_cnt=0
if __name__=="__main__":
	import sys,time
	from . import sigIO_net,utils
	#import glib_core
	io=sigIO.getIO()
	io.start()
	if '-server' in sys.argv:
		remote_dispatcher=None
		def dispatcher(evt,cb=None):
			global remote_dispatcher,remote_cnt
			print evt.name,evt.getDict()
			if evt.has_key('foo'):
				evt['foo']="manipulated foo"
			elif evt.has_key('dispatch'):
				print evt['dispatch']
				remote_dispatcher=evt['dispatch']
			#print "dispatch ->",evt.name,evt['index']
		ch=io.newChannel(SepProtocol,sigIO_net.TCPInetTransport,dispatcher)
		ch.listen(('127.0.0.1',8080))
		while 1:
			if remote_dispatcher:
				try:
					remote_dispatcher(tdf_core.SimpleEvent('org.digitao.AsyncTestEvent',**{'async_remote':'Test','index':remote_cnt}))
				except ReferenceError:
					remote_dispatcher=None
					continue
				remote_cnt=remote_cnt+1
				if remote_cnt>10:
					remote_dispatcher=None
			try:
				utils.sleep(2)
			except KeyboardInterrupt:
				break
	else:
		print "do client"
		term_lock=thread.allocate_lock()
		term_lock.acquire()
		def remoteDispatch(evt,cb=None):
			print "dispatch from remote ->",evt.getDict()
			evt['remote_dispatch']=True
		def callback(evt,exc=None):
			global cb_cnt
			if exc:
				print "exception during dispatch...."
				print repr(exc)
			if cb_cnt>=99:
				term_lock.release()
				print "callback ->",evt['index'],time.time()-evt['st']
			else:
				cb_cnt=cb_cnt+1
				print "callback ->",evt['index'],cb_cnt
		ch=io.newChannel(SepProtocol,sigIO_net.TCPInetTransport)
		ch.connect(('127.0.0.1',8080))
		st=time.time()
		ind=0
		ch(tdf_core.SimpleEvent('org.digitao.AsyncTestEvent',**{'index':ind,'st':st,'dispatch':remoteDispatch}))
		#while time.time()<tt:
		while ind<100:
			utils.sleep(1)
			if time.time()>st+ind:
				try:
					ind=ind+1
					#ch(tdf_core.SimpleEvent('org.digitao.TestEvent',**{'foo':'bar','index':ind,'st':st}),cb=callback)
					#ch(tdf_core.SimpleEvent('org.digitao.AsyncTestEvent',**{'foo':'bar','index':ind,'st':st}))
				except KeyboardInterrupt:
					break
		while 1:
			utils.sleep(1)
			if term_lock.acquire(0):
				break
	print len(ch.out_buffer),len(ch.producer)
	ch.close()

