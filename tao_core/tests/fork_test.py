from tao_core.sigIO_local import ForkTransport
from tao_core import sigIO
from tao_core import tdf_core
import unittest
import time,thread,os

class TestEvent(tdf_core.SimpleEvent):
	# set a sensible default for key 'output'
	defaults={'output':'Error - Unknown error'}

class TestProtocol(sigIO.Protocol):
	# for text based protocols spilt on '\n'
	sep='\n'
	# this is a simple example using the ioloop to drive dispatch (although you could dispatch any form of event into any event loop...)
	def __pinit__(self):
		sigIO.Protocol.__pinit__(self)
		self.producer=sigIO.SimpleProducer()
	def onData(self):
		print "onData",self.in_buffer,self._dispatcher,os.getpid()
		if self._dispatcher:
			for line in self.in_buffer:
				# dispatch an event into dispatcher with line in key 'text'
				# we catch any exceptions here to report exception to peer
				try:
					self.put(TestEvent("TestEvent",text=line),sync=True)
				except Exception,exc:
					self.send("Server Error - %s" % repr(exc))
					# then of course reraise the exception so we get a traceback
					raise exc
		else:
			for line in self.in_buffer:
				print "Got Data -> %s" % line
	def onCallback(self,event,exc=None):
		print "onCallback",self._dispatcher,os.getpid()
		## just send output with any whitespace stripped
		# we can rely on key 'output' existing as it is in the events defaults
		# really we should do something with exception here (if it exists)
		self.send(event['output']+'\n')

# create a quick and dirty handler for the resulting events.....
def TestHandler(event,cb=None):
	print "handler -> %s" % event['text']
	event['output']="Got %s" % event['text']


class Test(unittest.TestCase):
	def setUp(self):
		self.io=sigIO.getIO()
		self.io.start()
		time.sleep(3)
		self.count=1
		self.channel=self.io.newChannel(TestProtocol,ForkTransport)
		self.child_flag=self.channel.connect(TestHandler)
		self.lock=thread.allocate_lock()
		self.lock.acquire()
		self.lines=[]
		#time.sleep(1)
	def tearDown(self):
		self.channel.close()
		self.io.stop()
	def testName(self):
		if self.child_flag:
			while self.count:
				time.sleep(20)
		else:
			self.channel.resp_cb=self.callback
			thread.start_new_thread(self.sendLines,())
			self.lock.acquire()
	def callback(self,line):
		print "callback",line,self.lines
		line=line.split(' ',1)
		self.assertEqual(line[0],'Got')
		self.assertIn(line[1],self.lines)
		self.lines.remove(line[1])
		if not self.lines:
			self.lock.release()
	def sendLines(self):
		for ind in range(self.count):
			self.channel.send('test packet %s\n' % ind)
			self.lines.append('test packet %s' % ind)
		print self.lines

if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	#unittest.main()
	io=sigIO.getIO()
	io.start()
	time.sleep(1)
	def sendlines(ch,cnt):
		while cnt:
			ch.send('test packet %s\n' % cnt)
			cnt=cnt-1
			time.sleep(1)
			print cnt
	def printResult(evt,cb=None):
		print "RESULT -> %s" % event['text']
	count=10
	channel=io.newChannel(TestProtocol,ForkTransport)
	child_flag=channel.connect(TestHandler)
	if child_flag:
		while 1:
			try:
				time.sleep(20)
			except KeyboardInterrupt:
				exit()
	else:
		#channel._dispatcher=printResult
		sendlines(channel,count)
		while 1:
			try:
				time.sleep(20)
			except KeyboardInterrupt:
				exit()
		
