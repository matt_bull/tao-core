# signals based async IO


#   Copyright 2011 Matthew Bull

#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# base classes for signals based Async IO this should be fine under linux, and possibly other Unices.
# a full channel is made by using main.newChannel(<proto_class>,<transport_class>).
# By convention transport methods are all <doSomthing> and protocol methods are <onSomething>, most transports should
# already be implemented in the sigIO_xxx modules

# Protocol classes should implement the protocol used preferably in a completely transport independent way,
# its up to the programmer how to split the load between the Buffer class and the onData method of the Protocol class
# but generally speaking doing as little as possible in the Buffer is preferable for readability and maintainability
# its also up to the programmer how much of the protocol logic to implement in the Protocol class, in this case
# the more done in the Protocol class, generally the clearer the code, also you can take advantage of per connection state.
# eg for smtp, events should be generated for each individual email not each line of the smtp conversation

# there are currently two main classes which provide an IO loop...
# the generic driver which has no dependencies outside the standard python modules
# and the GTK main loop which integrates with the IO signal handling built into the
# gobject library... to use the GTK mainloop call sigIO.setEnv('gtk')

# FYI channels are also prefectly valid tdf_core sources, (see tdf_core.py) but aren't a subclass to reduce dependencies..
# however if you intend to use the gtk mainloop and tdf you should use gtk_core.py or glib_core.py which combines Channel and tdf's SyncSource to properly
# synchronise the callbacks, gtk_core with pygtk and glib_core for the later pygi bindings

# One Gotcha when it comes to sigIO and tdf_core, you may find a need to pass a dispatcher as an event item and think that the simplest way is to pass a method of your
# protocol class with the correct signature (event,cb=None) this will work fine... except that the channel is never destroyed and dispatch throws errors...
# for this use case, use tdf_core.DispatchProxy(self,'<method name>') and pass that to your event instead, your channel will disappear on close and the proxy will
# get deleted from any dispatchers its registered with at the first dispatch thereafter... either that or use ModifyGraph events to do all your graph changes,
# and make sure that you unregister in onClose....

import signal,thread,fcntl,os,select,time,traceback,sys,codecs
from utils import CompositeException
from os import O_NONBLOCK

#try:
#	from gi.repository import GLib
#except ImportError:
GLib=None

O_ASYNC=8192

ST_CLOSED=0
ST_CONNECTING=1
ST_LISTENING=2
ST_CONNECTED=3
ST_CLOSING=4
ST_ERROR=5

def log(msg):
	l_f=open('IO_log','a')
	l_f.write('[%s] %s\n' % (os.getpid(),msg))
	l_f.close()

class HoldIteration(Exception):
	""" raised by buffers when there is no more data (yet)"""
	pass

def FileProducer(p_f):
	# p_f should be an open readable file object....
	c_size=4096
	# default chunk size
	while 1:
		p=p_f.read(c_size)
		if len(p)<c_size:
			# EOF
			yield p
			break
		r_size=yield p
		if r_size:
			# the channel requested a new chunk size...
			c_size=r_size
	p_f.close()

class SimpleProducer(list):
	def __init__(self,encoding='UTF-8'):
		self.encoding=encoding
		list.__init__(self)
	def __iter__(self):
		return self
	def next(self):
		try:
			return self.pop(0)
		except IndexError:
			raise StopIteration
	def send(self,*args):
		"""to match generators.... we ignore this"""
		return self.next()
	def append(self,uni_str):
		if self.encoding:
			list.append(self,codecs.encode(uni_str,self.encoding))
		else:
			# an encoding of None means binary data...
			list.append(self,uni_str)

class FifoProducer(SimpleProducer):
	def __init__(self,init_data=[],p_type=str):
		SimpleProducer.__init__(self,init_data)
		self.curr_producer=None
		self.p_type=p_type
	def __nonzero__(self):
		# override for existence testing
		if self.curr_producer:
			return True
		else:
			return SimpleProducer.__len__(self)
	def next(self):
		if self.curr_producer:
			try:
				return self.curr_producer.next()
			except StopIteration:
				self.curr_producer=None
				return self.next()
		else:
			try:
				pack=self.pop(0)
			except IndexError:
				raise StopIteration()
			#print type(pack),self.p_type
			if type(pack)==self.p_type:
				return pack
			else:
				try:
					self.curr_producer=iter(pack)
				except:
					raise TypeError("Incorrect packet type %s, must be %s, or iterable" % (type(pack),self.p_type))
				return self.next()

# Buffers break incoming data up into valid chunks (lines, packets, etc) and return
# True from feed if there is valid data, (triggering a call to onData() in the protocol class)

class SimpleBuffer(SimpleProducer):
	"""really simple buffer expects data to be pre packetized... like from a UDP transport or as an output buffer"""
	def __init__(self):
		SimpleProducer.__init__(self,encoding=None)
	def feed(self,packet):
		# feed data from transport used on input buffers
		if packet:
			self.append(packet)
			return True
	def push(self,packet):
		# push data from protocol used on output producers
		# we don't assume to know what form packet may take...
		list.append(self,packet)

class Buffer(SimpleProducer):
	"""breaks up data by a seperator, sep, which can be a single char or a string"""
	def __init__(self,sep='\n'):
		self.sep=sep
		self.part=""
		SimpleProducer.__init__(self)
	def feed(self,data):
		# feed data from transport used on input buffers
		#print "feed",repr(data),repr(self.sep)
		self.part=self.part+data
		if self.sep in self.part:
			data,self.part=self.part.rsplit(self.sep,1)
			self.extend(data.split(self.sep))
			#print repr(self)
			return True
	def push(self,packet):
		# ensure that outbound data has the seperator on the end
		if not packet.endswith(self.sep):
			packet=packet+self.sep
		self.append(packet)

class LateBoundBuffer:
	""" A buffer class which doesn't split on feed, but instead splits on next...
	really useful for mixed line oriented / length oriented protocols, but
	less efficient as onData is always called.....
	"""
	def __init__(self,sep='\n'):
		self.sep=sep
		self.buf=""
	def __iter__(self):
		return self
	def __len__(self):
		if self.sep:
			return len(self.buf.split(self.sep))
		else:
			return len(self.buf)
	def next(self,sep=None,plen=None):
		if not sep:
			sep=self.sep
		if plen:
			if len(self.buf)>plen:
				part=self.buf[:plen]
				self.buf=self.buf[plen:]
				return part
			elif not self.buf:
				raise StopIteration()
			else:
				part=self.buf[:]
				self.buf=""
				return part
		elif self.buf and sep:
				if sep in self.buf:
					part,self.buf=self.buf.split(sep,1)
					return part
				else:
					raise StopIteration()
		elif self.buf:
			return self.buf
		else:
			raise StopIteration()
	def peek(self,sep=None,plen=None):
		if not sep:
			sep=self.sep
		if plen:
			if len(self.buf)>plen:
				return self.buf[:plen]
			else:
				return self.buf[:]
		elif self.buf and sep:
				if sep in self.buf:
					return self.buf.split(sep,1)[0]
		elif self.buf:
			return self.buf
	def feed(self,packet):
		self.buf=self.buf+packet
		return len(self.buf)

class Channel(object):
	def __init__(self,dispatcher,io_serv=None,trans_args={},proto_args={}):
		# dispatcher here is a tdf_core style dispatcher
		self._dispatcher=dispatcher
		self.io_serv=None
		if io_serv:
			io_serv.addChannel(self)
		self.peer=None
		self.exit_handler=None
		self.state=ST_CLOSED
		self.__tinit__(**trans_args)
		self.__pinit__(**proto_args)
		# cache the proto args for spawned channels
		self.__pa_cache__=proto_args
		#self.out_interlock=thread.allocate_lock()
	def readwrite(self,fd,flags):
		#print "readwrite in %s -> %s - %s" % (self,flags,self.state)
		#print self.producer
		if self.state==ST_CONNECTING and (flags & select.POLLIN or flags & select.POLLPRI):
			self.state=ST_CONNECTED
			self.onConnect(self.peer)
		elif flags & select.POLLERR or flags & select.POLLHUP or flags & select.POLLNVAL:
			# OK we've seen the error and are taking action... unregister the fd for now (to stop a runaway)..
			#print fd,flags
			#self.setFlags(fd,None)
			self.doError(fd,flags)
		elif self.state in (ST_CONNECTED,ST_LISTENING,ST_CLOSING):
			if (flags & select.POLLIN or flags & select.POLLPRI) and self.state!=ST_CLOSING:
				#print ">>>>> doInput"
				if self.doInput(fd):
					#print ">>>>> onData"
					self.onData()
			if flags & select.POLLOUT:
				# quick shortcut just to make sure there is something in out_buffer to send...
				if (not self.out_buffer) and self.producer:
					#print "getting from producer",self.producer
					try:
						self.out_buffer=self.producer.next()
					except StopIteration:
						self.finishOutput(fd=fd)
				if not self.doOutput(fd):
					if self.producer:
						try:
							self.out_buffer=self.producer.next()
						except StopIteration:
							self.finishOutput(fd=fd)
					else:
						self.finishOutput(fd=fd)
			elif self.state==ST_CLOSING:
				# we have been marked as closing and there is no output....
				self.close(force=True)
		else:
			# WTF??
			sys.stderr.write("readwrite called in %s with unknown state %s-%s, unregistering fd" % (repr(self),flags,self.state))
			self.setFlags(fd,None)
	# refactored to onX and doX methods
	def send(self,packet,address=None):
		try:
			self.onSend(packet)
			self.doSend()
		except:
			traceback.print_exc()
	#def send(self,packet):
	#	#self.out_interlock.acquire()
	#	if self.producer!=None:
	#		try:
	#			self.producer.append(packet)
	#		except AttributeError:
	#			raise ValueError("unable to append objects to producer of type %s" % repr(self.producer))
	#	elif self.sep and type(packet)==str:
	#		self.out_buffer=packet+self.sep
	#	elif type(packet)==str:
	#		self.out_buffer=self.out_buffer+packet
	#	else:
	#		raise TypeError("Channels with no producer can only handle strings")
	#	self.doSend()
	#	#self.out_interlock.release()
	def put(self,event,sync=False):
		# <sync> should indicate how to handle the event....
		# True if packet generating event requires a response
		# False otherwise
		# this is identical to tdf_core source but is copy pasted to prevent dependency
		if sync:
			try:
				dis_res=self._dispatcher(event,cb=self.onCallback)
			except StopIteration:
				self.onCallback(event)
			except Exception:
				self.onCallback(event,exc=CompositeException())
			else:
				#print 'no exception...',dis_res
				if not dis_res:
					self.onCallback(event)
		else:
			try:
				self._dispatcher(event)
			except:
				traceback.print_exc()
	def enable(self,io_serv):
		self.io_serv=io_serv
		# if you don't explicitly pass in dispatcher...
		# use the io_server as a dispatcher...
		#if not self._dispatcher:
		#	self._dispatcher=io_serv
		self.onEnable()
	def setFlags(self,fd,flags):
		if self.io_serv:
			self.io_serv.setFlags(fd,flags)
		elif self.state==ST_CLOSED:
			return
		else:
			#print "channel state ->",self.state
			#sys.stderr.write("setFlags on unregistered channel %s, call IOService.addChannel first" % repr(self))
			raise IOError("setFlags on unregistered channel, call IOService.addChannel first")
	def registerFd(self,fd):
		if self.io_serv:
			self.io_serv.registerFd(fd,self)
		else:
			sys.stderr.write("registerFd on unregistered channel, call IOService.addChannel first")
	def setDispatcher(self,dsp):
		self._dispatcher=dsp
	def setExitHandler(self,exit_handler):
		self.exit_handler=exit_handler
	def spawnChannel(self,trans_args={}):
		new_ch=self.__class__(self._dispatcher,io_serv=self.io_serv,trans_args=trans_args,proto_args=self.__pa_cache__)
		# if we have an exit handler propegate
		if self.exit_handler:
			new_ch.setExitHandler(self.exit_handler)
		# call onSpawn on the new channel....
		try:
			new_ch.onSpawn(self)
		except Exception,err:
			sys.stderr.write("[ERROR] channel exception %s" % err)
		return new_ch
	def close(self,force=False):
		#print "closing %s -> %s - %s" % (repr(self),self.state,force)
		# dereference dispatcher here to prevent circular refs...
		if self.state==ST_CLOSED:
			self._dispatcher=None
			if self.exit_handler:
				try:
					self.exit_handler(self,force)
				finally:
					self.exit_handler=None
			if self.io_serv:
				for fd in self.getFd():
					self.setFlags(fd,None)
					self.io_serv.unregisterFd(fd)
			try:
				self.doClose()
			except:
				pass
			self.io_serv=None
			return
		elif force:
			self.state=ST_CLOSED
			#try:
			#	self.onClose()
			#except:
			#	pass
			self.close(force=force)
		elif self.state not in (ST_CLOSING,ST_CLOSED):
			# set state to closing so when all output sent channel will close....
			self.state=ST_CLOSING
			try:
				hold=self.onClose()
			except Exception,exc:
				self.debug("error %s gracefully closing channel" % exc)
				hold=False
			if not hold:
				# our protocol does not have to send anything... close unilaterally
				self.state=ST_CLOSED
				self.close()
	#def __del__(self):
	#	print "destruction of channel"
	def debug(self,msg):
		# override to print or log debug messages.....
		#print msg
		pass

class Protocol(object):
	sep=None
	# __name__ basically exists to enable debugging that makes sense
	# it should be short and descriptive (no need to include 'protocol')
	# eg. smtp -> SMTP, jabber client -> XMPPClient, 
	__name__="Protocol"
	__in_buffer_class__=None
	sep=None
	def __pinit__(self,**kwargs):
		# kwargs should be proto_args handed to channel
		# override to set up relevant buffers....
		if self.sep:
			self.in_buffer=Buffer(self.sep)
		elif self.__in_buffer_class__:
			self.in_buffer=self.__in_buffer_class__()
		else:
			self.in_buffer=SimpleBuffer()
		# this is OK if you are dealing with one output packet at a time
		# (eg a "conversation" style protocol), all others should use a producer...
		self.producer=None
		self.out_buffer=""
	def onSend(self,packet):
		#print "onSend",self,packet,self.producer
		if self.producer!=None:
			try:
				self.producer.append(packet)
			except AttributeError:
				raise ValueError("unable to append objects to producer of type %s" % repr(self.producer))
		elif type(packet) in (str,unicode):
			if self.sep and not packet.endswith(self.sep):
				self.out_buffer=self.out_buffer+packet+self.sep
			else:
				self.out_buffer=self.out_buffer+packet
		else:
			raise TypeError("Channels with no producer can only handle strings not",type(packet))
		#print "!!onSend",self.producer
	def onData(self):
		# override to generate events from self.in_buffer and call self.put(event,sync=<sync>) to dispatch
		# note that if you want to pass a method of your instance as a dispatcher, wrap it first in DispatchProxy
		# ie. <Event>(dispatcher=DispatchProxy(<instance method>))
		# to prevent circular refs which hang up the channel on close
		pass
	def onCallback(self,event,exc=None):
		# override to render events into a producer or direct to self.out_buffer by calling self.send(<packet>)..
		# exc is any exception raised during dispatch, or the last exception if multiple exceptions raised...
		# catch circular refences here...
		if self in event.values():
			for key,value in event.items():
				if value==self:
					event[key]=None
	def onConnect(self,address):
		# override if something needs to happen when a (usually client) channel first connects...
		pass
	def onClose(self):
		# override to close connection gracefully (eg in SMTP send QUIT\n....)
		# return True if you have added output that should be processed
		pass
	def onEnable(self):
		# rarely used called just after the channel is enabled...
		pass
	def onSpawn(self,channel,**kwargs):
		#also rarely used but really useful.... called by transports that spawn new channels from a listener
		# eg TCP and bluetooth RFCOMM sockets.... called on the spawned channel immediately after initialisation...
		# <channel> is the channel object that spawned us... kwargs allows that channel to pass us extra useful data.
		pass
	def onIdle(self):
		# override to do something when there is nothing else to send, return true if you have sent anything...
		return False

class Transport(object):
	__name__="Transport"
	def __tinit__(self,**kwargs):
		# NOOP kwargs should be related to trans_args handed to channel
		# override to create underlying transport....
		self.dev=None
	def getFd(self):
		# return a list of fd's used....
		if self.dev:
			return (self.dev.fileno(),)
	def connect(self,address):
		# override to connect channel to <address> and set fd flags ready for input and output
		#print "Transport.connect"
		fd=self.getFd()
		self.registerFd(fd[0])
		self.peer=address
		self.setFlags(fd[0],select.POLLIN | select.POLLHUP | select.POLLERR | select.POLLNVAL)
		if len(fd)>1:
			self.registerFd(fd[1])
			self.setFlags(fd[1],select.POLLHUP | select.POLLERR | select.POLLNVAL)
		self.state=ST_CONNECTED
		try:
			self.onConnect(address)
		except Exception,err:
			sys.stderr.write("[ERROR] channel exception %s" % err)
	def listen(self,address):
		# override to listen on <address> and set fd flags ready for input
		fd=self.getFd()
		self.registerFd(fd[0])
		#self.peer=address
		self.setFlags(fd[0],select.POLLIN | select.POLLHUP | select.POLLERR | select.POLLNVAL)
		if len(fd)>1:
			self.registerFd(fd[1])
			self.setFlags(fd[1],select.POLLHUP | select.POLLERR | select.POLLNVAL)
		self.state=ST_LISTENING
	def doInput(self,fd):
		# we have a readable fd do the read here and feed input buffer return whatever buffer object returns from feed()...
		pass
	def doOutput(self,fd):
		# we have a writeable fd do a write from the output buffer, return length of remaining output buffer after write
		pass
	def doError(self,fd,flags):
		# we have an error on an fd...
		sys.stderr.write("IO ERROR: error on fd %s - %s.... closing" % (fd,flags))
		print "IO ERROR: error on fd %s - %s.... closing" % (fd,flags)
		self.close(force=True)
	def doSend(self):
		# new stuff added to our output buffer.. start sending it..
		#print "doSend"
		fd_t=self.getFd()
		if not fd_t:
			self.close()
		if len(fd_t)>1:
			fd=fd_t[1]
		else:
			fd=fd_t[0]
		self.setFlags(fd,select.POLLOUT | select.POLLIN | select.POLLHUP | select.POLLERR | select.POLLNVAL)
	def doSpawn(self):
		# override to register fds etc....
		pass
	def finishOutput(self,fd=None):
		# we have exhausted our output buffer set the output fd to no longer writeable... or if closing set state to closed and unregister fd.
		#print "finishOutput"
		if not fd:
			fd_t=self.getFd()
			if len(fd_t)>1:
				fd=fd_t[1]
			else:
				fd=fd_t[0]
		if self.state==ST_CLOSING:
			# woah we are set to closing... set state to ST_CLOSED and recall close to close cleanly
			print "closing due to state in finishOutput"
			self.state=ST_CLOSED
			self.close()
			return
		if not self.onIdle():
			self.setFlags(fd,select.POLLIN | select.POLLHUP | select.POLLERR | select.POLLNVAL)
	def doClose(self):
		# close transport note that by this point all fds are deregistered we should just close the underlying transport at this point
		pass
	def doValidate(self):
		# used in encrypted transports, must attempt to validate peer (if not already validated) and return peer info.
		# returns dict... {'name':<peer name>,'domain':<peer domain>,'verified':<the identity has been verified>} plus validation specific keys.
		pass

class IOMain:
	def __init__(self):
		self.map={}
		self.ch_map={}
		self.idle_set=[]
	def start(self):
		#override to start IO loop / register IOMain with mainloop...
		pass
	def stop(self):
		# override to stop IO loop
		pass
	def restart(self):
		# override to restart io_loop
		pass
	def release(self):
		pass
	def run(self):
		pass
	def newChannel(self,proto_class,trans_class,dispatcher=None,proto_args={},trans_args={}):
		# this next bit looks hairy/scary and I'm sure could be done cleaner with metaclasses...
		# but is basically the equivalent to...
		# class NewChannel(Channel,Protocol,Transport):
		# 	pass
		# although it does some derivation with __name__..
		# and gives us (the main class) a chance to change the Channel base class used.
		# override to modify channel creation (in particular the Channel base class used)
		#print "WTF -->>",proto_class,trans_class
		cls_name=proto_class.__name__+'__'+trans_class.__name__
		if not self.ch_map.has_key(cls_name):
			self.ch_map[cls_name]=type(cls_name,(Channel,proto_class,trans_class),{'__name__':cls_name})
		nch=self.ch_map[cls_name](dispatcher,proto_args=proto_args,trans_args=trans_args)
		self.addChannel(nch)
		return nch
	def addChannel(self,channel):
		#if hasattr(channel,'onIdle'):
		#	self.idle_set.append(channel)
		channel.enable(self)
	def registerFd(self,fd,channel):
		self.map[fd]=channel
		fl=fcntl.fcntl(fd,fcntl.F_GETFL)
		fcntl.fcntl(fd,fcntl.F_SETFL,fl | O_ASYNC | O_NONBLOCK)
	def unregisterFd(self,fd):
		try:
			del(self.map[fd])
		except KeyError:
			# gone already... pass
			pass
	def setFlags(self,fd,flags=None):
		# set flags for <fd> (which should have already be registered via registerFd)
		# using flags <flags> (which should consist of a combination of select.POLL* constants)
		pass

class GenericIOMain(IOMain):
	def __init__(self):
		IOMain.__init__(self)
		self.p_obj=select.poll()
		self.io_lock=thread.allocate_lock()
		self._enabled=False
		self.running=thread.allocate_lock()
	def start(self):
		# this _MUST_ be called from the main thread, everything else is thread safe
		#print "sigIO start",os.getpid()
		if self.running.acquire(0):
			signal.signal(signal.SIGIO,self.handle_sig)
			signal.signal(signal.SIGURG,self.handle_sig)
			signal.signal(signal.SIGCHLD,self.handle_sig)
			signal.signal(signal.SIGALRM,self.handle_sig)
			signal.signal(signal.SIGPIPE,self.handle_sig)
			# register handling process for all existing fds...
			for fd in self.map.keys():
				fcntl.fcntl(fd,fcntl.F_SETOWN,os.getpid())
			self._enabled=True
			# spawn a thread to run the loop
			thread.start_new_thread(self.loop,(),{})
	def run(self):
		# this is basically just start with a pseudo mainloop
		# to hold up the main thread....
		self.start()
		while 1:
			try:
				time.sleep(6000)
			except KeyboardInterrupt:
				break
		self.stop()
	def stop(self):
		for channel in self.map.values():
			if channel.state not in (ST_CLOSING,ST_CLOSED):
				try:
					channel.close()
				except:
					pass
		print "[sigIO.stop] set all channels to closing terminate loop"
		self._enabled=False
		# release io_loop so it will terminate...
		self.release()
		sys.stderr.write("============ waiting on io loop exit\n")
		self.running.acquire()
		self.running.release()
	def release(self):
		try:
			self.io_lock.release()
		except thread.error:
			pass
	def restart(self):
		log('restart loop')
		self._enabled=False
		try:
			self.io_lock.release()
		except thread.error:
			pass
		self.running.release()
		time.sleep(0.1)
		# we reaquire and release the lock here to ensure that
		# mainloop has terminated before we restart it
		log('restart loop reestablishing signals')
		if self.running.acquire(0):
			signal.signal(signal.SIGIO,self.handle_sig)
			signal.signal(signal.SIGURG,self.handle_sig)
			signal.signal(signal.SIGCHLD,self.handle_sig)
			signal.signal(signal.SIGALRM,self.handle_sig)
			signal.signal(signal.SIGPIPE,self.handle_sig)
			self.map={}
			# register handling process for all existing fds...
			#for fd in self.map.keys():
			#	fcntl.fcntl(fd,fcntl.F_SETOWN,os.getpid())
			self._enabled=True
			# spawn a thread to run the loop
			log('restart loop starting loop')
			thread.start_new_thread(self.loop,(),{})
			return True
	def handle_sig(self,sig,frame):
		#print "[sigIO %s] signal %s" % (os.getpid(),sig)
		self.release()
	def registerFd(self,fd,channel):
		#print "registerFd (%s,%s)" % (fd,os.getpid())
		fcntl.fcntl(fd,fcntl.F_SETOWN,os.getpid())
		IOMain.registerFd(self,fd,channel)
	def setFlags(self,fd,flags=None):
		#print "setFlags(%s,%s)" % (fd,flags)
		# check fd is in map then set flags... if flags is None unregister the fd and remove from map
		if not self.map.has_key(fd):
			sys.stderr.write("file descriptor %s not in map\n" % fd)
			# unregister from poll object here to prevent runaway...
			try:
				self.p_obj.unregister(fd)
			except:
				pass
			return
		if flags==None:
			try:
				self.p_obj.unregister(fd)
			except KeyError:
				# its already unregistered....
				pass
		else:
			#print "registering fd %s with flags %s" % (fd,flags)
			#fcntl.fcntl(fd,fcntl.F_SETOWN,os.getpid())
			self.p_obj.register(fd,flags)
			self.release()
	def loop(self):
		closing_cache=[]
		while self._enabled or closing_cache:
			log("[sigIO %s] getting lock" % os.getpid())
			self.io_lock.acquire()
			log("[sigIO %s] got lock" % os.getpid())
			fdset=self.p_obj.poll(0)
			log("[sigIO %s] %s" % (os.getpid(),fdset))
			if len(fdset):
				for fd,flags in fdset:
					if self.map.has_key(fd):
						try:
							self.map[fd].readwrite(fd,flags)
						except StopIteration:
							pass
						except Exception,err:
							sys.stderr.write(repr(CompositeException(err)))
							#print "[ERROR] channel exception %s" % err
							#raise
					else:
						pass
						sys.stderr.write("[ERROR] event for unknown fd %s" % fd)
						# unregister from poll object here to prevent runaway...
						try:
							self.p_obj.unregister(fd)
						except:
							pass
				self.release()
			#elif len(self.idle_set):
			#	#print self.idle_set
			#	for ch in self.idle_set:
			#		try:
			#			ch.onIdle()
			#		except:
			#			self.idle_set.remove(ch)
			else:
				#print "lock on no io activity"
				#print self.map.values()
				# this means that no matter what 60 seconds after a channel is marked as closing...
				# it gets closed... is this still needed??
				#print self.map
				for channel in self.map.values():
					#print channel,channel.state
					if channel.state==ST_CLOSING:
						if channel in closing_cache:
							channel.close(force=True)
							closing_cache.remove(channel)
						else:
							closing_cache.append(channel)
				#print "cache",closing_cache
				if closing_cache and not self._enabled:
					#we have channels closing and we are in the middle of trying to stop.... speed up the loop
					signal.alarm(2)
			signal.alarm(30)
		# force close any channels that are still open
		#log("[sigIO %s] get lock on exit" % os.getpid())
		#self.io_lock.acquire()
		#log("[sigIO %s] exit loop" % os.getpid())
		for channel in self.map.values():
			try:
				if channel.state!=ST_CLOSED:
					channel.close(force=True)
			except Exception,err:
				print "Error closing channel"
				traceback.print_exc()
		self.release()
		self.running.release()

class GtkIOMain(IOMain):
	# Note: there is no mainloop here, you need to run the gobject mainloop in the main thread....
	# you also need to be careful if you are combining this with tdf_core (or threads / threaded dispatchers), better to use the classes in gtk_core.py instead
	def __init__(self):
		if GLib==None:
			raise ImportError('Could not import GLib')
		IOMain.__init__(self)
		self.src_map={}
		GLib.idle_add(self.handleIdle)
	def setFlags(self,fd,flags=None):
		if self.src_map.has_key(fd):
			src_id=self.src_map.pop(fd)
			GLib.source_remove(src_id)
		if flags!=None:
			# convert flags....
			gflags=GLib.IO_NVAL
			if flags & select.POLLIN:
				gflags=gflags | GLib.IO_IN
			if flags & select.POLLOUT:
				gflags=gflags | GLib.IO_OUT
			if flags & select.POLLERR:
				gflags=gflags | GLib.IO_ERR
			if flags & select.POLLPRI:
				gflags=gflags | GLib.IO_PRI
			if flags & select.POLLHUP:
				gflags=gflags | GLib.IO_HUP
			self.src_map[fd]=GLib.io_add_watch(fd,gflags,self.handleIO,priority=GLib.PRIORITY_DEFAULT)
	def handleIO(self,fd,gflags):
		# convert gtk flags back to select
		#print fd,gflags
		flags=0
		if gflags & GLib.IO_IN:
			flags=flags | select.POLLIN
		elif gflags & GLib.IO_OUT:
			flags=flags | select.POLLOUT
		elif gflags & GLib.IO_ERR:
			flags=flags | select.POLLERR
		elif gflags & GLib.IO_PRI:
			flags=flags | select.POLLPRI
		elif gflags & GLib.IO_HUP:
			flags=flags | select.POLLHUP
		self.map[fd].readwrite(fd,flags)
		return True
	def addChannel(self,channel):
		if hasattr(channel,'onIdle'):
			self.idle_set.append(channel)
		channel.enable(self)
	def handleIdle(self):
		for ch in self.idle_set:
			try:
				ch.doIdle()
			except:
				self.idle_set.remove(ch)
		return True
	def stop(self):
		for channel in self.map.values():
			if channel.state not in (ST_CLOSING,ST_CLOSED):
				try:
					channel.close()
				except:
					pass
		for fd in self.src_map:
			GLib.src_remove(self.src_map[fd])

# this is a bit of a hack.... uses modules namespace to ensure only one IOloop.
# at the moment only the native loop and gtk based loop are supported...
io_main=None
io_main_class=GenericIOMain
io_env='python'

def setEnv(env,new_io_main=None):
	global io_main,io_main_class,io_env,GLib
	if io_main:
		raise Exception('Call to sigIO.setEnv failed, cannot replace existing mainloop')
	else:
		print "setting env to",env,new_io_main
		io_env=env
		if env=="gtk":
			import gobject as GLib
			io_main_class=GtkIOMain
			return
		elif env.startswith('gtk'):
			import gobject as GLib
		elif env.startswith('glib'):
			from gi.repository import GLib
		# this enables other modules to override this on import....
		if new_io_main:
			io_main_class=new_io_main

def getIO():
	global io_main,io_main_class
	if not io_main:
		io_main=io_main_class()
	return io_main

