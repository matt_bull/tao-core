# base transport classes for sigIO

#   Copyright 2011 Matthew Bull

#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from sigIO import *
import subprocess,termios,struct,copy,sys
import traceback

# constants used by the serial transport
TIOCM_RTS_str = struct.pack('I', termios.TIOCM_RTS)
TIOCM_DTR_str = struct.pack('I', termios.TIOCM_DTR)


class PipeTransport(Transport):
	""" transport for named pipes, or a pair of named pipes if you use listen and rdwr is True"""
	__name__="Pipe"
	def __tinit__(self,create=True,rdwr=False):
		# meaning of the kwargs... 
		# <create> if set True creates fifo at address if it doesn't already exist during listen,
		# <rdwr> if set True uses a pair of pipes (created if create is also true), an input pipe at the orginal address and
		# an output pipe at address+.out (eg. /tmp/mypipe -> /tmp/mypipe.out)
		# (neither has an effect on connect)
		self.create=create
		self.rdwr=rdwr
		self.inp_fd=None
		self.out_fd=None
	def getFd(self):
		fds=[]
		if self.inp_fd:
			fds.append(self.inp_fd)
		if self.out_fd:
			fds.append(self.out_fd)
		return fds
	def connect(self,address):
		if os.access(address,os.W_OK):
			self.out_fd=os.open(address,os.O_NONBLOCK | os.O_RDWR)
		else:
			raise IOError('file %s does not exsist' % address)
		self.registerFd(self.out_fd)
		self.setFlags(self.out_fd,select.POLLHUP | select.POLLERR | select.POLLNVAL)
		self.peer=address
		self.onConnect(address)
		self.state=ST_CONNECTED
	def listen(self,address):
		if self.rdwr:
			if not ((os.access(address,os.R_OK) and os.access(address+'.out',os.W_OK)) or self.create):
				raise IOError('file %s does not exsist' % address)
			elif not (os.access(address,os.R_OK) and os.access(address+'.out',os.W_OK)):
				os.mkfifo(address)
				os.mkfifo(address+'.out')
			self.out_fd=os.open(address+'.out',os.O_NONBLOCK | os.O_RDWR)
			# we have to do this for ourselves as our fd is not available the channel is enabled
			self.io_serv.registerFd(self.out_fd,self)
			self.setFlags(self.out_fd,select.POLLHUP | select.POLLERR | select.POLLNVAL)
		else:
			if not (os.access(address,os.R_OK) or self.create):
				raise IOError('file %s does not exsist' % address)
			elif not os.access(address,os.R_OK):
				os.mkfifo(address)
		self.inp_fd=os.open(address,os.O_NONBLOCK | os.O_RDWR)
		self.io_serv.registerFd(self.inp_fd,self)
		self.setFlags(self.inp_fd,select.POLLIN | select.POLLHUP | select.POLLERR | select.POLLNVAL)
		self.state=ST_LISTENING
	def doInput(self,fd):
		#print "incoming!!!\n Man This takes me back to Nam..."
		return self.in_buffer.feed(os.read(fd,1024))
	def doOutput(self,fd):
		sent=os.write(fd,self.out_buffer)
		self.out_buffer=self.out_buffer[sent:]
		return len(self.out_buffer)
	def doClose(self):
		os.close(self.inp_fd)
		self.inp_fd=None
		os.close(self.out_fd)
		self.out_fd=None

class ForkTransport(Transport):
	""" use two anonymous pipes to connect a parent and child proccess"""
	__name__="ForkedProcess"
	def __tinit__(self,pc_pipe=None,cp_pipe=None):
		self.dev=None
		self.r_fd=None
		self.w_fd=None
		if pc_pipe:
			self.pc_pipe=pc_pipe
		else:
			self.pc_pipe=os.pipe()
		if cp_pipe:
			self.cp_pipe=cp_pipe
		else:
			self.cp_pipe=os.pipe()
		#print self.pc_pipe,self.cp_pipe
	def open(self,uid=None):
		p_pid=os.getpid()
		c_pid=os.fork()
		self.dev=True
		#print p_pid,c_pid,os.getpid()
		if c_pid:
			# parent process....
			self.r_fd=self.cp_pipe[0]
			self.w_fd=self.pc_pipe[1]
			os.close(self.cp_pipe[1])
			os.close(self.pc_pipe[0])
			self.peer=c_pid
			#self.io_serv.restart()
			#print "parent %s - read %s, write %s" % (os.getpid(),self.r_fd,self.w_fd)
		else:
			# child process.... a little more tricksy
			#print os.getpid()
			if uid:
				os.seteuid(uid)
			self.r_fd=self.pc_pipe[0]
			self.w_fd=self.cp_pipe[1]
			os.close(self.cp_pipe[0])
			os.close(self.pc_pipe[1])
			self.peer=p_pid
			#log('restarting io loop')
			# we have to give the IO service a bit of a reach around here 
			try:
				if not self.io_serv.restart():
					print 'failed to restart IO loop'
			except:
				print traceback.format_exc()
		# now register our fd's
		if not c_pid:
			return True
	def listen(self,uid=None):
		# listen... fork child process and treat as client....
		# listen returns True if child process.
		c_fl=self.open(uid=uid)
		if c_fl:
			self.state=ST_CONNECTED
			self.onConnect(os.getpid())
		else:
			self.state=ST_LISTENING
			# this is really a misuse of onSpawn... but works OK for most implementations
			self.onSpawn(self)
		self.registerFd(self.r_fd)
		self.registerFd(self.w_fd)
		self.setFlags(self.r_fd,select.POLLIN | select.POLLHUP | select.POLLERR | select.POLLNVAL)
		self.setFlags(self.w_fd,select.POLLHUP | select.POLLERR | select.POLLNVAL)
		return c_fl
	def connect(self,dispatcher,uid=None):
		# connect... fork child process and treat as server....
		if self.open(uid=uid):
			self._dispatcher=dispatcher
			self.state=ST_LISTENING
		else:
			self.state=ST_CONNECTED
			try:
				self.onConnect(self.peer)
			except Exception,err:
				sys.stderr.write("[ERROR] channel exception %s" % err)
		self.registerFd(self.r_fd)
		self.registerFd(self.w_fd)
		#print "[sigIO %s] setting flags in connect..." % os.getpid()
		self.setFlags(self.r_fd,select.POLLIN | select.POLLHUP | select.POLLERR | select.POLLNVAL)
		if self.out_buffer:
			self.setFlags(self.w_fd,select.POLLOUT | select.POLLHUP | select.POLLERR | select.POLLNVAL)
		else:
			self.setFlags(self.w_fd,select.POLLHUP | select.POLLERR | select.POLLNVAL)
		return self.state==ST_LISTENING
	def doSend(self):
		self.setFlags(self.w_fd,select.POLLOUT | select.POLLHUP | select.POLLERR | select.POLLNVAL)
		#try:
		self.io_serv.release()
		#except thread.error:
		#	pass
	def doOutput(self,fd):
		print "doOutput",self.out_buffer.strip(),os.getpid()
		sent=os.write(fd,self.out_buffer)
		self.out_buffer=self.out_buffer[sent:]
		return len(self.out_buffer)
	def doInput(self,fd):
		#print "doInput",os.getpid()
		#log("doInput - %s" % os.getpid())
		inp=os.read(fd,4096)
		if inp:
			return self.in_buffer.feed(inp)
	def getFd(self):
		if self.r_fd:
			return [self.r_fd,self.w_fd]
		else:
			return []
	def doClose(self):
		os.close(self.r_fd)
		self.r_fd=None
		os.close(self.w_fd)
		self.w_fd=None
	def finishOutput(self,fd=None):
		if self.state==ST_CLOSING:
			# woah we are set to closing... recall close with force=True to close cleanly
			#print "closing due to state in finishOutput"
			self.state=ST_CLOSED
			self.close()
			return
		self.setFlags(fd,select.POLLHUP | select.POLLERR | select.POLLNVAL)

class CommandTransport(Transport):
	"""thread stdin and stdout to another process as a transport
	
	the commands CLi then becomes its protocol!"""
	__name__="SpawnedProcess"
	def __tinit__(self,use_stderr=False):
		self.dev=None
		self.inp_fd=None
		self.oup_fd=None
		self.err_fd=None
		self.use_stderr=use_stderr
	def getFd(self):
		if self.dev:
			return (self.inp_fd,self.oup_fd,self.err_fd)
		else:
			return ()
	def open(self):
		command=[self.command]
		if self.args:
			command.extend(self.args)
		#print command
		self.dev=subprocess.Popen(command,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
		self.inp_fd=self.dev.stdout.fileno()
		self.registerFd(self.inp_fd)
		self.oup_fd=self.dev.stdin.fileno()
		self.registerFd(self.oup_fd)
		self.err_fd=self.dev.stderr.fileno()
		self.registerFd(self.err_fd)
		self.setFlags(self.inp_fd,select.POLLIN | select.POLLHUP | select.POLLERR | select.POLLNVAL)
		self.setFlags(self.err_fd,select.POLLIN | select.POLLHUP | select.POLLERR | select.POLLNVAL)
		self.setFlags(self.oup_fd,select.POLLHUP | select.POLLERR | select.POLLNVAL)
		#print self.inp_fd,self.oup_fd,self.err_fd
	def listen(self,address,args=[]):
		self.command=address
		self.args=args
		self.state=ST_LISTENING
		try:
			self.open()
		except:
			self.state=ST_CLOSED
			raise
	def connect(self,address,args=[]):
		self.command=address
		self.args=args
		self.state=ST_CONNECTED
		try:
			self.open()
		except:
			self.state=ST_CLOSED
			raise
		self.onConnect(address)
	def doInput(self,fd):
		if fd==self.inp_fd:
			return self.in_buffer.feed(self.dev.stdout.read())
		elif fd==self.err_fd:
			if self.use_stderr:
				return self.in_buffer.feed(self.dev.stderr.read())
			else:
				sys.stderr.write(self.dev.stderr.read())
	def doOutput(self,fd):
		sent=self.dev.stdin.write(self.out_buffer)
		self.out_buffer=self.out_buffer[sent:]
		return len(self.out_buffer)
	def doError(self,fd,flags):
		# this gets fired on POLLHUP,POLLERR or POLLNVAL
		self.close(force=True)
	def doSend(self):
		# new stuff added to our output buffer.. start sending it..
		self.setFlags(self.oup_fd,select.POLLOUT | select.POLLHUP | select.POLLERR | select.POLLNVAL)
		self.io_serv.release()
	def finishOutput(self):
		# we have exhausted our output buffer set the output fd to no longer writeable... or if closing set state to closed and unregister fd.
		if self.state==ST_CLOSING:
			for fd in self.getFds():
				self.setFlags(fd,None)
			self.doClose()
			self.state=ST_CLOSED
		else:
			self.setFlags(self.oup_fd,select.POLLHUP | select.POLLERR | select.POLLNVAL)
	def doClose(self):
		if self.dev.poll() or self.dev.returncode:
			# we are already closed...
			#print "Command channel closed"
			return
		self.dev.stdin.close()
		try:
			os.kill(self.dev.pid,3)
		except OSError:
			sys.stderr.write("error killing process %s" % self.dev.pid) 
		kill_cnt=0
		while not self.dev.poll():
			time.sleep(.5)
			kill_cnt=kill_cnt+1
			if kill_cnt>5:
				# ok get serious with dash 9's
				os.kill(self.dev.pid,9)
			elif kill_cnt>10:
				# whoa still alive, either its unkillable or its exiting..
				# either way give up
				break
		#print "[CommandChannel.doClose]"

class CommandClientTransport(Transport):
	"""thread stdin and stdout to another process as a transport
	
	this is the client counterpart to above.... use out sys.stdin and sys.stdout as transport"""
	__name__="SpawnedProcess"
	def __tinit__(self):
		self.dev=None
	def getFd(self):
		if self.dev:
			return (self.inp_fd,self.oup_fd,self.err_fd)
		else:
			return ()
	def open(self):
		# redirect all output to stderr
		sys.stdout=sys.stderr
		self.inp_fd=sys.__stdin__.fileno()
		self.oup_fd=sys.__stdout__.fileno()
		self.err_fd=None
		self.dev=True
		self.registerFd(self.inp_fd)
		self.registerFd(self.oup_fd)
		self.setFlags(self.inp_fd,select.POLLIN | select.POLLHUP | select.POLLERR | select.POLLNVAL)
		sys.stderr.write('input fd - '+str(self.inp_fd))
		self.setFlags(self.oup_fd,select.POLLHUP | select.POLLERR | select.POLLNVAL)
	def listen(self,address):
		self.state=ST_LISTENING
		try:
			self.open()
		except:
			self.state=ST_CLOSED
			raise
	def connect(self,address):
		self.state=ST_CONNECTED
		try:
			self.open()
		except:
			self.state=ST_CLOSED
			raise
		self.onConnect(address)
	def doInput(self,fd):
		if fd==self.inp_fd:
			return self.in_buffer.feed(os.read(self.inp_fd,4096))
	def doOutput(self,fd):
		if fd==self.oup_fd:
			sent=os.write(self.oup_fd,self.out_buffer)
		self.out_buffer=self.out_buffer[sent:]
		return len(self.out_buffer)
	def doError(self,fd,flags):
		# this gets fired on POLLHUP,POLLERR or POLLNVAL
		self.close(force=True)
	def sendOutput(self):
		# new stuff added to our output buffer.. start sending it..
		self.setFlags(self.oup_fd,select.POLLOUT | select.POLLHUP | select.POLLERR | select.POLLNVAL)
	def finishOutput(self):
		# we have exhausted our output buffer set the output fd to no longer writeable... or if closing set state to closed and unregister fd.
		if self.state==ST_CLOSING:
			for fd in self.getFds():
				self.setFlags(fd,None)
			self.doClose()
			self.state=ST_CLOSED
		else:
			self.setFlags(self.oup_fd,select.POLLHUP | select.POLLERR | select.POLLNVAL)

class SerialTransport(Transport):
	"""Serial port Transport"""
	__name__="SerialPort"
	def __tinit__(self,baud=9600,bsize=8,stopb=1,parity=None):
		self.fd=None
		self.dev_conf={'baud':baud,'bsize':bsize,'stopb':stopb,'parity':parity}
	def getFd(self):
		if self.fd:
			return (self.fd,)
		else:
			return Transport.getFd(self)
	def doInput(self,fd):
		"""Read from serial port and fire generate if buffer has new elements"""
		return self.in_buffer.feed(os.read(fd,4096))
	def doOutput(self,fd):
		"""write all of out_buff to serial port"""
		bytes=os.write(fd,self.out_buffer)
		self.out_buffer=self.out_buffer[bytes:]
		return len(self.out_buffer)
	def open(self,address=None):
		"""open serial port at <address>"""
		#print "in serial_open %s" % address
		if not address:
			self.fd=None
			self.dev=None
			return
		self.fd=os.open(address,os.O_RDWR | os.O_NOCTTY | os.O_NONBLOCK)
		self.setAttr()
		self.address=address
		self.registerFd(self.fd)
		self.setFlags(self.fd,select.POLLIN | select.POLLHUP | select.POLLERR | select.POLLNVAL)
	def setAttr(self,conf=None):
		if conf:
			self.dev_conf.update(conf)
		attr=termios.tcgetattr(self.fd)
		attr[4]=eval('termios.B%s' % self.dev_conf['baud'])
		attr[5]=attr[4]
		attr[2] |=  (termios.CLOCAL|termios.CREAD)
		attr[3] &= ~(termios.ICANON|termios.ECHO|termios.ECHOE|termios.ECHOK|termios.ECHONL|termios.ECHOCTL|termios.ECHOKE|termios.ISIG|termios.IEXTEN) #|termios.ECHOPRT
		attr[1] &= ~(termios.OPOST)
		attr[0] &= ~(termios.INLCR|termios.IGNCR|termios.ICRNL|termios.IUCLC|termios.IGNBRK)
		attr[0] &= ~termios.CSIZE
		if self.dev_conf['bsize']==7:
			attr[2] |= termios.CS7
		elif self.dev_conf['bsize']==6:
			attr[2] |= termios.CS6
		elif self.dev_conf['bsize']==5:
			attr[2] |= termios.CS5
		else:
			attr[2] |= termios.CS8
		if self.dev_conf['stopb']==2:
			attr[2] |= (termios.CSTOPB)
		else:
			attr[2] &= ~(termios.CSTOPB)
		attr[0] &= ~(termios.INPCK|termios.ISTRIP)
		if self.dev_conf['parity'] == 'E':
			attr[2] &= ~(termios.PARODD)
			attr[2] |=  (termios.PARENB)
		elif self.dev_conf['parity']== 'O':
			attr[2] |=  (termios.PARENB|termios.PARODD)
		else:
			attr[2] &= ~(termios.PARENB|termios.PARODD)
		attr[0] &= ~(termios.IXON|termios.IXOFF|termios.IXANY)
		termios.tcsetattr(self.fd,termios.TCSANOW,attr)
		#self.address=address
		try:
			self.setRTSDTR(0) # make sure rts/dtr are off at start...
		except OSError:
			print '[SerialTransport] innappropriate ioctl port does not support RTS/DTR'
	def setRTSDTR(self,on=1):
		"""Unique to serial transport set RTS/DTR high or low"""
		if self.state==ST_CLOSED:
			sys.stderr.write("setRTSDTR on closed serial channel\n")
			return
		if on:
			fcntl.ioctl(self.fd, termios.TIOCMBIS,TIOCM_DTR_str)
			fcntl.ioctl(self.fd, termios.TIOCMBIS,TIOCM_RTS_str)
		else:
			fcntl.ioctl(self.fd, termios.TIOCMBIC,TIOCM_DTR_str)
			fcntl.ioctl(self.fd, termios.TIOCMBIC,TIOCM_RTS_str)
	def listen(self,address):
		self.state=ST_LISTENING
		try:
			self.open(address=address)
		except:
			self.state=ST_CLOSED
			raise
	def connect(self,address):
		self.state=ST_CONNECTED
		try:
			self.open(address=address)
		except:
			self.state=ST_CLOSED
			raise
		self.onConnect(address)
	def doClose(self):
		# override to close device...
		if self.fd:
			os.close(self.fd)
		self.fd=None

if __name__=="__main__":
	import time,utils
	class EchoProtocol(Protocol):
		sep='\n'
		def onData(self):
			for line in self.in_buffer:
				if self.state==ST_LISTENING:
					# we are server side....
					print 'input -'+repr(line)
					self.send('you said %s, uid - %s' % (line.strip(),os.getpid()))
				else:
					print 'from child -->',line
		def onSend(self,packet):
			Protocol.onSend(self,packet)
			print "onSend",self,packet,self.out_buffer
	io_serv=getIO()
	io_serv.start()
	ch=io_serv.newChannel(EchoProtocol,ForkTransport)
	if ch.connect(None,uid=1000):
		ts=time.time()+30
		while ts>time.time():
			time.sleep(1)
	else:
		time.sleep(2)
		for ind in range(20):
			ch.send('line %s' % ind)
			if ind>5:
				utils.sleep(.01)
	utils.sleep(2)
	io_serv.stop()
	print 'out'

	#from . import glib_core
	#class DummyProtocol(Protocol):
	#	# so this just prints out each line outputed by the command.....
	#	# normally this would parse the lines into the significant parts,
	#	# in our trivial example here possibly date, hostname, source, etc
	#	# create events and dispatch them using self.dispatcher....
	#	sep='\n'
	#	def onData(self):
	#		for line in self.in_buffer:
	#			print "[%s] - %s" % (self.command,line)
	# setup IOService and run
	#io_serv=getIO()
	#io_serv.start()
	#create channel
	#ch=io_serv.newChannel(DummyProtocol,CommandTransport)
	# listen to a command that will give us some delayed output... tail -f on a log file should do nicely
	#ch.listen('tail',args=['-f','/var/log/syslog'])
	#io_serv.run()
	# now plug and unplug a usb device to see the log lines appear...
	#io_serv.run()
	#io_serv.stop()
