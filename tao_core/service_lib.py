
#   Copyright 2011 Matthew Bull

#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from threading import Condition
from utils import *
import thread,sys
import traceback

STAT_RUN=1		# true if we are currently running
STAT_START=2	# true if we should be running.... this and STAT_RUN indicates actually running, this without STAT_RUN indicates service starting, can you guess the rest??
STAT_AUTO=4		# true if we should be started automatically if required
STAT_RESTART=8	# true if we should be 'respawned'
STAT_ERROR=16

class ServiceError(CompositeException):
	def __init__(self,info=""):
		CompositeException.__init__(self,info="Service Exception %s" % info)

"""
basic service handling libraries, derived services may override start and stop (both of which should return _after_ the service
has actually started or stopped
"""

class ServiceStatus:
	def __init__(self,init_status=0):
		self._cond=Condition()
		self.status=init_status
	def __eq__(self,comp):
		self._cond.acquire()
		res=self.status & comp>0
		self._cond.release()
		return res
	def setStatus(self,status):
		self._cond.acquire()
		self.status=status
		self._cond.notifyAll()
		self._cond.release()
	def setFlag(self,flag,val=True):
		self._cond.acquire()
		if val:
			self.status=self.status | flag
		else:
			self.status=self.status & ~flag
		self._cond.notifyAll()
		self._cond.release()
	def checkFlag(self,flag):
		return self.status & flag
	def wait(self,status):
		self._cond.acquire()
		while (self.status & status)!=status:
			if self.status & STAT_ERROR:
				self._cond.release()
				raise ServiceError()
			self._cond.wait()
		self._cond.release()
		return (self.status & status)==status
	def get(self):
		self._cond.acquire()
		stat=self.status
		self._cond.release()
		return stat
	def close(self):
		self._cond.acquire()
		self.status=STAT_ERROR
		self._cond.notifyAll()
		self._cond.release()
	def __del__(self):
		self.close()

class Service:
	provides=["Service"]
	depends=[]
	def_status=STAT_AUTO | STAT_RESTART
	def __init__(self):
		self.service_man=None
		self.dependents=[]
		self.status=ServiceStatus(self.def_status)
	def _init_service(self,service_man):
		self.service_man=service_man
		self.serviceInit()
	def serviceInit(self):
		print "serviceInit %s" % self.provides[0]
	def waitForStart(self,dependent=None):
		if dependent:
			self.dependents.append(dependent)
		if not self.status.wait(self.def_status | STAT_START | STAT_RUN):
			raise ServiceError()
	def _s_start(self):
		if not self.service_man:
			print "unregistered service"
			raise ServiceError("Unregistered service %s" % self.provides[0])
		print "service start"
		if self.status.checkFlag(STAT_START):
			# service already started or starting...
			print "service %s started..... wait on start" % repr(self)
			self.status.wait(self.def_status | STAT_START | STAT_RUN)
			return
		self.status.setFlag(STAT_START,True)
		for s_name in self.depends:
			print "starting dependency",s_name
			try:
				self.service_man.startService(s_name,dependent=self.provides[0])
			except ServiceError:
				# this happens when dependancy fails to start...
				print "Dependency %s failed to start" % s_name
				self.status.setFlag(STAT_ERROR,True)
				return
		try:
			self.start()
		except Exception,exc:
			print "error on service start"
			self.status.setFlag(STAT_ERROR,True)
			#print "set stat_error"
			self.status.setFlag(STAT_START,False)
			raise ServiceError("in startup of %s" % repr(self))
		self.status.setFlag(STAT_RUN,True)
	def _s_stop(self):
		if not self.status.checkFlag(STAT_START):
			# services already stopped or stopping....
			self.status.waitForState(self.def_status)
			return
		self.status.setFlag(STAT_START,False)
		for s_name in self.dependents:
			self.service_man.stopService(s_name)
		try:
			self.stop()
		except:
			print "error stopping service %s" % self.provides[0]
		finally:
			self.status.setFlag(STAT_RUN,False)
	def start(self):
		"""Override to start service"""
		print "start %s" % self.provides[0]
	def stop(self):
		"""Override to stop service"""
		print "stop %s" % self.provides[0]

class ConcurrentService(Service):
	""" ConcurrentService has its own loop, so gets started differently"""
	def __init__(self):
		Service.__init__(self)
		self._run=False
	def _s_start(self):
		if not self.service_man:
			raise ServiceError("Unregistered service %s" % self.provides[0])
		if self.status.checkFlag(STAT_START):
			self.status.waitForState(self.def_status | STAT_START | STAT_RUN)
		self.status.setFlag(STAT_START,True)
		for s_name in self.depends:
			try:
				self.service_man.startService(s_name,dependent=self.provides[0])
			except ServiceError:
				# this happens when dependancy fails to start...
				print "Dependency %s failed to start" % s_name
				self.status.setFlag(STAT_ERROR,True)
				return
		try:
			self.start()
		except:
			#print "error on service start"
			self.status.setFlag(STAT_ERROR,True)
			#print "set stat_error"
			self.status.setFlag(STAT_START,False)
			traceback.print_exc()
			raise ServiceError("%s Exception during startup" % repr(self))
		self.status.setFlag(STAT_RUN,True)
		self._run=True
		try:
			self.mainloop()
		except:
			traceback.print_exc()
			self.status.setFlag(STAT_ERROR,True)
		# OK well this may look a little strange but start never returns (untill stop)
		# so we call self.stop here _after_ the mainloop has exited
		if self.status.checkFlag(STAT_START):
			self.status.setFlag(STAT_ERROR,True)
		try:
			self.stop()
		except:
			print "error stopping service %s" % self.provides[0]
		finally:
			self.status.setFlag(STAT_RUN,False)
	def _s_stop(self):
		if not self.status.checkFlag(STAT_START):
			# services already stopped or stopping....
			self.status.waitForState(self.def_status)
			return
		self.status.setFlag(STAT_START,False)
		for s_name in self.dependents:
			self.service_man.stopService(s_name)
		self._run=False
	def mainloop(self):
		while self._run:
			# do main iteration stuff here
			pass

class ServiceManager:
	def __init__(self):
		self.services={}
	def registerService(self,service):
		service._init_service(self)
		for s_name in service.provides:
			if self.services.has_key(s_name):
				print "Duplicate service %s -> %s" % (repr(service),repr(self.services[s_name]))
			else:
				self.services[s_name]=service
		if len(service.depends):
			for dep_name in service.depends:
				if not self.services.has_key(dep_name):
					print "unsatisfied dependency %s" % dep_name
					return False
		return True
	def startService(self,s_name,dependent=None):
		self.checkDeps(s_name)
		service=self.getService(s_name)
		#print "attempting to start service %s" % repr(service)
		if dependent:
			if service.status.checkFlag(STAT_AUTO) and not service.status.checkFlag(STAT_START):
				#service is dependency of other service and has auto flag set give it a try...
				thread.start_new_thread(service._s_start,(),{})
		else:
			thread.start_new_thread(service._s_start,(),{})
		if dependent and not service.status.checkFlag(STAT_AUTO):
			print "Warning: dependent %s requires start of service %s which is not started automatically" % (dependent,s_name)
		#print "%s start waiting for start of %s" % (dependent,s_name)
		service.waitForStart(dependent)
	def stopService(self,name):
		service=self.services[name]
		for dsrv in service.dependents:
			self.stopService(dsrv)
		service.stop()
	def getService(self,name):
		return self.services[name]
	def checkDeps(self,s_name,deps=[]):
		if len(self.services[s_name].depends):
			deps.append(s_name)
			for dep_name in self.services[s_name].depends:
				if dep_name in deps:
					raise ServiceError("circular dependency %s in %s" % (dep_name,s_name))
				self.checkDeps(dep_name,deps=deps)

if __name__=="__main__":
	import time
	sm=ServiceManager()
	class ts1(ConcurrentService):
		provides=['Test Service 1']
		# uncomment to test circular deps
		#depends=['Test Service 3']
		def mainloop(self):
			while self._run:
				time.sleep(1)
				print "ts1 main iteration"
	class ts2(Service):
		provides=['Test Service 2','Test Service Sub']
		depends=['Test Service 1']
		def start(self):
			print "start...."
			time.sleep(3)
			print "started....."
	class ts3(Service):
		provides=['Test Service 3']
		depends=['Test Service Sub']
	s1=ts1()
	s2=ts2()
	s3=ts3()
	sm.registerService(s1)
	sm.registerService(s2)
	sm.registerService(s3)
	sm.startService('Test Service 3')
	time.sleep(5)
	sm.stopService('Test Service 1')


