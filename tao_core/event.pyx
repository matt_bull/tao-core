# base class for authenticated events....
cdef class Event:
	cdef object py_event,name
	cdef readonly object identity,cred,validated
	def __init__(self,py_event,ident,cred,domain=None):
		self.py_event=py_event
		self.name=py_event.name
		self.identity=ident
		self.credentials=cred
		self.domain=domain
		self.validated=False
	def setAuthenticated(self,valid):
		if not self.credentials:
			return
		self.credentials=None
		if valid:
			self.validated=True
		else:
			self.validated=False
	def __str__(self):
		return "<event.Event object name %s>" % self.name
	def __repr__(self):
		return self.__str__()
	def __getitem__(self,key):
		if key=='identity':
			return self.identity
		elif key=='validated':
			return self.validated
		elif key=='credentials':
			return self.credentials
		else:
			return self.py_event[key]
	def __setitem__(self,key,value):
		self.py_event[key]=value
	def has_key(self,key):
		if key in ('identity','credentials','validated'):
			return True
		return self.py_event.has_key(key)
	#def __hash__(self):
	#	return hash(py_event)
	def __hash__(self):
		return id(self)^hash(self.identity+'@'+self.domain)
