# refactored originally from http_dispatch.py, shared virtual file system for network servers
# RSP_LST responses should be a list of tuples in the format (<name>,<primary mime type>,)
from tdf_core import SimpleEvent,Dispatcher
from sigIO import FileProducer
import os

REQ_GET=1
REQ_UPD=2

RSP_LST=1
RSP_DAT=2

extension_map={
		('htm','html'):'text/html',
		('txt'):'text/utf-8'
	}

class FsAccessRequest(SimpleEvent):
	defaults={
			'host':'localhost',
			'user':'guest',
			'path':[],
			'action':REQ_UPD,
			'resp_type':RSP_LST,
			'data_mime':'',
			'update_index':(0,-1),
			'data':None
			}
	def __init__(self,host='localhost',user='guest',path='/',action='GET'):
		pth=path.split('/')
		while pth[0]=='' and len(pth)>0:
			pth=pth[1:]
		SimpleEvent.__init__(self,'org.digitao.VfsAccessRequest',host=host,user=user,path=pth,action=action)
		self.rel_path=pth[:]

class VfsFsDispatcher(Dispatcher):
	accept_events=['org.digitao.VfsAccessRequest']
	def __init__(self,base_path):
		super(Dispatcher,self).__init__()
		self.base_path=base_path
	def dispatch(self,event,cb=None):
		# we never return True so we can ignore cb
		# check request type... we only handle GET
		if event['req_type']!="GET":
			return False
		# first strip any query string from the event path
		if '?' in event['req_path']:
			path=event['req_path'].split('?',1)[0]
		else:
			path=event['req_path']
		while path.startswith('/'):
			path=path[1:]
		# then check the file exists
		#print os.path.join(self.base_path,path)
		if not os.access(os.path.join(self.base_path,path),os.R_OK):
			# we have no access to the file abort and let other handlers have a crack...
			print "no file!!!"
			print event['resp_headers']
			return
		p_f=open(os.path.join(self.base_path,path),'r')
		# there must be a better way of doing this???
		p_f.seek(0,os.SEEK_END)
		# but we must set file length.....
		event['resp_headers']['Content-Length']=p_f.tell()
		#print "file length ->",p_f.tell()
		p_f.seek(0)
		f_typ=self.guessType(p_f,path)
		if f_typ:
			event['resp_headers']['Content-Type']=f_typ
		#print event['resp_headers']
		event['resp_body']=FileProducer(p_f)
		#print repr(event['resp_body'])
		event['resp_code']=200
		event['resp_message']="OK"
		#print "raising stopIteration"
		raise StopIteration()
	def guessType(self,f_obj,path):
		ext=path.rsplit('.',1)[1]
		for exten_grp in extension_map:
			if ext in exten_grp:
				return extension_map[exten_grp]
