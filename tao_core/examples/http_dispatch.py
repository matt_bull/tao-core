#=====================================
# please note, this example bacame so complete and useful it got split off into its own library...
# that library (tao_web) contains a much more up to date version of this file, this file is
# included here for historical reasons only, for actual use please use tao_web
#=====================================

from http import *
import sys

#this is just for example
sys.path.append('../')

from tdf_core import *
from sigIO import FileProducer

extension_map={
		('htm','html'):'text/html',
		('txt'):'text/utf-8'
	}

error_pages[401]='<html><head><title>401 - Authorization Required</title></head><body><font family=arial,helvetica><h2>401 - Authorization Required</h2>Authentication required to access the requested document %s</font></body></html>'

class HttpLogDispatcher(TrackDispatcher):
	""" obviously this could / should be a lot more complex, do log rotation etc"""
	def __init__(self,dispatcher,log_path):
		TrackDispatcher.__init__(self)
		self._dispatcher=dispatcher
		self.log_file=open(log_path,'a')
	def dispatch(self,event,cb=None):
		print "dispatch ->",event['req_path']
		TrackDispatcher.dispatch(self,event,cb=cb)
		try:
			dis_res=self._dispatcher(event,cb=self.finish)
		except StopIteration:
			self.finish(event)
		except Exception,exc:
			self.finish(event,exc=exc)
		else:
			self.finish(event)
		return True
	def finish(self,event,exc=None):
		# TODO: log to self.log_file in standard (ie apache log format....)
		print "log",event['resp_headers'],repr(exc)
		TrackDispatcher.finish(self,event,exc=exc)
	def __del__(self):
		# clean up the log file....
		self.log_file.close()

class HttpVHostDispatcher(Dispatcher):
	def __init__(self):
		Dispatcher.__init__(self)
		self.vhost_map={}
	def registerDispatcher(self,vhost,disp):
		# check a dispatcher doesn't exist...
		if self.vhost_map.has_key(vhost):
			raise KeyError("dispatcher already registered for virtual host %s" % vhost)
		self.vhost_map[vhost]=disp
	def removeDispatcher(self,vhost,disp):
		del(self.vhost_map[vhost])
	def dispatch(self,event,cb=None):
		if self.vhost_map.has_key(event['req_headers']['Host'].strip()):
			return self.vhost_map[event['req_headers']['Host'].strip()](event,cb=cb)
		else:
			print "unknown vhost",event['req_headers']['Host'],self.vhost_map

class HttpAuthDispatcher(Dispatcher):
	def __init__(self,dispatcher,user_db,protected_path="/"):
		Dispatcher.__init__(self)
		self._dispatcher=dispatcher
		# user_db must be a subclass of utils.UserDB
		self.user_db=user_db
		self.auth_path=protected_path
	def dispatch(self,event,cb=None):
		if event['req_headers'].has_key('Authorization'):
			if event['req_headers']['Authorization'].strip().startswith('Basic'):
				# do basic auth....
				cred_str=event['req_headers']['Authorization'].strip().split(' ',1)[1]
				uname,cred=base64.base64decode(cred_str).split(':',1)
				if self.user_db.authenticate(uname,cred):
					return self._dispatcher(event,cb=cb)
		event['resp_code']=401
		event['resp_headers']={'WWW-Authenticate':'Basic realm="%s"' % self.user_db.getRealm}
		event['resp_body']=error_pages[401] % event['req_path']
		raise StopIteration()

#class HttpOpenIDAuthDispatcher(Dispatcher):
#	def __init__(self,user_db,):

# there is a problem with path mapping the dispatchers (or more precisely nesting the path maps....
# that of dispatchers getting ignored due to a dispatcher being added for 
# this should be changed to consume paths and derive from map rather than Multi
class HttpPathDispatcher(MultiDispatcher):
	def __init__(self):
		Dispatcher.__init__(self)
		self.path_map={}
	def registerDispatcher(self,path,disp):
		# we allow multiple dispatchers for a path (how else would we do auth??)
		#????
		if not self.path_map.has_key(path):
			self.path_map[path]=[]
		self.path_map[path].append(disp)
	def removeDispatcher(self,path,disp):
		self.path_map[path].remove(disp)
	def dispatch(self,event,cb=None):
		# not sure how to deal with the paths / dispatchers once we have them...
		# or should we only allow one dispatcher per path???
		evt_path=event['path'].split('/')
		pth_list=[]
		for path in self.path_map.keys():
			if evt_path[:len(path)]==path:
				pth_list.append(path)
		# from here we need to dispatch event to dispatchers in order...
		# for auth its _ONLY_ the most specfic dispatcher that matters, no others should be called (if that one fails
		# for path mapping it should also be

class HttpPathDispatcher(MappingDispatcher):
	def __init__(self):
		MappingDispatcher.__init__(self)
		self.path_map={}
	def registerDispatcher(self,path,disp):
		path_elem=path.split('/')
		

class HttpUserPathDispatcher(HttpPathDispatcher):
	def __init__(self,home_base='/home',export_folder='Public'):
		self.home_base=home_base
		self.folder=export_folder
	def dispatch(self,event):
		pass		

class HttpFsDispatcher(Dispatcher):
	def __init__(self,base_path):
		super(Dispatcher,self).__init__()
		self.base_path=base_path
	def dispatch(self,event,cb=None):
		# we never return True so we can ignore cb
		# check request type... we only handle GET
		if event['req_type']!="GET":
			return False
		# first strip any query string from the event path
		if '?' in event['req_path']:
			path=event['req_path'].split('?',1)[0]
		else:
			path=event['req_path']
		while path.startswith('/'):
			path=path[1:]
		# then check the file exists
		#print os.path.join(self.base_path,path)
		if not os.access(os.path.join(self.base_path,path),os.R_OK):
			# we have no access to the file abort and let other handlers have a crack...
			print "no file!!!"
			print event['resp_headers']
			return
		p_f=open(os.path.join(self.base_path,path),'r')
		# there must be a better way of doing this???
		p_f.seek(0,os.SEEK_END)
		# but we must set file length.....
		event['resp_headers']['Content-Length']=p_f.tell()
		#print "file length ->",p_f.tell()
		p_f.seek(0)
		f_typ=self.guessType(p_f,path)
		if f_typ:
			event['resp_headers']['Content-Type']=f_typ
		#print event['resp_headers']
		event['resp_body']=FileProducer(p_f)
		#print repr(event['resp_body'])
		event['resp_code']=200
		event['resp_message']="OK"
		#print "raising stopIteration"
		raise StopIteration()
	def guessType(self,f_obj,path):
		ext=path.rsplit('.',1)[1]
		for exten_grp in extension_map:
			if ext in exten_grp:
				return extension_map[exten_grp]

#class JsonRPCDispatcher(Dispatcher):
#	def __init__(self,):

if __name__=="__main__":
	import sigIO_net
	io_main=sigIO.getIO()
	io_main.start()
	# assemble our dispatch graph.....
	# create a vhost dispatcher...
	http_vh=HttpVHostDispatcher()
	# create an fs dispatcher for pwd
	http_fs=HttpFsDispatcher('./')
	# add that as localhost....
	http_vh.registerDispatcher('localhost',http_fs)
	# create a dispatcher for our home directory...
	http_home=HttpFsDispatcher('/home/mbull/Public/')
	# 
	
	# register as virtual host http_test.local
	http_vh.registerDispatcher('http_test.local',http_home)
	# put a log dispatcher on the front end....
	http_log=HttpLogDispatcher(http_vh,os.path.expanduser('~/http_log'))
	# feed that into our channel
	chan=io_main.newChannel(HttpServerProtocol,sigIO_net.TCPInetTransport,dispatcher=http_log)
	chan.listen(('localhost',80))
	while 1:
		try:
			time.sleep(20)
		except KeyboardInterrupt:
			break
	chan.close()

