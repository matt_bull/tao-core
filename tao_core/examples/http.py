#=====================================
# please note, this example bacame so complete and useful it got split off into its own library...
# that library (tao_web) contains a much more up to date version of this file, this file is
# included here for historical reasons only, for actual use please use tao_web
#=====================================


## simplest possible Http server for sigIO ##
import time,copy
import sys

#this is just for example
# if you want to use this in anger remove this and prepend
# tao_core. to the imports below....
sys.path.append('../')

# we don't really use tdf_core here but the event class (with defaults)
# makes things simpler...
from tdf_core import SimpleEvent
import sigIO

ST_IDLE=1 # idle expect request / response line
ST_HEADER=2 # expect header line
ST_BODY=3 # expect body section...
ST_CBODY=4 # expect chunked body.....
ST_CLOSE=5 # when all data sent close connection...

def_mime_type='text/plain'
error_pages={
	404:'<html><head><title>404 - Document Not Found</title></head><body><font family=arial,helvetica><h2>404 - Document Not Found</h2>The requested document %s was not found</font></body></html>',
	500:'<html><head><title>500 - Internal Server Error</title></head><body><font family=arial,helvetica><h2>500 - Internal Server Error</h2>The request for %s could not be served due to an internal server error</font></body></html>'
	}
weekdayname = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
monthname = [None,'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

def httpDate():
	"""Return the current date and time formatted for a message header."""
	now = time.time()
	year, month, day, hh, mm, ss, wd, y, z = time.gmtime(now)
	s = "%s, %02d %3s %4d %02d:%02d:%02d GMT" % (
			weekdayname[wd],
			day, monthname[month], year,
			hh, mm, ss)
	return s

class HttpRequest(SimpleEvent):
	"""event representing a single http request"""
	defaults={
			'req_path':'/index.htm',
			'req_type':'GET',
			'req_version':'0.9',
			'req_headers':{},
			'hostname':'',
			'request_body':'',
			'http1':False,
			'resp_code':404,
			'resp_headers':{},
			'resp_message':'File Not Found',
			'resp_body':None,
			'doctypes':[]
			}
	def __init__(self,req_type,req_path=None,req_version='0.9',headers={},body=""):
		data={'req_type':req_type,'req_version':req_version,'req_headers':headers}
		#tweak a couple of things on the way in
		if req_path:
			data['req_path']=req_path
		if req_version!='0.9':
			data['http1']=True
			data['req_body']=data
		SimpleEvent.__init__(self,"org.digitao.events.HttpRequest",**data)

class HttpProtocol(sigIO.Protocol):
	"""HTTP protocol class"""
	def __pinit__(self,**kwargs):
		sigIO.Protocol.__pinit__(self)
		self.in_buffer=sigIO.LateBoundBuffer(sep='\r\n')
		self.producer=sigIO.FifoProducer()
		self.pstate=ST_IDLE
		#self.body_len=0
		#self.curr_chunk=0
		#self.chunked=False
		self.curr_headers={}
		self.curr_request=None
		self.body_part=''
	def onData(self):
		"""Generate HTTP request events from in_buff"""
		while 1:
			if self.pstate==ST_IDLE:
				#print self.in_buffer.peek()
				try:
					self.curr_request=self.in_buffer.next().split(' ',2)
					# ensure we aren't catching an empty line from a previous request...
					if len(self.curr_request)==3:
						self.pstate=ST_HEADER
				except StopIteration:
					break
				#except:
				#	self.close()
			elif self.pstate==ST_HEADER:
				try:
					req_line=self.in_buffer.next().strip()
				except StopIteration:
					break
				#print req_line,self.curr_request
				if req_line=="":
					if self.curr_headers.has_key('Content-Length'):
						# end of headers and expecting body
						self.pstate=ST_BODY
					elif self.curr_headers.has_key('Transfer-Encoding'):
						if "chunked" in self.curr_headers['Transfer-Encoding']:
							# chunked encoding....
							self.pstate=ST_CBODY
							self.c_len=0
					else:
						# end of headers.. and no body...
						print "headers only ->",self.curr_headers
						self.onPacket(self.curr_request,head=self.curr_headers)
						self.curr_request=None
						self.curr_headers={}
						self.pstate=ST_IDLE
				else:
					key,val=req_line.split(':',1)
					self.curr_headers[key]=val
			elif self.pstate==ST_BODY:
				try:
					self.body_part=self.body_part+self.in_buffer.next(plen=int(self.curr_headers['Content-Length'])-len(self.body_part))
				except StopIteration:
					break
				if len(self.body_part)==int(self.curr_headers['Content-Length']):
					self.onPacket(self.curr_request,head=self.curr_headers,body=self.body_part)
					self.curr_request=None
					self.curr_headers={}
					self.body_part=''
					self.pstate=ST_IDLE
			elif self.pstate==ST_CBODY:
				# TODO: implement chunked encoding for body...
				#raise NotImplementedError('chunked encoding not implemented')
				#print self.c_len
				if self.c_len:
					# read chunk and update self.c_len
					try:
						http_chunk=self.in_buffer.next(plen=self.c_len)
					except StopIteration:
						break
					self.c_len=self.c_len-len(http_chunk)
					self.body_part=self.body_part+http_chunk
					try:
						self.in_buffer.next(plen=2) # trailer
					except StopIteration:
						pass
				else:
					# get the chunk length....
					try:
						#self.c_len=int(self.in_buffer.next(),16)
						h_len=self.in_buffer.next()
					except StopIteration:
						break
					try:
						self.c_len=int(h_len,16)
					except ValueError:
						#self.c_len=0
						print h_len
					if self.c_len==0:
						# zero length chunk..... done
						self.onPacket(self.curr_request,head=self.curr_headers,body=self.body_part)
						self.curr_request=None
						self.curr_headers={}
						self.body_part=''
						self.pstate=ST_IDLE
						# deal with the trailing CRLF
						try:
							self.in_buffer.next()
						except StopIteration:
							pass
		#for req_line in self.in_buffer:
		#	print '>>>',req_line,self.pstate
		#	if self.pstate==ST_BODY:
		#		self.onBodyChunk(req_line)
		#	elif self.pstate==ST_IDLE:
		#		req_line=req_line.strip()
		#		try:
		#			self.curr_request=req_line.split(' ',2)
		#			self.pstate=ST_HEADER
		#		except:
		#			print "Malformed request line %s" % req_line
		#			self.close()
		#	elif self.pstate==ST_HEADER:
		#		req_line=req_line.strip()
		#		if req_line=="":
		#			if self.curr_headers.has_key('Content-Length'):
		#				# end of headers and expecting body
		#				self.pstate=ST_BODY
		#			elif self.curr_headers.has_key('Transfer-Encoding'):
		#				if "chunked" in self.curr_headers['Transfer-Encoding']:
		#					# chunked encoding....
		#					self.pstate=ST_BODY
		#			else:
		#				# end of headers.. and no body...
		#				print "headers only ->",self.curr_headers
		#				self.onPacket(self.curr_request,head=self.curr_headers)
		#				self.curr_request=None
		#				self.curr_headers={}
		#				self.pstate=ST_IDLE
		#		else:
		#			key,val=req_line.split(':',1)
		#			self.curr_headers[key]=val
	def onPacket(self,req,head={}):
		"""depends on if we are client or server"""
		pass
	#def onBodyChunk(self,body_chunk):
	#	""" this is the default (handles both client and server bodies),
	#	must be overidden in HTTP1.1 and up clients to handle chunked Transfer encoding..."""
	#	self.body_part=self.body_part+'\r\n'+body_chunk
	#	#print self.body_part
	#	if len(self.body_part)>=int(self.curr_headers['Content-Length']):
	#		self.onPacket(self.curr_request,head=self.curr_headers,body=self.body_part)
	#		self.curr_request=None
	#		self.curr_headers={}
	#		self.body_part=''
	#		self.pstate=ST_IDLE

class HttpClientProtocol(HttpProtocol):
	__name__="HttpClient"
	# now for the client specific stuff
	def onPacket(self,resp,head={},body=""):
		""" this will do nicely for testing... but really should be a map of req -> callbacks"""
		#print "onPacket",self.pending
		self.pending(resp[1],resp[2],headers=head,body=self.body_part)
		self.pending=None
	def request(self,method,path,req_headers={},body=None):
		if self.state!=sigIO.ST_CONNECTED:
			raise IOError("Request on unconnected channel")
		headers={
				'User-Agent':'Tao Service Framework',
				'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
				'Keep-Alive':300,
				'Connection':'Keep-Alive'
				}
		headers.update(req_headers)
		if body:
			headers['Content-Length']=len(body)
		self.producer.append('%s  %s%s HTTP/1.1\r\n' % (method.upper(),self.peer[0],path))
		for header in headers.keys():
			self.producer.append('%s:%s\r\n' % (header,str(headers[header])))
		self.producer.append('\r\n')
		if body:
			self.producer.append(body)
		self.doSend()
	# couple of quick shortcuts for testing....
	def get(self,path,cb=True):
		if not hasattr(self,'pending'):
			self.pending=None
		if not self.pending:
			self.pending=cb
			self.request('GET',path)
	def post(self,path,data,cb=True):
		if not hasattr(self,'pending'):
			self.pending=None
		if not self.pending:
			self.pending=cb
			self.request('POST',path,body=data)

class HttpServerProtocol(HttpProtocol):
	__name__="HttpServer"
	def onPacket(self,req,head={},body=""):
		print "putting event"
		self.put(HttpRequest(req[0],req_path=req[1],req_version=req[2],headers=head,body=body),sync=True)
	def onCallback(self,event,exc=None):
		"""Render a (possibly handled) event back to the client"""
		print "Render ->",event['resp_code'],repr(event['resp_body'])
		headers=event['resp_headers']
		headers['Server']='Tao Service Framework v0.1'
		headers['Date']=' %s' % httpDate()
		if event['req_headers'].has_key('Keep-Alive'):
			headers['Connection']='Keep-Alive'
		if event['resp_body']:
			body=event['resp_body']
		elif error_pages.has_key(event['resp_code']):
			body=error_pages[event['resp_code']] % event['req_path']
			headers['Content-Type']='text/html'
		elif exc:
			event['resp_code']=500
			body=error_pages[500]
		# close connection on all errors except 404's
		if event['resp_code']>399 and event['resp_code']!=404:
			headers['Connection']='close'
		if event['http1']:
			self.producer.append('HTTP/1.1 %s %s\r\n' % (event['resp_code'],event['resp_message']))
		else:
			self.producer.append('HTTP/0.9 %s %s\r\n' % (event['resp_code'],event['resp_message']))
		for header in headers.keys():
			if header!='Content-Length':
				# don't do conent-length here.... it must be the last header..
				self.producer.append('%s:%s\r\n' % (header,headers[header]))
		# this should check if body is a generator....
		# and if so should use TE - Chunked
		if headers.has_key('Content-Length'):
			print "set content length -",headers['Content-Length']
			# this is a producer..... 
			self.producer.append('Content-Length:%s\r\n\r\n' % (headers['Content-Length']))
			self.producer.append(body)
			self.producer.append('\r\n')
		else:
			self.producer.append('Content-Length: %s\r\n\r\n' % len(body))
			self.producer.append(body+'\r\n')
		self.doSend()

if __name__=="__main__":
	import sigIO_net
	io_main=sigIO.getIO()
	io_main.start()
	if '-server' in sys.argv:
		# real quick and dirty dispatcher....
		def httpDispatch(event,cb=None):
			print "Http Request ->",event['req_path'],cb
			print "Request Headers ->"
			for key in event['req_headers'].keys():
				print "\t%s -> %s" % (key,event['req_headers'][key])
			event['resp_code']=200
			event['resp_message']='OK'
			event['resp_body']="<html><body>test page - %s</body></html>" % event['req_path']
		chan=io_main.newChannel(HttpServerProtocol,sigIO_net.TCPInetTransport,dispatcher=httpDispatch)
		chan.listen(('localhost',80))
		while 1:
			try:
				time.sleep(20)
			except KeyboardInterrupt:
				break
		chan.close()
	else:
		def resp_cb(code,msg,headers={},body=""):
			print "Response -> %s - %s" % (code,msg)
			print headers
			print body
		c_chan=io_main.newChannel(HttpClientProtocol,sigIO_net.TCPInetTransport)
		c_chan.connect(('www.google.co.uk',80))
		last=0
		while 1:
			if time.time()-last>20:
				print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>> calling GET"
				c_chan.get('/',cb=resp_cb)
				last=time.time()
			try:
				time.sleep(20-(time.time()-last))
			except KeyboardInterrupt:
				break
		c_chan.close()
	io_main.stop()


