#=====================================
# please note, this example bacame so complete and useful it got split off into its own library...
# that library (tao_web.web_widget) contains a much more up to date version of this file, this file is
# included here for historical reasons only, for actual use please use tao_web
#=====================================

# nested templates.... must have doc template as top level object...
# everything else renders a dictionary into an html fragment
import types
from urlparse import urlparse
from tao_core.tdf_core import Dispatcher

# utility functions, mergeLists merges lists of objects which are...
# HostRelativePath instances, DocumentRelativePath instances or absolute urls (as strings)
# and returns a single list of unique file urls

def mergeLists(top,sub,base_url):
	for item in sub:
		if hasattr(item,'getPath'):
			item=item.getPath(base_url)
		if item not in top:
			top.append(item)

class HostRelativePath(str):
	def getPath(self,doc_url):
		o=urlparse(doc_url)
		return o.scheme+'://'+o.hostname+'/'+self.__str__()

class DocumentRelativePath(str):
	def getPath(self,doc_url):
		o=urlparse(doc_url)
		return o.scheme+'://'+o.hostname+o.path.rsplit('/',1)[0]+'/'+self.__str__()

def recursiveIter(lst):
	lst=lst[:]
	curr_gen=None
	while lst or curr_gen:
		if curr_gen:
			try:
				yield curr_gen.next()
			except StopIteration:
				curr_gen=None
		else:
			if type(lst[0])==str:
				yield lst.pop(0)
			elif isinstance(lst[0],Template) or type(lst[0])==types.GeneratorType:
				curr_gen=lst.pop(0)
			else:
				curr_gen=recursiveIter(lst.pop(0))

class Template:
	js_links=[]
	css_links=[]
	def __init__(self,child=[]):
		""" child list of child items,
		items can be strings, template instances or iterables which yield strings"""
		self.child=child
		self.index=0
	def getCss(self,base_url):
		css=[]
		for sub in self.child:
			if isinstance(sub,Template):
				mergeLists(css,sub.getCss(base_url),base_url)
		mergeLists(css,self.css_links,base_url)
		return css
	def getScript(self,base_url):
		scripts=[]
		for sub in self.child:
			if isinstance(sub,Template):
				mergeLists(scripts,sub.getScript(base_url),base_url)
		mergeLists(scripts,self.js_links,base_url)
		return scripts
	def __iter__(self):
		return self
	def next(self):
		# we just serve up our children in order here....
		# override this to actually do something
		try:
			return self.child[self.index]
		finally:
			self.index=self.index+1

class DocumentTemplate(Template):
	def __init__(self,child=[],doc_title='',base_url='',body_attr={}):
		Template.__init__(self,child=child)
		self.title=doc_title
		self.url=base_url
		self.body_attr=body_attr
	def __iter__(self):
		return recursiveIter([self.getHeader(),self.child,self.getFooter()])
	def getHeader(self):
		head='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml">\n<head>\n\t<title>'+self.title+'</title>\n'
		for link in self.getCss(self.url):
			head=head+'\t<link rel="stylesheet" href="%s" type="text/css" />\n' % link
		for link in self.getScript(self.url):
			head=head+'\t<script type="text/javascript" src="%s" />\n' % link
		body_tag='<body'
		for item in self.body_attr.items():
			body_tag=body_tag+' %s=%s' % item
		return head+'</head>\n'+body_tag+'>'
	def getFooter(self):
		return "</body></html>"

class SiteDispatcher(Dispatcher):
	accept_events=['org.digitao.events.HttpRequest']
	def __init__(self,site_uri):
		Dispatcher.__init__(self)
		self.site_uri=site_uri
	def dispatch(self,event,cb=None):
		if event['req_headers'].has_key('Accept'):
			# split on acceptable content mime type
			accept_types=event['req_headers']['accept'].split(';')[0].split(',')
			
		

if __name__=="__main__":
	# the hard (but more powerful way) you need this to modify the javascript and css links....
	class ListTemplate(Template):
		js_links=[DocumentRelativePath('test.js')]
		css_links=[HostRelativePath('theme.css'),'http://www.digitao.org/widgets/list.css']
		def next(self):
			try:
				if self.index==0:
					return "<ul>"
				elif self.index<len(self.child)+1:
					return "<li>"+self.child[self.index-1]+'</li>'
				elif self.index==len(self.child)+1:
					return "</ul>"
				else:
					raise StopIteration()
			finally:
				self.index=self.index+1
	# the easy way for a simple list....
	def GenListTemplate(items):
		yield "<ul>"
		for item in items:
			yield "<li>%s</li>" % item
		yield "</ul>"
	print "\n\t##### with class based list template ####\n"
	lt=ListTemplate(child=['test document','test'])
	doc=DocumentTemplate(child=['<b>test list</b>',lt],doc_title="Template Test",base_url="http://www.digitao.org/test/index.html")
	for line in doc:
		print line
	print "\n\t##### with generator based list template ####\n"
	lt=GenListTemplate(['test document','test'])
	doc=DocumentTemplate(child=['<b>test list</b>',lt],doc_title="Template Test",base_url="http://www.digitao.org/test/index.html")
	for line in doc:
		print line
