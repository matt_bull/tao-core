
#   Copyright 2011 Matthew Bull

#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# service / dispatcher utils
import sys,re,thread,os,string,weakref,time,types

def sleep(secs):
	st=time.time()+secs
	while time.time()<st:
		time.sleep(st-time.time())

def runMain():
	import sigIO
	if sigIO.io_env=='python':
		while 1:
			try:
				sleep(90)
			except KeyboardInterrupt:
				break
	elif sigIO.io_env.startswith('gtk'):
		import gobject
		gobject.mainloop()
	elif sigIO.io_env.startswith('glib'):
		from gi.repository import GObject
		ml=GObject.MainLoop()
		try:
			ml.run()
		except KeyboardInterrupt:
			pass
	else:
		print "[ERROR] Unknown io environment %s" % sigIO.io_env

class CompositeException(Exception):
	"""subclass of Exception which carries its own traceback data
	must be initialised within an except block...."""
	def __init__(self,info=""):
		if type(info)==type(""):
			self.info=info
		else:
			self.info=repr(info)
		typ,msg,tb=sys.exc_info()
		if typ:
			self.type=typ.__name__
			self.info="<"+str(msg).strip()+">"
			self.stack=[(tb.tb_frame.f_code.co_filename,tb.tb_frame.f_code.co_name,tb.tb_lineno)]
			while tb.tb_next:
				tb=tb.tb_next
				self.stack.append((tb.tb_frame.f_code.co_filename,tb.tb_frame.f_code.co_name,tb.tb_lineno))
		else:
			self.type=self.__class__.__name__
			self.stack=[]
	def __str__(self):
		return self.info+' - '+self.type
	def __repr__(self):
		tb_string=""
		for frame in self.stack:
			tb_string=tb_string+"\t%s in file %s line %s\n" % (frame[1],frame[0],frame[2])
		return self.__str__()+"\n"+tb_string
	def printFull(self):
		print self.__repr__()

def getBasePath(glob=None):
	if not glob:
		glob=globals()
	if glob.has_key('__path__'):
		return glob['__path__']
	elif glob.has_key('__file__'):
		dp,fn=os.path.split(os.path.abspath(glob['__file__']))
		#print dp,fn
		#if glob['__name__']==fn.rsplit('.',1)[0]:
		return dp

def checkVersion(major,minor,greater=True):
	maj_v,min_v=sys.version_info[:2]
	if greater and maj_v>=major and min_v>=minor:
		return True
	elif maj_v<=major and min_v<=minor and not greater:
		return True

if checkVersion(3,5):
	import importlib.util,sys
	def getModuleFromPath(path,modname=None):
		basepath,fname=os.path.split(path)
		if not modname:
			modname,ext=fname.rsplit('.',1)
		if sys.modules.has_key(modname):
			if sys.modules[modname].__file__==path:
				return sys.modules[modname]
			else:
				modname=modname+'_'+basepath.replace(os.path.sep,'_')
		spec=importlib.util.spec_from_file_location(modname,path)
		if spec:
			mod=importlib.util.module_from_spec(spec)
			spec.loader.exec_module(mod)
			sys.modules[modname]=mod
			return mod
else:
	import imp
	def getModuleFromPath(path,modname=None):
		basepath,fname=os.path.split(path)
		if not modname:
			modname,ext=fname.rsplit('.',1)
		if sys.modules.has_key(modname):
			if sys.modules[modname].__file__==path:
				return sys.modules[modname]
			else:
				(mfile,mpath,desc)=imp.find_module(modname,[basepath])
				return imp.load_module(modname+'_'+basepath.replace(os.path.sep,'_'),mfile,mpath,desc)
		else:
			(mfile,mpath,desc)=imp.find_module(modname,[basepath])
			return imp.load_module(modname,mfile,mpath,desc)

# conf file format re's
section_re=re.compile(r"^\t*\[(?P<name>.*)\]")
line_re=re.compile(r"^(?P<depth>[\t ]*).*")
repl_re=re.compile(r"\$\<(?P<name>.*)\>")
list_re=re.compile(r"^\[(?P<listcont>.*)\]$")
tupl_re=re.compile(r"^\((?P<tupcont>.*)\)$")

def confLinesBuffer(conf_lines):
	l_no=1
	for line in conf_lines:
		yield (l_no,line)
		l_no=l_no+1

class ConfigNode(dict):
	"""config file or subsection dict like object"""
	def __init__(self,parent=None,path=[],conf_lines=[],data={}):
		dict.__init__(self)
		for key in data.keys():
			if isinstance(data[key],dict):
				dict.__setitem__(self,key,ConfigNode(data=data[key]))
			else:
				dict.__setitem__(self,key,data[key])
		self.parent=parent
		self._comments={}
		self.path=path
		if conf_lines:
			self.load(confLinesBuffer(conf_lines))
		self._dirty=False
	def load(self,buff):
		comments=[]
		depth=None
		for l_no,line in buff:
			if line.strip().startswith("#"):
				comments.append(line.strip())
				continue
			ldepth=len(line_re.match(line).group('depth'))
			#print l_no,line.strip(),ldepth,depth
			if not depth:
				depth=ldepth
			elif ldepth<depth:
				#print "breaking at line no",l_no,"in node",repr(self)
				return l_no,line
			elif ldepth>depth:
				print "WARNING - incorrect line format in config file %s, line %s" % self.file,l_no
			s_match=section_re.match(line)
			if s_match:
				#section starting line
				n_path=self.path[:]
				n_path.append(s_match.group('name'))
				if not self.has_key(s_match.group('name')):
					self[s_match.group('name')]=ConfigNode(parent=self,path=n_path)
				elif not isinstance(self[s_match.group('name')],ConfigNode):
					print "WARNING - trying to update existing key %s=%s with section" % (s_match.group('name'),self[s_match.group('name')])
					break
				l_no,line=self[s_match.group('name')].load(buff)
				if comments:
					# there was a comment line attached to this...
					self[s_match.group('name')]._comments['__section_header__']=comments
					comments=[]
			# ordinary line
			if "=" not in line:
				print "bad line - %s" % line
				continue
			key,value=line.split('=',1)
			# strip each element individually to deal with
			# whitespace either side of the '='
			key=key.strip()
			value=value.strip()
			l_m=list_re.match(value)
			t_m=tupl_re.match(value)
			if l_m:
				value=self.parseList(l_m.group('listcont'))
			elif t_m:
				value=self.parseTuple(t_m.group('tupcont'))
			else:
				value=self.parseValue(value)
			if self.has_key(key):
				#if self.log:
				#	self.log(3,"Duplicate key %s in %s" % (key,self.path))
				#else:
				print "Duplicate key %s in %s" % (key,self.path)
			self[key]=value
			if comments:
				# there was a comment line attached to this...
				self._comments[key]=comments
				comments=[]
	def parseList(self,l_str):
		l_list=[]
		if l_str.startswith('['):
			l_str=l_str[1:]
		if l_str.endswith(']'):
			l_str=l_str[:-1]
		while len(l_str):
			next_ind=self.getNextIndex(l_str)
			l_list.append(self.parseValue(l_str[:next_ind]))
			l_str=l_str[next_ind+1:]
		return l_list
	def parseTuple(self,t_str):
		l_list=[]
		if t_str.startswith('('):
			t_str=t_str[1:]
		if t_str.endswith(')'):
			t_str=t_str[:-1]
		while len(t_str):
			next_ind=self.getNextIndex(t_str)
			l_list.append(self.parseValue(t_str[:next_ind]))
			t_str=t_str[next_ind+1:]
		return tuple(l_list)		
	def getNextIndex(self,l_str):
		if l_str.startswith('"'):
			return l_str[1:].index('"')+2
		if l_str.startswith("'"):
			return l_str[1:].index("'")+2
		try:
			return l_str.index(',')
		except ValueError:
			return len(l_str)
	def parseValue(self,value):
		"""try and guess value in string"""
		#value=repl_re.sub(self._loadRepl,value)
		if value[0]=='0' and len(value)>1:
			# number that starts with a 0.... treat as a string
			return value
		try:
			return int(value)
		except ValueError:
			try:
				return float(value)
			except ValueError:
				if value.lower=="true":
					return True
				elif value.lower=="false":
					return False
				return self.stripQuotes(value)
	def stripQuotes(self,qstr):
		if qstr.startswith("'") and qstr.endswith("'"):
			qstr=qstr[1:-1]
		if qstr.startswith('"') and qstr.endswith('"'):
			qstr=qstr[1:-1]
		return qstr
	def __setitem__(self,key,value):
		if self.has_key(key):
			if isinstance(self[key],ConfigNode):
				# break the circular ref...
				self[key].parent=None
		if isinstance(value,ConfigNode):
			# its a node....
			dict.__setitem__(self,key,value)
			value.parent=self
		elif not (type(value)==str or type(value)==int or type(value)==long or type(value)==float or type(value)==list or type(value)==dict or type(value)==tuple):
			raise TypeError('config objects can only hold numerical, string or list values - Illegal value %s of type %s' % (value,type(value)))
		elif type(value)==dict:
			dict.__setitem__(self,key,ConfigNode(parent=self,data=value))
		else:
			dict.__setitem__(self,key,value)
		self.setDirty()
	def __delitem__(self,key):
		if isinstance(self[key],ConfigNode):
			# break the circular ref...
			self[key].parent=None
		dict.__delitem__(self,key)
		self.setDirty()
	def dumpStr(self):
		return ''.join(self.dump())
	def dump(self,path=[]):
		"""get string rep of the keys / values
		
		downcall children to get string rep for each child"""
		d_lines=[]
		depth=len(path)
		for key in sorted(self.keys()):
			if isinstance(self[key],ConfigNode):
				# another config node instance
				path.append(key)
				if self[key]._comments.has_key('__section_header__'):
					for comment in self[key]._comments['__section_header__']:
						d_lines.append("%s%s\n" % (("\t"*depth),comment))
				d_lines.append("%s[%s]\n" % (("\t"*depth),key))
				d_lines.extend(self[key].dump(path=path))
				path.pop()
			else:
				if self._comments.has_key(key):
					for comment in self._comments[key]:
						d_lines.append("%s%s\n" % (("\t"*depth),comment))
				if type(self[key])==list:
					val_list=[]
					for sval in self[key]:
						val_list.append(repr(sval))
					value="[%s]" % string.join(val_list,',')
					#print value
				else:
					value=repr(self[key])
				d_lines.append("%s%s=%s\n" % (("\t"*depth),key,value))
		return d_lines
	def isDirty(self):
		""" of course there is no way to guarantee that another thread
		won't have mutated the dictionary by the time you come to use this info..."""
		return self._dirty
	def setDirty(self):
		self._dirty=True
		if self.parent:
			self.parent.setDirty()

def confFileBuffer(path):
	f=open(path,'r')
	l_no=1
	for line in f:
		yield (l_no,line)
		l_no=l_no+1
	f.close()

class FileConfigNode(ConfigNode):
	""" a convenient subclass which is backed by a file.... can still be updated with load(),
	flush() flushes changes back to disc...."""
	def __init__(self,f_path,data={}):
		ConfigNode.__init__(self,data=data)
		f_path=os.path.abspath(f_path)
		if not (os.access(f_path,os.R_OK) or os.access(os.path.split(f_path)[0],os.W_OK)):
			# can't read the file at path or create a file in that directory....
			# bail
			raise OSError("file %s doesn't exist and directory isn't writable" % f_path)
		self.f_path=f_path
		self.mutex=thread.allocate_lock()
		self._dirty=False
		self._closed=False
		if os.access(f_path,os.R_OK):
			self.load(confFileBuffer(self.f_path))
	def __setitem__(self,key,value):
		self.mutex.acquire()
		if self._closed:
			raise OSError('set on closed node')
		try:
			ConfigNode.__setitem__(self,key,value)
			self._dirty=True
		finally:
			self.mutex.release()
	def __delitem__(self,key):
		self.mutex.acquire()
		if self._closed:
			raise OSError('del on closed node')
		try:
			ConfigNode.__delitem__(self,key)
			self._dirty=True
		finally:
			self.mutex.release()
	def flush(self):
		""" flush changes to disc....
		if there are changes to flush of course, there is no guarantee that
		another thread won't mutate this immediately after you flush... the
		mutex is purely there to ensure that another thread doesn't mutate our
		contents _during_ flush.... and that the dirty flag is accurate"""
		self.mutex.acquire()
		try:
			if self._dirty:
				f=open(self.f_path,'w')
				f.write(self.dumpStr())
				f.close()
				self._dirty=False
		finally:
			self.mutex.release()
	def close(self):
		self.mutex.acquire()
		self._closed=True
		try:
			if self._dirty:
				f=open(self.f_path,'w')
				f.write(self.dumpStr())
				f.close()
		finally:
			self.mutex.release()

# hold weakrefs to objects in an iterable list...
#class RefList:
#	def __init__(self,items=[]):
#		self._refs=[]
#		for item in items:
#			self.append(item)
#	def __remref__(self,ref):
#		self._refs.remove(ref)
#	def __iteritems__(self,refs):
#		for ref in refs:
#			item=ref()
#			if item:
#				yield item
#	def __iter__(self):
#		return self.__iteritems__(self._refs[:])
#	def append(self,item):
#		self._refs.append(weakref.ref(item,self.__remref__))

class RefList(list):
	def __init__(self,seq=[]):
		for i in seq:
			self.append(i)
	def append(self,obj):
		# only allow one copy of obj in list...
		ref=weakref.ref(obj,self.__remref__)
		if not ref in self:
			list.append(self,ref)
	def __iter__(self):
		for ref in self[:]:
			obj=ref()
			if obj:
				yield obj
	def __remref__(self,ref):
		print "remove ref %s from ref_list" % ref
		list.remove(self,ref)
	def remove(self,obj):
		print "remove obj %s from ref_list" % obj
		found=False
		for item in self[:]:
			if item()==obj:
				self.remove(item)
				found=True
				# we don't break here because remove should take out _every_ instance of object
		if not found:
			raise IndexError('%s not found in %s' % (obj,self))

class UserDB:
	'''Abstract base class for user authentication object'''
	def __init__(self):
		if self.__class__==UserDB:
			# don't instanciate directly
			raise TypeError('UserDB is an abstract base type and should not be instanciated directly')
	def getRealm(self):
		raise NotImplementedError('getRealm not implemented')
	def authenticate(self,user,credential):
		raise NotImplementedError('authenticate not implemented')

class ListDict(dict):
	def __init__(self,d={}):
		dict.__init__(self)
		for key in d.keys():
			self.add(key,d[key])
	def add(self,key,value):
		#print "add in ListDict - %s -> %s" % (key,value)
		if self.has_key(key):
			if type(value)==list:
				self[key].extend(value)
			else:
				self[key].append(value)
		else:
			if type(value)==list or type(value)==types.GeneratorType:
				self[key]=value
			else:
				self[key]=[value]

if __name__=="__main__":
	print "Testing Config Node >>>>>>>>>>"
	test_dict={'test key 1':'test "value" 1','test key 2':[1,'string, element',2,3,4],'test key 3':2.20,'test key 4':{'sub key 1':"'sub value 1'",'sub key 2':[1,2,3,4],'sub key 3':2.20,'sub key 2':(1,2,3,4)}}
	t_n=ConfigNode(data=test_dict)
	t_n['test key 4']._comments['__section_header__']=['# test comment for section "test key 4"','# multline comment of course']
	t_n['test key 4']._comments['sub key 1']=['# test comment for key "sub key 1" in section "test key 4" ']
	t_s=t_n.dumpStr()
	print t_s
	r_n=ConfigNode(conf_lines=t_s.split('\n'))
	#c_f=open('test.conf','w')
	#c_f.write(t_s)
	#c_f.close()
	f_n=FileConfigNode('test.conf')
	print "Original Node ->",t_n
	print "Derived Node  ->",r_n
	print "File Node     ->",f_n
	if t_n==r_n==f_n:
		print "Test Result - [OK]"
	else:
		print "Test Result - [FAIL]"
	f_n['test key new']={'sub key':'test value"','sub key,':20}
	#f_n.flush()
	rf_n=FileConfigNode('test.conf')
	print "File Node     ->"
	for key in f_n.keys():
		print key,"->",f_n[key],type(f_n[key])
	print "Derived Node  ->"
	for key in rf_n.keys():
		print key,"->",rf_n[key],type(rf_n[key])
	if f_n==rf_n:
		print "Test Result - [OK]"
	else:
		print "Test Result - [FAIL]"
	del(f_n)
	del(rf_n)
	#os.unlink('test.conf')

	

