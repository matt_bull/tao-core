# sencode... encode python values in the most efficient manner possible


#   Copyright 2011 Matthew Bull

#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# typecodes....
# 0x00 boolean / no value
# 0x01 integer
# 0x02 long integer
# 0x03 float
# 0x04 string
# 0x05 bin string (embedded NULL's)
# 0x06 list
# 0x07 dictionary
# 0x08 iterator

# 0x10 is used as a flag for boolean values

# note: all values packed / unpacked in big-endian (network) byte order,
# ints are 4 bytes, longs 8 bytes (64 bit),
# all numeric values are packed signed
# NULL bytes in strings are illegal

import struct,types
#from operator import isCallable

TYPE_BOOL=0
TYPE_INT=1
TYPE_LONG=2
TYPE_FLOAT=3
TYPE_STR=4
TYPE_BIN=5
TYPE_SEQ=6
TYPE_DICT=7
TYPE_NONE=8
TYPE_REF=9

#class Ref(object):
#	def __init__(self,oid):
#		self.oid=oid
		

def guessType(value):
	# guess the type of value returning one of the numeric constants above
	v_type=type(value)
	if v_type==types.NoneType:
		return TYPE_NONE
	elif v_type==types.BooleanType:
		return TYPE_BOOL
	elif v_type==types.LongType:
		return TYPE_LONG
	elif v_type==types.IntType:
		# newer version of python no longer have long... so catch the value here..
		if abs(value)>2147483647:
			return TYPE_LONG
		return TYPE_INT
	elif v_type==types.FloatType:
		return TYPE_FLOAT
	elif v_type==types.StringType:
		if '\x00' in value:
			return TYPE_BIN
		else:
			return TYPE_STR
	elif hasattr(value,'keys'):
		# a dict or somthing that looks like it
		return TYPE_DICT
	elif hasattr(value,'__call__'):
		# callable, assume its a dispatcher...
		return TYPE_REF
	else:
		# last roll of the dice.... its iterable (make into list) or default to string
		try:
			iter(value)
			return TYPE_SEQ
		except:
			return TYPE_STR

def encode(value,typehint=None,ref_map=None):
	if typehint:
		typ=typehint
	else:
		typ=guessType(value)
	if typ==TYPE_BOOL:
		if value:
			return b'\x10'
		else:
			return b'\x00'
	elif typ==TYPE_INT:
		#print "packing int ->",value
		return b'\x01'+struct.pack('>i',value)
	elif typ==TYPE_LONG:
		return b'\x02'+struct.pack('>q',value)
	elif typ==TYPE_FLOAT:
		return b'\x03'+struct.pack('>d',value)
	elif typ==TYPE_STR:
		return b'\x04'+str(value)+'\x00'
	elif typ==TYPE_BIN:
		value=str(value)
		return b'\x05'+struct.pack('>I',len(value))+value
	elif typ==TYPE_SEQ:
		enc_str=''
		for s_v in value:
			enc_str=enc_str+encode(s_v,ref_map=ref_map)
		return b'\x06'+struct.pack('>I',len(value))+enc_str
	elif typ==TYPE_DICT:
		enc_str=''
		for key in value.keys():
			enc_str=enc_str+str(key)+'\x00'+encode(value[key],ref_map=ref_map)
		return b'\x07'+struct.pack('>I',len(value))+enc_str
	elif typ==TYPE_NONE:
		return b'\x08'
	elif typ==TYPE_REF:
		print id(value)
		if ref_map:
			ref_map[id(value)]=value
			return b'\x09'+struct.pack('>Q',id(value))
		else:
			raise ValueError('cannot pack callables without ref_map')

def decode(enc_str,ref_proxy=None):
	# ref proxy should be a function which returns a proxy object for callables
	# returns tuple of (bytes used, decoded value)
	if not enc_str:
		return (0,None)
	t_code=ord(enc_str[0]) & 0x0f
	st_len=len(enc_str)
	if t_code==TYPE_BOOL:
		return (1,bool(ord(enc_str[0]) & 0x10))
	elif t_code==TYPE_INT and st_len>=5:
		return (5,struct.unpack('>i',enc_str[1:5])[0])
	elif t_code==TYPE_LONG and st_len>=9:
		return (9,struct.unpack('>q',enc_str[1:9])[0])
	elif t_code==TYPE_FLOAT and st_len>=9:
		return (9,struct.unpack('>d',enc_str[1:9])[0])
	elif t_code==TYPE_STR and b'\x00' in enc_str:
		val,junk=enc_str[1:].split(b'\x00',1)
		return (len(val)+2,val)
	elif t_code==TYPE_BIN and st_len>=5:
		s_len=struct.unpack('>I',enc_str[1:5])[0]
		if st_len>=s_len+5:
			return (s_len+5,enc_str[5:s_len+5])
	elif t_code==TYPE_SEQ and st_len>=5:
		count=5
		s_len=struct.unpack('>I',enc_str[1:5])[0]
		val=[]
		ind=0
		while ind<s_len:
			r_l,r_val=decode(enc_str[count:],ref_proxy=ref_proxy)
			if r_val==None:
				# incomplete....
				return (0,None)
			count=count+r_l
			val.append(r_val)
			ind=ind+1
		return (count,val)
	elif t_code==TYPE_DICT and st_len>=5:
		count=5
		s_len=struct.unpack('>I',enc_str[1:5])[0]
		val={}
		ind=0
		while ind<s_len:
			if b'\x00' not in enc_str[count:]:
				return (0,None)
			r_key,rem=enc_str[count:].split(b'\x00',1)
			r_l,r_val=decode(rem,ref_proxy=ref_proxy)
			if r_l==0:
				return (0,None)
			count=count+r_l+len(r_key)+1
			val[r_key]=r_val
			ind=ind+1
		return (count,val)
	elif t_code==TYPE_NONE:
		return (1,None)
	elif t_code==TYPE_REF and st_len>=9:
		#print "ref!!",ref_proxy
		# if ref_proxy exists use it to generate a proxy....
		if ref_proxy:
			ref=struct.unpack('>Q',enc_str[1:9])[0]
			#print "ref!!",ref
			return (9,ref_proxy(ref))
		else:
			return (9,None)
	# if we get here the value was incomplete....
	return (0,None)

def complete(enc_str):
	# returns None if incomplete or length of packet used
	if not enc_str:
		return None
	t_code=ord(enc_str[0]) & 0x0f
	e_len=len(enc_str)
	try:
		if t_code==TYPE_BOOL:
			return 1
		elif t_code==TYPE_INT and e_len>=5:
			return 5
		elif (t_code==TYPE_LONG or t_code==TYPE_FLOAT) and e_len>=9:
			return 9
		elif t_code==TYPE_STR:
			if b'\x00' in enc_str:
				return enc_str.index(b'\x00')+1 
		elif t_code==TYPE_BIN and e_len>4:
			s_len=struct.unpack('>I',enc_str[1:5])[0]
			if e_len>=s_len+5:
				return s_len+5
		elif t_code==TYPE_SEQ and e_len>4:
			count=5
			s_len=struct.unpack('>I',enc_str[1:5])[0]
			ind=0
			while ind<s_len:
				r_l=complete(enc_str[count:])
				if r_l:
					count=count+r_l
					ind=ind+1
				else:
					return None
			return count
		elif t_code==TYPE_DICT and e_len>4:
			count=5
			s_len=struct.unpack('>I',enc_str[1:5])[0]
			ind=0
			while ind<s_len:
				r_key,rem=enc_str[count:].split(b'\x00',1)
				r_l=complete(rem)
				if r_l:
					count=count+r_l+len(r_key)+1
					ind=ind+1
				else:
					return None
			return count
		elif t_code==TYPE_NONE:
			return 1
		elif t_code==TYPE_REF and e_len>=9:
			return 9 
	except ValueError:
		return None

if __name__=="__main__":
	p_str=encode(None)
	print "None -> "+repr(decode(p_str))
	p_str=encode(True)
	print "True -> "+repr(decode(p_str))
	p_str=encode(20)
	print "20 -> "+repr(decode(p_str))
	p_str=encode(long(255))
	print "255L -> "+repr(decode(p_str))
	p_str=encode(20.0)
	print "20.0 -> "+repr(decode(p_str))
	p_str=encode("foo bar")
	print "'foo bar' -> "+repr(decode(p_str))
	p_str=encode("\x00\x01\x02\x03\x00\x40",typehint=5)
	print r"\x00\x01\x02\x03\x00\x40 -> "+repr(decode(p_str))
	p_str=encode(range(20))
	print "range(20) -> "+repr(decode(p_str))
	p_str=encode({'foo':'bar','t_int':20,'t_float':20.0,'t_seq':range(10),'t_none':None,'t_dict':{'nested_key':'nested_value','nested_int_key':3824768276L}})
	print len(p_str),"->",complete(p_str)
	print complete(p_str[:-5])
	print "dict -> "+repr(decode(p_str))

