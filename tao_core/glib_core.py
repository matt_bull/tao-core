# combine sigIO and tdf_core with gtk/gobject....
# this is safe to use with threaded / main / loop dispatchers from tdf_core
# as it synchroises all the returned events....

#   Copyright 2011 Matthew Bull

#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from sigIO import *
from tdf_core import *
#import gobject
#gobject.threads_init()
from gi.repository import GLib,GObject
#GLib.threads_init()
import traceback

class GlibChannel(Channel,SyncSource):
	def __init__(self,dispatcher,io_serv=None,trans_args={},proto_args={}):
		#print "init channel",self,'dispatcher',dispatcher
		Channel.__init__(self,dispatcher,io_serv=io_serv,trans_args=trans_args,proto_args=proto_args)
		SyncSource.__init__(self,dispatcher)
	def put(self,event,sync=False):
		if sync:
			#dispatchWithCallback(self._dispatcher,event,self.render)
			try:
				dis_res=self._dispatcher(event,cb=self._cb)
				#print "dispatched",event.name,event.uri,self._dispatcher
			except StopIteration:
				self.render(event)
			except Exception:
				self.render(event,exc=DispatchException(self._dispatcher))
			else:
				if not dis_res:
					self.render(event)
		else:
			try:
				self._dispatcher(event)
			except Exception,exc:
				sys.stderr.write("Exception on dispatch -"+repr(self)+repr(DispatchException(self._dispatcher)))
	def _cb(self,event,exc=None):
		#print "GLibSource._cb",event.name,event.uri
		if exc:
			print exc
		#GLib.idle_add(self.render,event,exc)
		GLib.idle_add(self._render,event,exc)
	def _render(self,event,exc):
		# hook to protocols onCallback method
		self.onCallback(event,exc=exc)

class GlibSource(Source):
	def __init__(self,dispatcher):
		Source.__init__(self,dispatcher)
	def put(self,event,sync=False):
		if sync:
			#dispatchWithCallback(self._dispatcher,event,self.render)
			try:
				dis_res=self._dispatcher(event,cb=self._cb)
				#print "dispatched",event.name,event.uri,self._dispatcher
			except StopIteration:
				self.render(event)
			except Exception,exc:
				self.render(event,exc=exc)
			else:
				if not dis_res:
					self.render(event)
		else:
			try:
				self._dispatcher(event)
			except Exception,exc:
				sys.stderr.write("Exception on dispatch -",repr(self),repr(DispatchException()))
	def render(self,event,exc=None):
		"""Override to update interface"""
		pass
	def _cb(self,event,exc=None):
		#print "GLibSource._cb",event.name,event.uri
		if exc:
			print exc
		#GLib.idle_add(self.render,event,exc)
		GLib.idle_add(self.render,event)

class GlibDispatcher:
	"""Only deals with Async events.... if cb passsed in will barf..."""
	queue_len=20
	def __init__(self):
		self.in_queue=Queue.Queue(self.queue_len)
		self._active=True
		self.idle=GLib.idle_add(self.iterDispatch)
	def iterDispatch(self):
		try:
			event=self.in_queue.get_nowait()
		except Queue.Empty:
			self.idle=None
			return False
		try:
			self.dispatch(event)
		except:
			pass
		return self._active
	def __call__(self,event,cb=None):
		#print "call in GlibDispatcher"
		self.in_queue.put_nowait(event)
		if not self.idle:
			self.idle=GLib.idle_add(self.iterDispatch)
	def __del__(self):
		self._active=False

class GlibTrackDispatcher(TrackDispatcher):
	"""track dispatcher which syncs both dispatch and callback with glib event loop

	can be useful if you need to insert a dispatcher interacting with glib / gtk into the _middle_ of a dispatch graph...
	"""
	def doDispatch(self,event,cb=None):
		#print "doDispatch in GLibTrackDispatcher",event
		#print self.cb_map
		if cb:
			#print "with Callback"
			try:
				dis_res=self.dispatch(event,cb=self._cb)
				#print "res=",dis_res
			except StopIteration:
				#print "StopIteration"
				self.finish(event)
			except Exception,exc:
				traceback.print_exc()
				self.finish(event,exc=exc)
			else:
				if not dis_res:
					self.finish(event)
		else:
			self.dispatch(event)
	def __call__(self,event,cb=None):
		#print "dispatch in GLibTrackDispatcher"
		GLib.idle_add(self.doDispatch,event,cb)
		if cb:
			return True
	def _cb(self,event,exc=None):
		#print "GLibSource._cb",event.name,event.uri
		if exc:
			print exc
		#GLib.idle_add(self.render,event,exc)
		GLib.idle_add(self.finish,event,exc)

class GlibBusDispatcher(MultiDispatcher):
	""" similar to bus dispatcher, but running from gtk's event loop via idle_add"""
	def __init__(self):
		MultiDispatcher.__init__(self)
		self.idle_id=None
	def glibIterDispatch(self):
		# thin wrapper to return True for GTK...
		self.iterDispatch()
		if self.active:
			return True
		else:
			self.idle_id=None
	def handleNext(self,event,exc=True):
		MultiDispatcher.handleNext(self,event,exc=exc)
		if self.active and not self.idle_id:
			self.idle_id=GLib.idle_add(self.glibIterDispatch)
	def dispatch(self,event,cb=None):
		MultiDispatcher.dispatch(self,event,cb=cb)
		if not self.idle_id:
			self.idle_id=GLib.idle_add(self.glibIterDispatch)
		if cb:
			return True

# sigIO main IOloop class...
class TdfGlibIOMain(GtkIOMain):
	def run(self):
		self.start()
		# call threads_init to stop GObject tying up the GIL
		GObject.threads_init()
		# run the GLib mainloop
		try:
			GLib.MainLoop().run()
		except KeyboardInterrupt:
			pass
		self.stop()
	def newChannel(self,proto_class,trans_class,dispatcher=None,proto_args={},trans_args={}):
		cls_name=proto_class.__name__+'__'+trans_class.__name__
		if not self.ch_map.has_key(cls_name):
			self.ch_map[cls_name]=type(cls_name,(GlibChannel,proto_class,trans_class),{'__name__':cls_name})
		nch=self.ch_map[cls_name](dispatcher,proto_args=proto_args,trans_args=trans_args)
		self.addChannel(nch)
		return nch

import sigIO
sigIO.setEnv("glib-tdf",new_io_main=TdfGlibIOMain)

