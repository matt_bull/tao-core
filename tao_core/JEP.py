from __future__ import absolute_import
from tao_core.tdf_core import SimpleEvent,TrackDispatcher,DispatchException
from tao_core.sigIO import Protocol,Buffer,SimpleProducer,ST_LISTENING
from tao_core import tdf_core,utils
#from operator import isCallable
import thread,json,binascii,struct,weakref,sys,gc

"""
jep protocol... basically just a JSON encoded array...

[<op_code>,<peer_id>,<event_name>,<optional event_id>]

op_code -> one of the integer op codes below.
peer_id -> ID of the dispatcher to use for the event on the far side... 0 being channel._dispatcher
event_name -> name spaced event name
optional -> event_id -> only used when sync dispatch required (ie one with cb=<function>)

"""

OP_REG=0x01 # -> register for events of name <arg1>, optionally with peer_id <arg2>
OP_UNREG=0x02 # -> unregister for events of name <arg1>, optionally with peer_id <arg2>
OP_EVT=0x03 # -> event for peer dispatcher <arg1> with name <arg2> and event dict <arg3> and optionally id of <arg4> (if sync dispatch)
OP_CB=0x04 # -> event callback with id <arg1>, event dict of <arg2> and exception tb (if any) of <arg3>
OP_CONT=0x05 # -> continue previously incomplete event (eg one without FL_FINAL set)
OP_REL=0x06 # -> release a dispatcher in peer with id <arg1>
OP_ERR=0x07

FL_EXC=0x40 # -> dispatch generated remote exception (only valid for OP_CB)
FL_CB=0x40 # -> op requires callback (only valid for OP_EVT)
FL_FINAL=0x80 # -> set if this is the final (or only packet) in an event or callback

OP_MASK=0x1f

class JepBuffer(Buffer):
	def __init__(self,proxy_src=None):
		Buffer.__init__(self,sep='\r\n')
		self.proxy_src=proxy_src
	def next(self):
		#print repr(self[0])
		try:
			return json.loads(self.pop(0),object_hook=self.proxy_src)
		except IndexError:
			raise StopIteration()

class ProxyMap(dict):
	def __call__(self,o):
		if  hasattr(o, '__call__'):
			self[id(o)]=o
			return {'__ref__':id(o),'accept_events':getattr(o,'accept_events',[])}
		else:
			raise TypeError

class DispatchProxy(object):
	def __init__(self,peer,channel):
		self._dsp=weakref.ref(channel)
		self.peer_id=peer['__ref__']
		self.accept_events=list(peer['accept_events'])
	def __call__(self,evt,cb=None):
		if self.accept_events and (evt.name not in self.accept_events):
			raise DispatchException('Dispatcher %s does not accept events of name %s' % (self,evt.name))
		else:
			#print 'GC -',gc.collect()
			#print 'GC -',gc.garbage
			chan=self._dsp()
			#print 'GC -',gc.get_referrers(chan)
			if chan:
				chan.dispatch(evt,cb=cb,peer_id=self.peer_id)
			else:
				raise ReferenceError
	def __del__(self):
		#print "---deleting proxy"
		chan=self._dsp()
		try:
			chan.releaseRef(self.peer_id)
		except:
			pass

# decode_hook should be a callable which takes (<event name>,<event dict>) as args and returns event
# encode_hook takes the result of event.getDict() and returns a json serializable dictionary

class JEPProtocol(Protocol,TrackDispatcher):
	sep='\r\n'
	def __pinit__(self,**kwargs):
		TrackDispatcher.__init__(self,dispatcher=self._dispatcher)
		Protocol.__pinit__(self)
		if kwargs.has_key('decode_hook'):
			self.decode_hook=kwargs['decode_hook']
		else:
			self.decode_hook=None
		self.in_buffer=JepBuffer(proxy_src=self.getProxy)
		self.producer=SimpleProducer()
		self.op_map={} # map of events we have generated to id's
		self.ev_map={} # map of id's to events we have dispatched
		self.id_mutex=thread.allocate_lock() # lock to control access to the above 2
		self.ref_map={}
		self.proxied_map=ProxyMap({0:self._dispatcher})
		if kwargs.has_key('encode_hook'):
			self.encode_hook=kwargs['encode_hook']
		else:
			self.encode_hook=None
	def encode(self,data):
		if self.encode_hook:
			data=self.encode_hook(data)
			return json.dumps(data,default=self.proxied_map)+'\r\n'
		return json.dumps(data,default=self.proxied_map)+'\r\n'
	def dispatch(self,evt,cb=None,peer_id=0):
		if self.state==ST_LISTENING:
			# we are a listener dipatch events toour dispatcher....
			self._dispatcher(evt,cb=cb)
		elif cb:
			self.cb_map[evt]=cb
			e_id=self.getId(evt)
			self.send(self.encode([OP_EVT,peer_id,evt.name,evt.getDict(),e_id]))
			#self.send(chr(op_fl)+'\x04'+sencode.encode(peer_id,sencode.TYPE_LONG)+sencode.encode(evt.name,sencode.TYPE_STR)+sencode.encode(evt.getDict(),sencode.TYPE_DICT,ref_map=self.proxied_map)+sencode.encode(e_id))
			return True
		else:
			self.send(self.encode([OP_EVT,peer_id,evt.name,evt.getDict()]))
			#self.send(chr(op_fl)+'\x03'+sencode.encode(peer_id,sencode.TYPE_LONG)+sencode.encode(evt.name,sencode.TYPE_STR)+sencode.encode(evt.getDict(),sencode.TYPE_DICT,ref_map=self.proxied_map))		
	def onData(self):
		for d_obj in self.in_buffer:
			#print d_obj
			op=d_obj.pop(0)
			if op in (OP_REG,OP_UNREG):
				if len(d_obj)>1:
					# peer_id included... get Proxy
					evts=d_obj[2:]
					# if accept events is set ensure the registered event is included
					if evts and (d_obj[0] not in evts):
						evts.insert(0,d_obj[0])
					dispatcher=self.getProxy({'__ref__':d_obj[1],'accept_events':evts})
				else:
					dispatcher=self
				try:
					if op==OP_REG:
						self._dispatcher.registerDispatcher(d_obj[0],dispatcher)
					elif op==OP_UNREG:
						self._dispatcher.removeDispatcher(d_obj[0],dispatcher)
				except:
					# we should really pass this back to the client as an error...
					pass
			elif op==OP_EVT:
				if self.decode_hook:
					evt=self.decode_hook(d_obj[1],d_obj[2])
				else:
					evt=tdf_core.SimpleEvent(d_obj[1],**d_obj[2])
				if len(d_obj)>3:
					self.op_map[evt]=d_obj[3]
					tdf_core.dispatchWithCallback(self.proxied_map[d_obj[0]],evt,self.onCallback)
				else:
					self.proxied_map[d_obj[0]](evt)
			elif op==OP_CB:
				self.id_mutex.acquire()
				if self.ev_map.has_key(d_obj[0]):
					evt=self.ev_map.pop(d_obj[0])
					self.id_mutex.release()
					evt.update(d_obj[1])
					if len(d_obj)>2:
						self.finish(evt,exc=d_obj[2])
					else:
						self.finish(evt)
				else:
					# oooppppsss....
					self.id_mutex.release()
					raise KeyError('no callback / event registered for id %s' % d_obj[0])
			elif op==OP_REL:
				try:
					del(self.proxy_map[d_obj[0]])
				except KeyError:
					print "Trying to release nonexistant peer id %s" % d_obj[0]
			elif op==OP_ERR:
				# remote error, print....
				print "REMOTE EXCEPTION",d_obj
			else:
				print "unknown operation %s from remote endpoint %s" % (op,self.peer)
	def getProxy(self,o):
		try:
			ref=o['__ref__']
			if not self.ref_map.has_key(ref):
				self.ref_map[ref]=DispatchProxy(o,self)
			return self.ref_map[ref]
		except KeyError:
			return o
	def registerDispatcher(self,evt_name,dispatcher,**kwargs):
		if self.state==ST_LISTENING:
			#we are a listener not a real channel pass this on to our dispatcher...
			self.dispatcher.registerDispatcher(evt_name,dispatcher,**kwargs)
		else:
			# whilst we don't descend from mapping dispatcher, we do map on event name
			self.proxied_map[id(dispatcher)]=dispatcher
			args=[OP_REG,evt_name,id(dispatcher)]
			args.extend(getattr(dispatcher,'accept_events',[]))
			self.send(self.encode(args))
	def removeDispatcher(self,evt_name,dispatcher):
		if self.state==ST_LISTENING:
			self.dispatcher.removeDispatcher(evt_name,dispatcher)
		else:
			del(self.proxied_map[id(dispatcher)])
			self.send(self.encode([OP_UNREG,evt_name,id(dispatcher)]))
	def onCallback(self,event,exc=None):
		#print 'Callback ->',event.getDict()
		if self.op_map.has_key(event):
			if exc:
				self.send(self.encode([OP_CB,self.op_map.pop(event),event.getDict(),exc]))
				#self.send(chr(OP_CB | FL_EXC)+'\x03'+sencode.encode(self.op_map.pop(event))+sencode.encode(event.getDict(),sencode.TYPE_DICT,ref_map=self.proxied_map)+sencode.encode(repr(exc),sencode.TYPE_STR))
			else:
				self.send(self.encode([OP_CB,self.op_map.pop(event),event.getDict()]))
				#print "SEP rendering ->",self.op_map[event],event.getDict()
				#self.send(chr(OP_CB)+'\x02'+sencode.encode(self.op_map.pop(event))+sencode.encode(event.getDict(),sencode.TYPE_DICT,ref_map=self.proxied_map))
		else:
			raise KeyError('no Id for event %s' % repr(event))
	def onSpawn(self,channel):
		#replace dispatcher here to be our parent channel, so the registerDispatcher et al work as expected
		self._dispatcher=channel
	def getId(self,event):
		self.id_mutex.acquire()
		self.ev_map[id(event)]=event
		self.id_mutex.release()
		return id(event)
	def releaseRef(self,peer_id):
		self.send(self.encode([OP_REL,peer_id]))
		#self.send(chr(OP_REL)+'\x01'+sencode.encode(peer_id,sencode.TYPE_LONG))
	def onClose(self):
		#print "onClose...",self
		del(self.in_buffer)
		self.id_mutex.acquire()
		for evt in self.ev_map.values():
			self.finish(evt,exc=IOError('connection to remote dispatcher closed'))
		self.id_mutex.release()
		self._dispatcher.removeAllDispatcher(self.dispatch)

cb_cnt=0
remote_dispatcher=None
remote_cnt=0
if __name__=="__main__":
	import sys,time,os
	from . import sigIO_net,utils,sigIO
	from tao_core.tdf_core import BusDispatcher
	#import glib_core
	io=sigIO.getIO()
	io.start()
	if '-server' in sys.argv:
		def dispatcher(evt,cb=None):
			global remote_dispatcher,remote_cnt
			#print evt.name,evt.getDict()
			if evt.has_key('foo'):
				evt['foo']="manipulated foo"
			print "dispatch ->",evt.name,evt['index']
		bus=BusDispatcher()
		bus.start()
		bus.registerDispatcher('org.digitao.TestEvent',dispatcher)
		ch=io.newChannel(JEPProtocol,sigIO_net.TCPInetTransport,bus)
		ch.listen(('localhost',8080))
		while 1:
			try:
				bus.dispatch(tdf_core.SimpleEvent('org.digitao.AsyncTestEvent',**{'async_remote':'Test','index':remote_cnt}))
			except ReferenceError:
				print "ReferenceError"
				continue
			remote_cnt=remote_cnt+1
			try:
				utils.sleep(2)
			except KeyboardInterrupt:
				break
	else:
		print "do client"
		term_lock=thread.allocate_lock()
		term_lock.acquire()
		remote_count=0
		def remoteDispatch(evt,cb=None):
			global remote_count
			if remote_count==0:
				remote_count=evt['index']+10
			print "dispatch from remote ->",evt.getDict()
			evt['remote_dispatch']=True
			if evt['index']>=remote_count:
				ch.removeDispatcher('org.digitao.AsyncTestEvent',remoteDispatch)
		def callback(evt,exc=None):
			global cb_cnt
			if exc:
				print "exception during dispatch...."
				print repr(exc)
			if cb_cnt>=99:
				term_lock.release()
				print "callback ->",evt['index'],time.time()-evt['st']
			else:
				cb_cnt=cb_cnt+1
				print "callback ->",evt['index'],cb_cnt,evt.getDict()
		ch=io.newChannel(JEPProtocol,sigIO_net.TCPInetTransport)
		ch.connect(('localhost',8080))
		st=time.time()
		ind=0
		#ch(tdf_core.SimpleEvent('org.digitao.AsyncTestEvent',**{'index':ind,'st':st,'dispatch':remoteDispatch}))
		#while time.time()<tt:
		while ind<100:
			utils.sleep(1)
			if ind==10:
				ch.registerDispatcher('org.digitao.AsyncTestEvent',remoteDispatch)
			if time.time()>st+ind:
				try:
					ind=ind+1
					ch(tdf_core.SimpleEvent('org.digitao.TestEvent',**{'foo':'bar','index':ind,'st':st}),cb=callback)
					#ch(tdf_core.SimpleEvent('org.digitao.AsyncTestEvent',**{'foo':'bar','index':ind,'st':st}))
				except KeyboardInterrupt:
					break
		while 1:
			utils.sleep(1)
			if term_lock.acquire(0):
				break
	print len(ch.out_buffer),len(ch.producer)
	ch.close()

