# base transport classes for sigIO

#   Copyright 2011 Matthew Bull

#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
from __future__ import absolute_import
from .sigIO import *
try:
	from OpenSSL import SSL,crypto
	#SSL_V=SSL.OPENSSL_VERSION
except:
	import ssl as SSL

import socket,struct,fcntl,errno

class TCPInetTransport(Transport):
	__name__='TCPInet'
	def __tinit__(self,sock=None,peeraddr=None,allow_reuse_addr=False):
		if sock:
			self.dev=sock
			self.peer=peeraddr
			self.registerFd(sock.fileno())
			self.setFlags(sock.fileno(),select.POLLIN | select.POLLERR)
			self.state=ST_CONNECTED
		else:
			self.dev=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
			self.state=ST_CLOSED
		if allow_reuse_addr:
			self.dev.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.zero_cnt=0
	def connect(self,address):
		'''while True:
			try:
				data = s.recv(2048)
			except socket.error, e:
				if e.errno != errno.EINTR:
				    raise
			else:
				break'''
		while True:
			try:
				self.dev.connect(address)
			except socket.error,e:
				if e.errno!=errno.EINTR:
					raise
			else:
				break
		Transport.connect(self,address)
	def listen(self,address):
		self.dev.bind(address)
		self.dev.listen(10)
		Transport.listen(self,address)
		self.addr=address
	def doInput(self,fd):
		#print "doInput in",repr(self)
		if self.state==ST_LISTENING:
			so,peer=self.dev.accept()
			#print "spawn in",repr(self)
			new_ch=self.spawnChannel(trans_args={'sock':so,'peeraddr':peer})
			# all this is now done in the channel class (as it should be)
			#new_ch=self.__class__(self._dispatcher,trans_args={'sock':so,'peeraddr':peer})
			#self.io_serv.addChannel(new_ch)
			# call onSpawn on the new channel....
			#try:
			#	new_ch.onSpawn(self)
			#except Exception,err:
			#	print "[ERROR] channel exception %s" % err
		else:
			inb=self.dev.recv(1048576)
			#print "got",len(inb),"bytes >>",repr(inb)
			#print repr(self.in_buffer)
			if len(inb)>0:
				self.zero_cnt=0
				return self.in_buffer.feed(inb)
			else:
				if self.zero_cnt>20:
					self.debug("closing on race condition")
					#print "closing on race condition"
					self.close(force=True)
				self.zero_cnt=self.zero_cnt+1
	def doOutput(self,fd):
		#print ">>>>>>>>>>>>>>>>>>>>>>>doOutput"
		sent=self.dev.send(self.out_buffer)
		#print ">>>>>>>>>>>>>>>>>>>>>>>sent",sent
		self.out_buffer=self.out_buffer[sent:]
		#print ">>>>>>>>>>>>>>>>>>>>>>>rem",len(self.out_buffer)
		return len(self.out_buffer)
	def doClose(self):
		self.debug("closing socket")
		try:
			self.dev.shutdown(socket.SHUT_RDWR)
		except socket.error:
			self.dev=None
		self.state=ST_CLOSED

class TLSInetTransport(TCPInetTransport):
	__name__='TLSInet'
	def __tinit__(self,sock=None,peeraddr=None,host_check=True,allow_reuse_addr=False,tls_vers=(1,2)):
		if sock:
			if isinstance(sock,SSL.Connection):
				self.dev=sock
				self.ctx=sock.get_context()
			else:
				if allow_reuse_addr:
					sock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
				if tls_vers[1]==0:
					self.ctx=SSL.Context(SSL.TLSv1_METHOD)
				elif tls_vers[1]==2:
					self.ctx=SSL.Context(SSL.TLSv1_2_METHOD)
				self.dev=SSL.Connection(self.ctx,sock)
			# sock is only passed in on spawn.....
			# set ourselves to accept state
			self.dev.set_accept_state()
			self.peer=peeraddr
			self.registerFd(self.dev.fileno())
			self.setFlags(self.dev.fileno(),select.POLLIN | select.POLLERR)
			self.state=ST_CONNECTED
		else:
			sock=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
			if allow_reuse_addr:
				sock.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
			if tls_vers[1]==0:
				self.ctx=SSL.Context(SSL.TLSv1_METHOD)
			elif tls_vers[1]==2:
				self.ctx=SSL.Context(SSL.TLSv1_2_METHOD)
			else:
				self.ctx=SSL.Context(SSL.SSLv3_METHOD)
			self.dev=SSL.Connection(self.ctx,sock)
			self.state=ST_CLOSED
		self.zero_cnt=0
		self.accept_certs=[]
		self.peer_data=None
	def connect(self,address):
		while True:
			try:
				self.dev.connect(address)
			except socket.error,e:
				if e.errno!=errno.EINTR:
					raise
			else:
				break
		self.dev.do_handshake()
		Transport.connect(self,address)
	def doInput(self,fd):
		#print "<<<SSL Input"
		try:
			return TCPInetTransport.doInput(self,fd)
		except SSL.WantReadError:
			return 0
		except SSL.WantWriteError:
			return 0
		except SSL.WantX509LookupError:
			return 0
		except SSL.ZeroReturnError:
			if self.zero_cnt>20:
				self.debug("closing on race condition")
				#print "closing on race condition"
				self.close(force=True)
			self.zero_cnt=self.zero_cnt+1
		except:
			traceback.print_exc()
			self.setFlags(fd,select.POLLHUP | select.POLLERR | select.POLLNVAL)
			self.close()
	def doOutput(self, fd):
		#print "<<<SSL Output"
		try:
			return TCPInetTransport.doOutput(self, fd)
		except SSL.WantReadError:
			return len(self.out_buffer)
		except SSL.WantWriteError:
			return len(self.out_buffer)
		except SSL.WantX509LookupError:
			return len(self.out_buffer)
		except:
			traceback.print_exc()
			self.setFlags(fd,select.POLLIN | select.POLLHUP | select.POLLERR | select.POLLNVAL)
	def setChannelCert(self,cert,pass_cb=None):
		if pass_cb:
			self.ctx.set_passwd_cb(pass_cb)
		c_f=open(cert,'r')
		self.cert=crypto.load_certificate(crypto.FILETYPE_PEM,c_f.read())
		c_f.seek(0)
		self.pkey=crypto.load_privatekey(crypto.FILETYPE_PEM,c_f.read())
		c_f.close()
		self.ctx.use_certificate(self.cert)
		self.ctx.use_privatekey(self.pkey)
		self.ctx.set_cipher_list("AES256-SHA:AES128-SHA")
	def addVerificationCert(self,certfile):
		"""set verification key from pem file <keyfile>"""
		c_f=open(certfile,'r')
		self.accept_certs.append(crypto.load_certificate(crypto.FILETYPE_PEM,c_f.read()))
		c_f.close()
	def _verify(self,conn,cert,err_no,err_depth,ret_code):
		# this gradually assembles the peer data out of checked certs
		if not self.peer_data:
			self.peer_data={'CA':[],'verified':False,'cert':None}
		if conn!=self.dev:
			# what??
			return False
		if err_depth==0:
			# this is the peer certificate...
			self.peer_data['cert']=cert
			subj=cert.get_subject()
			if '.' in subj.CN:
				self.peer_data['name'],self.peer_data['domain']=subj.CN.split('.',1)
			else:
				self.peer_data['name']=subj.CN
				self.peer_data['domain']=None
			if ret_code:
				self.peer_data['verified']=True
			try:
				self.onVerify(self.peer_data)
			except AttributeError:
				# we don't seem to have an onVerify method....
				print '[TLSTransport] error calling onVerify in channel',self
		elif ret_code:
			self.ca_chain.append(cert)
		return True
	def doValidate(self):
		if not self.peer_data:
			# called before send or recv....
			# force handshake
			self.dev.do_handshake()
		return self.peer_data

class TCPUnixTransport(TCPInetTransport):
	__name__='TCPUnix'
	def __tinit__(self,sock=None,peeraddr=None):
		self.addr=None
		self.zero_cnt=0
		if sock:
			self.dev=sock
			self.peer=peeraddr
			self.registerFd(sock.fileno())
			self.setFlags(sock.fileno(),select.POLLIN | select.POLLERR)
			self.state=ST_CONNECTED
		else:
			self.dev=socket.socket(socket.AF_UNIX,socket.SOCK_STREAM)
			self.peer=None
			self.state=ST_CLOSED
	def listen(self,address):
		self.dev.bind(address)
		self.dev.listen(10)
		self.addr=address
		Transport.listen(self,address)
	def doInput(self,fd):
		#print "doInput in",repr(self)
		if self.state==ST_LISTENING:
			so,peer=self.dev.accept()
			#print so
			new_ch=self.spawnChannel(trans_args={'sock':so,'peeraddr':peer})
		else:
			inb=self.dev.recv(4096)
			if len(inb)>0:
				self.zero_cnt=0
				return self.in_buffer.feed(inb)
			else:
				if self.zero_cnt>20:
					self.debug("closing on race condition")
					#print "closing on race condition"
					self.close(force=True)
				self.zero_cnt=self.zero_cnt+1
	def doClose(self):
		TCPInetTransport.doClose(self)
		#print "unlink address.....",addr,self.dev
		if self.addr:
			# we have been bound unlink file....
			try:
				os.unlink(self.addr)
			except OSError:
				pass

class UDPInetTransport(Transport):
	__name__='UDPInet'
	def __tinit__(self):
		self.dev=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
	def connect(self,address):
		Transport.connect(self,address)
		#UDP is generally unidirectional... if you want both directions do the below in onConnect
		#self.setFlags(self.dev.fileno(),select.POLLIN | select.POLLHUP | select.POLLERR | select.POLLNVAL)
	def listen(self,address):
		self.dev.bind(address)
		Transport.listen(self,address)
	def doInput(self,fd):
		(pack,self.peer)=self.dev.recvfrom(4096)
		#print "%s from %s" % (pack,self.peer)
		return self.in_buffer.feed(pack)
	def doOutput(self,fd):
		#print 'doOutput',self.out_buffer,self.peer
		sent=self.dev.sendto(self.out_buffer,self.peer)
		#print sent
		self.out_buffer=self.out_buffer[sent:]
		return len(self.out_buffer)
	def doClose(self):
		try:
			self.dev.close()
		except:
			self.dev=None
		self.state=ST_CLOSED

def getIpAddress(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(s.fileno(),0x8915,struct.pack('256s', ifname[:15]))[20:24])

DEF_MCAST_GRP = '224.1.1.1'
class UDPMcastTransport(UDPInetTransport):
	def __tinit__(self):
		self.dev=socket.socket(socket.AF_INET,socket.SOCK_DGRAM,socket.IPPROTO_UDP)	
	def listen(self,address,iface=None):
		self.dev.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
		self.dev.bind(address)
		if iface:
			local_addr=getIpAddress(iface)
			self.dev.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_IF, socket.inet_aton(local_addr))
		else:
			local_addr='0.0.0.0'
		if address[0]=='':
			mreq=socket.inet_aton(DEF_MCAST_GRP)+socket.inet_aton(local_addr)
		else:
			mreq=socket.inet_aton(address[0])+socket.inet_aton(local_addr)
		self.dev.setsockopt(socket.IPPROTO_IP,socket.IP_ADD_MEMBERSHIP,mreq)
		self.peer=address
		Transport.listen(self,address)
	def doInput(self,fd):
		# we always receive our own packets back in mullticast so don't set peer from inbound address if state not ST_LISTENING
		(pack,peer)=self.dev.recvfrom(4096)
		if self.state==ST_LISTENING:
			# set peer for use in onData
			self.peer=peer
		#print "%s from %s" % (pack,peer)
		return self.in_buffer.feed(pack)
	def setTTL(self,ttl):
		self.dev.setsockopt(socket.IPPROTO_IP,socket.IP_MULTICAST_TTL,struct.pack('@i',ttl))
	def connect(self,address,ttl=1):
		if ttl>1:
			self.setTTL(ttl)
		Transport.connect(self,address)

class UDPUnixTransport(UDPInetTransport):
	__name__='UDPUnix'
	def __tinit__(self):
		self.dev=socket.socket(socket.AF_UNIX,socket.SOCK_DGRAM)
	def doClose(self):
		addr=self.dev.getsockname()
		UDPInetTransport.doClose(self)
		if addr:
			# we have been bound unlink file....
			os.unlink(addr)
		
if __name__=="__main__":
	import sys
	import time
	from . import tdf_core
	# so how easy is it to implement a protocol?? see for yourself...
	class DummyEvent(tdf_core.SimpleEvent):
		# set a sensible default for key 'output'
		defaults={'output':'Error - Unknown error'}
	class DummyProtocol(Protocol):
		# for text based protocols spilt on '\n'
		sep='\n'
		# this is a simple example using the ioloop to drive dispatch (although you could dispatch any form of event into any event loop...)
		def onData(self):
			for line in self.in_buffer:
				# dispatch an event into dispatcher with line in key 'text'
				# we catch any exceptions here to report exception to peer
				try:
					self.put(DummyEvent("TestEvent",text=line),sync=True)
				except Exception,exc:
					self.send("Server Error - %s" % repr(exc))
					# then of course reraise the exception so we get a traceback
					raise exc
		def onCallback(self,event,exc=None):
			## just send output with any whitespace stripped
			# we can rely on key 'output' existing as it is in the events defaults
			# really we should do something with exception here (if it exists)
			self.send(event['output'].strip())
	# create a quick and dirty handler for the resulting events.....
	def handler(event,cb=None):
		print "handler -> %s" % event['text']
		event['output']="Got %s" % event['text']
		if cb:
			cb(event)
	#create IO Service
	io_main=getIO()
	# start the io loop (from the main thread of course)
	io_main.start()
	# create channel and hand it handler as dispatcher...
	ch=io_main.newChannel(DummyProtocol,TCPInetTransport,dispatcher=handler)
	# listen on localhost
	ch.listen(('localhost',9001))
	# loop until ^c.....
	while 1:
		try:
			time.sleep(90)
		except KeyboardInterrupt:
			break
	# stop the io loop (which also closes all channels)
	io_main.stop()


