# connection  managers for sigIO channels which can dispatch events
from tao_core.tdf_core import Dispatcher
from tao_core import sigIO
import queue,threading

class PersistentConnection(Dispatcher):
	def __init__(self,address,proto_cls,trans_cls,dispatcher=None,idle_timeout=600,conn_timeout=5,conn_max_timeout=600):
		self.address=address
		self.p_cls=proto_cls
		self.t_cls=trans_cls
		self.ch_disp=dispatcher
		self.ch=None
		self.io=sigIO.getIO()
		self.conn_tmo=conn_timeout
		self.curr_conn_tmo=conn_timeout
		self.max_conn_tmo=conn_max_timeout
		self.idle_tmo=idle_timeout
		self.queue=queue.SimpleQueue()
		self._stop=threading.Event()
		self._n_idle=threading.Event()
		self.main_thr=threading.Thread(target=self.mainloop)
		self.main_thr.start()
	def dispatch(self,evt,cb=None):
		self.queue.put((evt,cb))
		self._n_idle.set()
	def onChClose(self,ch,force):
		print('onChClose',ch,force)
		if ch==self.ch:
			self.ch=None
			# chnannel closed by peer...put None to unlock feedloop
			if force:
				self.queue.put(None)
	def connect(self):
		# connect channel
		while 1:
			self.ch=self.io.newChannel(self.t_cls,self.p_cls,self.ch_disp)
			self.ch.exit_handler=self.onChClose
			try:
				self.ch.connect(self.address)
			except IOError:
				self.ch=None
				if self.curr_conn_tmo<self.max_conn_tmo:
					self.curr_conn_tmo=self.conn_tmo*2
			else:
				# yay we got a connection... reset curr_conn_tmo and break
				self.curr_conn_tmo=self.conn_tmo
				break
			if self._stop.wait(timeout=self.curr_conn_tmo):
				break
	def feedloop(self,idle=600):
		# feed channel with events from queue unless idle timeout is reached
		while 1:
			try:
				evt_tup=self.queue.get(timeout=idle)
				if evt_tup:
					yield evt_tup
				else:
					# channel was closed by peer....
					# break as if idle to trigger reconnect
					#if self.queue.empty():
					self._n_idle.clear()
					break
			except queue.Empty:
				if self.ch:
					self.ch.close()
				self._n_idle.clear()
				break
	#def idleloop(self):
	#	while not self._n_idle.wait(timeout=self.idle_tmo):
	#		print('idleing')
	def mainloop(self):
		while 1:
			if not self.ch:
				self.connect()
			if self._stop.is_set():
				break
			print('connected')
			for evt,cb in self.feedloop(self.idle_tmo):
				if not self.ch:
					self.connect()
				self.ch.dispatch(evt,cb=cb)
			if self._stop.is_set():
				break
			if self.queue.empty():
				print('idle')
				self._n_idle.wait()
			if self._stop.is_set():
				break
			print('idle break')	
	def close(self):
		#set the stop event
		self._stop.set()
		# put none to the queue to release feedloop
		self.queue.put(None)
		# set _n_idle to release mainloop in idle state
		self._n_idle.set()
		if self.ch:
			self.ch.close()
		self.main_thr.join()

if __name__=='__main__':
	from tao_core.sigIO_net import TCPInetTransport
	from tao_core import sigIO
	from tao_core.JEP import JEPProtocol
	from tao_core.tdf_core import SimpleEvent
	from tao_core import utils
	io=sigIO.getIO()
	io.start()
	intv=10
	p_c=PersistentConnection(('localhost',9000),JEPProtocol,TCPInetTransport,idle_timeout=20)
	ind=0
	#p_c.dispatch(SimpleEvent('test_event',test1='test1'))
	while 1:
		try:
			utils.sleep(intv)
			print('dispatch on intv',intv)
			intv=intv+5
			p_c.dispatch(SimpleEvent('test_event',test1='test1',interval=intv,index=ind))
		except KeyboardInterrupt:
			break
		ind=ind+1
	p_c.close()
	io.stop()

