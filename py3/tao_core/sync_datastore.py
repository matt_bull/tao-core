# replicating database with optional versioning
from tao_core.sigIO import Protocol
from tao_core.sigIO_net import TCPInetTransport,TLSInetTransport,ST_CLOSING,ST_CLOSED
from tao_core.utils import FileConfigNode
from tao_core.conn_managers import PersistentConnection
from tao_core.datastore import JsonDataStore,LmdbMixin,DAT_CREATE,DAT_READ,DAT_UPDATE,DAT_DELETE
from urllib.parse import urlparse
import os,threading,queue,stat

# journal opcodes extend DataStore operations above
DB_VERS='VERS'	# version see protcol description
# protocol opcodes
DB_SYNC='SYN'	# sync see protocol description
DB_CHNGS='CHG'	# get changes from vers in the form of a list of op,data tuples
DB_CONN='CON'	# new connection authenticate

RESP_CODES=(
	'ACK',	# ack arg is sequence number
	'RSN', # version out of range do full resync....
	'AFL',	# auth fail arg is message
	'CFL',	# record conflict arg is master version of record
	'ERR'	# unknown error
	)

# sync status
ST_SYNC=1
ST_SYNC_CLIENT=2
ST_SYNC_MASTER=3
ST_STREAM=4

class SyncEvent(SimpleEvent):
	defaults={
		'data_op':DB_SYNC,
		'vers':0,
		'client_id':None, # only used by master channel
	}
	def __init__(self,op,**kwargs):
		SimpleEvent.__init__('org.digitao.stack.SyncEvent',data_op=op,**kwargs)

class SyncMasterProtocol(Protocol):
	sep='\n'
	__encoding__='UTF-8'
	def __pinit__(self,**kwargs):
		Protocol.__pinit__(self,**kwargs)
		self.sync_status=None
		self.update_disp=[]
	def _register(self,upd_disp):
		self.update_disp.append(DispatchProxy(upd_disp,meth='asyncDispatch'))
	def onSpawn(self,chan):
		self.client_id=None
		self.update_disp=None
		chan._register(self)
		self.async_queue=queue.SimpleQueue()
		self.ctx.set_session_id("Stack_Master_Server")
		self.ctx.set_verify(VERIFY_PEER | VERIFY_FAIL_IF_NO_PEER_CERT | VERIFY_CLIENT_ONCE,self._verify)
	def streamUpdates(self):
		while self.status not in (ST_CLOSING,ST_CLOSED):
			evt=self.async_queue.get(timeout=60)
			if evt:
				self.sendOp(evt['data_op'],evt['data_id'],evt['data_body'])
	def asyncDispatch(self,evt,cb=None):
		if evt['client_id']!=self.client_id and self.sync_status in (ST_SYNC_MASTER,ST_STREAM):
			self.async_queue.put(evt)
	def sendOp(self,op,_id,data=None):
		if op==DAT_DELTE:
			if op==DAT_DELETE:
				self.send('\t'.join(op,_id))
			else:
				self.send('\t'.join(op,':'.join(_id,json.dumps(data))))
	def dispatch(self,evt,cb=None):
		if self.update_disp:
			for up_d in self.update_disp[:]:
				try:
					up_d(evt)
				except ReferenceError:
					self.update_disp.remove(up_d)
	def syncRecords(self,record_list):
		self.sync_thr=threading.Thread(self._sync)
		self.sync_thr.start()
	def _sync(self,record_list):
		for rec_id,op,data in record_list:
			self.sendOp(op,rec_id,data)
		self.sync_status=ST_STREAM
		self.stream_thr=threading.Thread(self.streamUpdates)
		self.stream_thr.start()
	def onData(self):
		for packet in self.in_buffer:
			seq,op,body=packet.split('\t',2)
			if self.sync_status==ST_NC and op==DB_CONN:
				self.client_id=self.validate(body)
				if self.client_id:
					self.put(SimpleEvent('org.digitao.stack.SyncEvent',data_op=op,client_id=self.client_id,data_body=body,auth=False),sync=True)
				else:
					self.send('AFL\tchannel failed to validate connection')
					self.close()
			elif self.sync_status==ST_SYNC:
				if op==DB_VERS:
					self.put(SimpleEvent('org.digitao.stack.SyncEvent',data_op=DB_CHNGS,seq=int(seq),client_id=self.client_id,vers=body,conflicts=None),sync=True)
				else:
					# change replay
					pass
			elif self.sync_status==ST_SYNC_CLIENT:
				if op==DB_SYNC:
					# end of journal
					self.syncRecords(self.sync_list)
				else:
					# dispatch from journal....
					try:
						_id,body=body.split(':',1)
					except ValueError:
						_id=body
						body='{}'
					body=json.loads(body)
					coll,_id=_id.split('.',1)
					self.put(SyncEvent(op,data_collection=coll,data_id=_id,data_body=body
			elif self.sync_status==ST_STREAM:
				# this is a streamed DataEvent...
				_id,body=body.split(':',1)
				body=json.loads(body)
				if 
				self.put(SimpleEvent('org.digitao.stack.SyncEvent',data_op=op,seq=int(seq),client_id=self.client_id,data_body=body['data_body'],data_id=body['data_id']),sync=True)
	def onCallback(self,evt,exc=None):
		if exc:
			self.send('ERR\tUnknown error')
			return
		if self.sync_status==None and evt['op']==DB_CONN:
			if evt['auth']:
				self.send('%s\t%s' % (DB_SYNC,evt['version']))
				self.sync_status=ST_SYNC
			else:
				self.send('AFL\tAuthentication error')
		elif self.sync_status==ST_SYNC and evt['op']==DB_CHNGS:
			if evt['vers']==0:
				# couldn't find version.... force full resync
				self.send('RSN\tversion too old')
				# we go straight to sync master....
				self.sync_status=ST_SYNC_MASTER
				self.syncRecords(evt['changes'])
			else:
				self.sync_list=evt['changes']
				self.sync_status=ST_SYNC_CLIENT
				self.send('ACK\t'+str(evt['seq']))
		else:
			self.send('ACK\t'+str(evt['seq']))
	def onVerify(self,peer_data):
		# what to do on verify? lookup user and fork?? 
		pass

class SyncClientProtocol(Protocol):
	sep='\n'
	def __pinit__(self,journal=None,**kwargs):
		self.stat=None
		if not journal:
			raise AttributeError('journal must be passed in __pinit__')
		self.journal=journal
		self.journal_thread=threading.Thread(self.asyncFeed)
		self.async_wait=threading.Event()
	def onConnect(self,addr):
		# TODO: send connect packet with auth....
	def sendOp(self,seq,op,_id,data=None):
		if op==DAT_DELTE:
			if op==DAT_DELETE:
				self.send('\t'.join(op,_id))
			else:
				self.send('\t'.join(seq,op,':'.join(_id,json.dumps(data))))
	def onData(self):
		for pk in self.in_buffer:
			code,body=pk.split('\t',1)
			if not self.stat:
				if code in ('AFL','ERR'):
					# oooppppsss bail
					print('[Error in SyncClientProtocol]',body)
					self.close()
				elif code==DB_SYNC:
					# start of the sync interaction
					self.master_vers=int(body)
					self.stat=ST_SYNC
					self.put(SyncEvent(code,vers=0),sync=True)
			elif self.stat==ST_SYNC
				if code=='RSN':
					# argh resync....
					self.stat=ST_SYNC_MASTER
					self.put(SyncEvent(code))
				elif code=='ACK':
					self.stat=ST_SYNC_CLIENT:
					self.client_sync_iter=self.journal.replayFrom()
					try:
						seq,data=self.client_sync_iter.send(None)
						self.send('%s\t%s\t%s' % (seq,op,json.dumps(data)))
					except StopIteration:
						self.send('SYN\tend')
						self.stat=ST_SYNC_MASTER
						del(self.client_sync_iter)
			elif self.stat==ST_SYNC_CLIENT:
				if self.code=='CFL':
					op,_id,data=body.split(':',2)
					if op==DAT_CREATE:
						# yikes this must be a bad record_id....
						#TODO: sync event to change record_id and retry...
						pass
					else:
						self.put(SyncEvent('CFL',data_id=_id,data_body=json.loads(data)))
				try:
					seq,op,_id,data=self.client_sync_iter.send(None)
					self.sendOp(seq,op,_id,data)
					#if op==DAT_DELETE:
					#	self.send('%s\t%s\t%s' % (seq,op,data['data_id'])))
					#else:
					#	self.send('%s\t%s\t%s' % (seq,op,':'.join(data['data_id'],json.dumps(data['data_body'])))
				except StopIteration:
					# end of journal
					self.send('SYN\tend')
					self.stat=ST_SYNC_MASTER
					del(self.client_sync_iter)
			elif self.stat==ST_SYNC_MASTER:
				_id,body=body.split(':',1)
				body=json.loads(body)
				self.put(SyncEvent(op,data_id=_id,data_body=body))
			elif self.stat==ST_STREAM and code=='ACK':
				# ack return event to callback
				self.journal.pop(int(body))
				self.async_wait.set()
			else:
				# this is an async change to the db
				if code==DAT_DELETE:
					self.put(SyncEvent(code,data_id=body))
				elif code==DB_VERS:
					self.put(SyncEvent(code,vers=int(body)))
				else:
					body=body.split(':',1)
					self.put(SyncEvent(code,data_id=body[0],data_body=json.loads(body[1])))
			elif code==11:
				print('Authentication failure, %s' % body)
				sys.exit(2)
	def asyncFeed(self):
		journal_offset=0
		while self.status not in (ST_CLOSING,ST_CLOSED):
			if self.async_wait.wait(timeout=30):
				self.async_wait.clear()
				if self.journal.wait(offset=journal_offset,timeout=900)>0:
					seq,op,_id,data=self.journal.peek(offset=journal_offset)
					while op==DB_VERS:
						journal_offset=journal_offset+1
						try:
							seq,op,_id,data=self.journal.peek(offset=journal_offset)
						except IndexError:
							# end of the journal
							self.async_wait.set()
							continue
					self.sendOp(seq,op,_id,data)
					#self.send('%s\t%s\t%s' % (seq,op,json.dumps(data)))
			else:
				# timeout waiting for response, resend...
				seq,op,_id,data=self.journal.peek(offset=journal_offset)				
				self.sendOp(seq,op,_id,data)
	def onCallback(self,evt,exc=None):
		if not exc:
			if evt['op']==DB_SYNC:
				# send version
				self.send('VERS\t'+str(evt['vers']))
			elif evt['op']==DB_VERS:
	def onVerify(self,peer_data):
		# what to do on verify? check server hostname against cert?
		pass

class LmdbJournal
	def __init__(self,path,store):
		self.db_env=lmdb.oepn(path)
		self.ro_txn=self.db_env.begin(max_dbs=100)
		csr=self.ro_txn.cursor()
		if csr.last():
			self.curr_version=csr.key()
		else:
			# blank virgin journal....
			self.curr_version=0
		self.curr_journal=self.db_env.open_db(self.curr_version,integerkey=True)
		self._queue_csr=self.ro_txn.cursor(self.curr_journal)
		if self._queue_csr.last():
			self.seq_no=self._queue_csr.key()+1
		else:
			self.seq_no=1
		self.mutex=threading.Lock()
		self.not_empty=threading.Condition(self.mutex)
		#self.latest_vers=None
	def sync(self):
		# mmapped database this is a noop
		pass
	def chkpoint(self,vers):
		with self.mutex:
			with self.db_env.begin(write=True,max_dbs=2) as txn:
				self.curr_journal=self.db_env.open_db(vers,txn=txn,integerkey=True)
				self._queue_csr=self.ro_txn.cursor(self.curr_journal)
			self.seq_no=1
	def put(self,evt):
		if 'data_collection' in evt:
			_id='.'.join((evt['data_collection'],evt['data_id']))
		else:
			_id='nc.'+evt['data_id']
		with self.mutex:
			with self.db_env.begin(write=True) as txn:
				csr=txn.cursor()
				csr.last()
				csr.put(struct.pack('I',self.seq_no),(evt['data_op']+':'+_id+':'+json.dumps(evt['data_body'])).encode('UTF-8'))
			self.seq_no=self.seq_no+1
		self.not_empty.notify_all()
	def wait(self,timeout):
		with self.not_empty:
			if timeout is None:
				self.not_empty.wait()
			elif timeout < 0:
				raise ValueError("'timeout' must be a non-negative number")
			else:
				endtime = time() + timeout
				while not self._queue_csr.next():
					remaining =endtime-time()
					if remaining<=0.0:
						return 0
					self.not_empty.wait(remaining)
			return struct.unpack('I',self._queue_csr.key())
	def pop(self,seq_no):
		with self.db_env.begin(write=True) as txn:
			cursor=txn.cursor(self.curr_journal)
			return cursor.pop(struct.pack('I',seq_no))
	def peek(self,offset=0):
		with self.db_env.begin() as txn:
			csr=txn.cursor()
			if not csr.first():
				raise IndexError('current journal has no entries')
			while offset<0:
				if not csr.next():
					raise IndexError('offset beyond end of journal')
			seq,val=csr.item()
			op,_id,data=val.decode('UTF-8').split(':')
			return (struct.unpack('I',seq),op,_id,data)
	def replayFrom(self,vers=None):
		#TODO: yield journal entries from version (or self.curr_version if None) or raise IndexError
		if not vers:
			vers=self.curr_version
		db=self.db_env.open_db(vers,integerkey=True)
		db_csr=self.ro_txn.cursor()
		db_csr.set_key(vers)
		csr=self.ro_txn.cursor(db)
		csr.first()
		while csr:
			for seq,val in csr.iternext():
				op,_id,data=val.decode('UTF-8').split(':')
				yield (struct.unpack('I',seq),op,_id,data)
			if not db_csr.next():
				break
			db=self.db_env.open_db(db_csr.key(),integerkey=True)
			csr=self.ro_txn.cursor(db)
			csr.first()

def_config={
		'master':None,
	}

class ReplicatingDataStore(LmdbMixin,JsonDataStore):
	#TODO: need to add sensible index function
	def __init__(self,base_dir):
		JsonDataStore.__init__(self,base_dir)
		self.meta_base=os.path.join(os.path.abspath(self.base_dir),'.meta')
		try:
			if not stat.S_ISDIR(os.stat(self.meta_base).st_mode):
				raise OSError('something other than a directory already at %s' % self.meta_base)
		except FileNotFoundError:
			os.mkdir(self.meta_base)
		self.journal=LmdbJournal(os.path.join(self.meta_base,'journal'))
		self.config=FileConfigNode(os.path.join(self.meta_base,'config'),def_config)
		io=sigIO.getIO()
		self.in_sync=False
		if self.config['master']:
			# setup client channel and connect to master....
			self.conn_thr=threading.Thread(self.connectToServer,(self.config['master'],))
			self.conn_thr.start()
		else:
			# for now this is OK but ideally every node would have the sync listening...
			self.sync_channel=o.newChannel(SyncMasterProtocol,TCPInetTransport,dispatcher=self)
			if 'listen_address' in self.config:
				addr=self.config['listen_address']
			else:
				addr='0.0.0.0'
			if 'listen_port' in self.config:
				port=self.config['listen_port']
			else:
				port=8560
			self.sync_channel.listen((addr,port))
	def connectToServer(self,server_addr):
		while not self.sync_channel:
			self.sync_channel=io.newChannel(SyncClientProtocol,TCPInetTransport,dispatcher=self,proto_args={'journal':self.journal})
			try:
				self.sync_channel.connect(self.config['master'])
			except IOError:
				self.sync_channel=None
	def backup(self,vers):
		# transfer a copy of any record with changes in journal...
		os.mkdir(os.path.join(self.base_dir,'.backup'))
		for seq,op,_id,data in self.journal.replayFrom(vers):
			if op!=DAT_DELETE:
				r_f=open(os.path.join(self.base_path,'%s.%s' % ('stack',_id)),'r')
				b_f=open(os.path.join(self.base_path,'.backup','%s.%s' % ('stack',_id)),'w')
				b_f.write(r_f.read())
				b_f.close()
	def dispatch(self,evt,cb=None):
		if evt.name=='org.digitao.stack.SyncEvent':
			# sync event from master or slave...
			if evt['data_op']==DAT_SYNC:
				evt['vers']=self.journal.curr_version
				self.in_sync=True
				return
			elif evt['data_op']=='RSN':
				# yikes full resync from server... clear the decks
				# TODO: clear the entire datastore
				self.backup(self.journal.curr_version)
				for f_n in os.listdir(self.base_dir):
					if not (f_n.startswith('.') or stat.S_ISDIR(os.stat(os.path.join(self.base_dir,f_n)).st_mode)):
						os.unlink(os.path.join(self.base_dir,f_n))
				self.journal.clear()
			elif evt['data_op']=='CFL':
				# conflict mark local copy as conflicted...
				#TODO: not sure if this should manipulate flags or set a conflicted key
				o_rec=self.getRecord(evt['data_id'])
				if 'tags' in o_rec:
					tags=o_rec['tags'][:]
					tags.append('conflicted')
				else:
					tags=['conflicted']
				cfl_data={}
				for key in evt['data_body']:
					if key in o_rec:
						cfl_data[key]=o_rec[key]
					o_rec[key]=evt['data_body'][key]
				o_rec=self.updateRecord('stack',evt['data_id'],{'tags':tags,'conflicted_data':cfl_data})
				self.indexRecord('stack',evt['data_id'],
			elif evt['data_op']==DB_CHNGS:
				evt['change_list']=self.getChanges()
			elif evt['data_op']==DB_VERS:
				self.journal.chkpoint(evt['vers'])
				self.in_sync=False
			elif evt['data_op']==DAT_CREATE:
				self.createRecord('stack',evt['data_body'],rec_id=evt['data_id'])
			elif evt['data_op']==DAT_UPDATE:
				self.updateRecord('stack',evt['data_id'],evt['data_body'])
			elif evt['data_op']==DAT_DELETE:
				self.deleteRecord('stack',evt['data_id'])
		elif evt.name=='org.digiato.DataEvent':
			# outside data event
			if not self.in_sync and evt['data_op']!=DAT_READ:
				# we are syncing try again later
				raise DataError(self,'in sync with master unable to service operation')
			rc=JsonDataStore.dispatch(self,evt,cb=cb)
			if evt['data_op']!=DAT_READ:
				e_dat=evt.getDict()
				if e_dat['data_op']==DAT_CREATE:
					e_dat['data_body']={}
				self.journal.put(e_dat)
			return rc
	def getChanges(self,version):
		changes={}
		order=[]
		for _id,op,data in self.journal.replayFrom(version):
			if _id in changes:
				if op==DAT_DELETE:
					# overrides everything... do it to it
					changes[_id]=(op,None)
				elif changes[_id][0]==DAT_CREATE:
					# don't override this we will confuse the client (sending an update for a record they don't have)
					continue
			elif op==DAT_UPDATE and _id in changes:
				# no point to this we already have curr version
				continue
			else:
				changes[_id]=(op,self.getRecord('stack',_id))
				order.append(_id)
		for _id in order:
			yield (_id,changes[_id][0],changes[_id][1])


