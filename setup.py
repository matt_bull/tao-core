from setuptools import setup

setup(name='tao_core',
      version='0.6',
      description='Core Tao framwork',
      author='Matt Bull',
      author_email='matt@digitao.org',
      url='https://digitao.org',
      packages=['tao_core']
     )
